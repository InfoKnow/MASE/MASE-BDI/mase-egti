## MASE-EGTI

MASE-EGTI (Multi-Agent System for Environmental Simulation with Evolutionary Game Theory Interactions) is a tool created for simulations of Land Use and Cover Change models. 

# Installation

It is recommended to pull this project and import it as a Eclipse IDE project with default JRE as JRE 7 ou better. To run, simply set the main class as main.Starter.

# Model Configuration

To configure a model, use the following steps:

1) Duplicate the folder "Dummy" inside the folder "model". Then, rename it to the your model's name.
2) Inside this newly renamed folder, place the initial space file on the folder "spaces", the layers inside the folder "layers" and the public policy inside the folder "pdot".
3) Now, get back to the root of the project and locate the folder "conf". Duplicate the file "cerrado_mapbiomas_experimentos_1.json", then rename the duplicate to a name that is significant to your project.
4) Configure your project by editing this new json file, configuring the necessary parameters.
5) Inside the conf folder, locate the mase.json file. Change the parameter "default_model" to the name of your recently duplicated, renamed and edited json.

# Graphic Interface

Instructions will be released alongside the next commit.

# Contact 

Info-Know Research Group - Prof. Celia Ghedini Ralha

University of Brasilia, Institute of Exact Science, Department of Computer Science
Campus Universitario Darcy Ribeiro, Asa Norte, Edificio CIC/EST, Sala 01, Asa Norte
Zip Code:70904-970 - Brasilia, DF - Brazil

Telephone: +55 61 31073662 
Cel: +55 61 982976665

Emails: cassiocouto88 a? gmail.com, ghedini a? unb.br (replace the ' a? ' with the @ symbol)



# License
[GNU GPLv3](https://www.gnu.org/licenses/agpl-3.0.html)
