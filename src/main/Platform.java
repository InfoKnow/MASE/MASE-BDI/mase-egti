package main;

import java.util.Collection;
import java.util.Map;

import jadex.bridge.IComponentIdentifier;
import jadex.bridge.IExternalAccess;
import jadex.bridge.service.RequiredServiceInfo;
import jadex.bridge.service.search.SServiceProvider;
import jadex.bridge.service.types.cms.CreationInfo;
import jadex.bridge.service.types.cms.IComponentManagementService;
import jadex.commons.future.IFuture;
import jadex.commons.future.IResultListener;
import jadex.commons.future.ITuple2Future;
import jadex.commons.future.ThreadSuspendable;
import jadex.commons.future.TupleResult;

public class Platform {
	private static Platform the_platform;
	private IExternalAccess platform;
	private String platformName;
	private IComponentManagementService cms;

	public static void initPlatform() {
		the_platform = new Platform();
	}

	public static Platform getInstance() {
		return the_platform;
	}
	
	private Platform() {
		String[] defargs = new String[] { "-gui", "false", "-welcome", "false", "-cli", "false", "-printpass", "false",
				"-awareness", "false" };
		IFuture<IExternalAccess> platform_creator = jadex.base.Starter.createPlatform(defargs);
		ThreadSuspendable sus = new ThreadSuspendable();
		platform = platform_creator.get(sus);
		cms = SServiceProvider.getService(platform.getServiceProvider(), IComponentManagementService.class,
				RequiredServiceInfo.SCOPE_PLATFORM).get(sus);
		
	}

	public IExternalAccess getPlatform() {
		return platform;
	}

	public void setPlatform(IExternalAccess platform) {
		this.platform = platform;
	}

	public String getPlatformName() {
		return platformName;
	}

	public void setPlatformName(String platformName) {
		this.platformName = platformName;
	}

	public IComponentManagementService getCms() {
		return cms;
	}

	public void setCms(IComponentManagementService cms) {
		this.cms = cms;
	}

	public ITuple2Future<IComponentIdentifier, Map<String, Object>> deployAgent(String agentPath,
			final Map<String, Object> param) {
		ITuple2Future<IComponentIdentifier, Map<String, Object>> agent = cms.createComponent(agentPath,
				new CreationInfo(param));
		
		return agent;
	}
	
	public void stopPlatform() {
		platform.killComponent();
	}
}
