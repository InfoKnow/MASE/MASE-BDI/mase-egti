package main;

import java.util.HashMap;
import java.util.Set;

import main.loader.JSONLoader;
import main.stats.StatsCollector;

public class Starter {
	public static void main(String[] args) {

		if (args != null && args.length > 0) {

			HashMap<String, String> additional_conf = new HashMap<String, String>();
			for (String arg : args) {
				if (arg.contains(":") || arg.contains("=")) {
					String[] aux = arg.split(":|=");
					additional_conf.put(aux[0], aux[1]);
				}
			}
			HashMap<String, Object> additional_initial_conf = new HashMap<String, Object>();
			if (additional_conf.containsKey("mase.default_model")) {
				String value = additional_conf.remove("mase.default_model");
				additional_initial_conf.put("mase.default_model", value);
			}

			JSONLoader.initInstance(additional_initial_conf);

			Set<String> keys = additional_conf.keySet();
			for (String key : keys) {
				JSONLoader.getInstance().setProperty(key, additional_conf.get(key));
			}

		} else {
			JSONLoader.initInstance();
		}

		Platform.initPlatform();
		Simulation.initSimulation();
		StatsCollector.initStatsCollector();
		Simulation.getInstance().startSimulation();
	}

}
