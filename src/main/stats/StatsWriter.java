package main.stats;

import java.awt.image.RenderedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.imageio.ImageIO;

import main.loader.CommonBeliefDB;

public class StatsWriter {
	private String filename;
	private long file_number;
	private String csv_separator = ",";

	StatsWriter(String extension) {
		filename = createFileName(extension);
	}

	public void writeLine(Object[] line) {
		String result_line = "";
		for (int i = 0; i < line.length; i++) {
			if (i < line.length - 1) {
				result_line = result_line + String.format("%s%s", line[i].toString(), csv_separator);
			} else {
				result_line = result_line + String.format("%s", line[i].toString());
			}
		}
		writeLine(result_line);
	}

	public void writeLine(String line) {
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(filename, true));
			bw.write(line);
			bw.write("\n");
			bw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getFilename() {
		return filename;
	}

	public long getFileNumber() {
		return file_number;
	}

	private String createFileName(String extension) {
		String template = "simulation_%s_%s.%s";
		int file_number = CommonBeliefDB.getCurrSimulation();
		return String.format(template, file_number,
				CommonBeliefDB.getAgent_quantity(),  extension);
	}

	public void writeImage(RenderedImage a, String extension, int step) {
		String template = "simulation_%s_%s_%s.%s";
		int file_number = CommonBeliefDB.getCurrSimulation()-1;
		int curr_step = step;
		String imname = String.format(template, file_number, CommonBeliefDB.getAgent_quantity(), curr_step, extension);
		try {
			ImageIO.write(a, "bmp", new File(imname));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void writeImage(String custom_prefix, RenderedImage a, String extension, int step) {
		String template = "%s_simulation_%s_%s_%s.%s";
		int file_number = CommonBeliefDB.getCurrSimulation()-1;
		int curr_step = step;
		String imname = String.format(template, custom_prefix, file_number, CommonBeliefDB.getAgent_quantity(), curr_step, extension);
		try {
			ImageIO.write(a, "bmp", new File(imname));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
