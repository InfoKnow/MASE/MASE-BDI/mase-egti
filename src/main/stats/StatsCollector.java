package main.stats;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collections;

import agent.feature.AgentSignature;
import game.Game;
import main.loader.CommonBeliefDB;
import main.loader.JSONLoader;
import main.loader.SpatialLoader;
import spatial.Cell;
import spatial.Space;
import util.ImageUtil;

public class StatsCollector {
	private StatsWriter writer;
	private static StatsCollector instance;

	// figure of merit
	private int height;
	private int width;
	private int[][] initialSpace;
	private int[][] finalSpace;
	private int[][] ignoredLayers;

	// census
	private int[] census;
	private int[] owned_cells;

	// conflicts and waiting times
	private int new_spaces;
	private int u_turns;
	private int requisition_line_length;
	private ArrayList<Long> conflicting_times;
	private ArrayList<Long> waiting_times;
	private int exchanged_messages_quantity;
	private long step_time;

	// variables for population counting on Hawk Dove game
	private int agent_quantity;
	private int curr_step;
	private int[] hawk_dove_conflicts;

	// variables for distance and payoff calculation
	private String strategy_change_type;
	private Game chosen_game;
	private ArrayList<Cell> positions;
	private ArrayList<Integer> r0; // accumulated payoff register 0
	private ArrayList<Integer> r1; // accumulated payoff register 1

	public static void initStatsCollector() {
		instance = new StatsCollector();
	}

	public static StatsCollector getInstance() {
		return instance;
	}

	private StatsCollector() {
		height = Space.getInstance().getHeight();
		width = Space.getInstance().getWidth();
		String observed_space_sources[] = SpatialLoader.getInstance().getObservedSpaceSources();
		String spatial_attributes_sources[] = SpatialLoader.getInstance().getSpatialAttributesSources();
		initialSpace = ImageUtil.filtragem_classes_iniciais(observed_space_sources[0].trim());
		finalSpace = ImageUtil.filtragem_classes_iniciais(observed_space_sources[1].trim());
		ignoredLayers = ImageUtil.filtragem_layers_basica(spatial_attributes_sources, width, height);
		census = new int[JSONLoader.getInstance().getCurrGame().getStrategy_count()];
		owned_cells = new int[JSONLoader.getInstance().getCurrGame().getStrategy_count()];
		chosen_game = CommonBeliefDB.getChoosenGame();
		strategy_change_type = JSONLoader.getInstance().getPropertyValue("games.strategy_change_type");
		startStatsCollecting();
	}

	public void startStatsCollecting() {
		writer = new StatsWriter("csv");
		ArrayList<String> header = new ArrayList<>();
		/*
		 * header.add("architecture_type"); header.add("resolution_type");
		 * header.add("curr_game"); header.add("agent_quantity");
		 * header.add("curr_step"); header.add("figure_of_merit");
		 * header.add("producers_accuracy"); header.add("users_accuracy");
		 * header.add("wrong_change"); header.add("right_change");
		 * header.add("wrong_persistance"); header.add("null_model");
		 * header.add("simulated_null_model"); header.add("max_sustainability_status");
		 * header.add("min_sustainability_status"); header.add("max_cell_status");
		 * header.add("min_cell_status"); header.add("new_spaces");
		 * header.add("u_turns"); header.add("max_requisition_line_length");
		 * header.add("conflict_quantity"); header.add("min_conflicting_time");
		 * header.add("mean_conflicting_time");
		 * header.add("standard_deviation_conflicting_time");
		 * header.add("max_conflicting_time"); header.add("min_waiting_time");
		 * header.add("mean_waiting_time");
		 * header.add("standard_deviation_waiting_time");
		 * header.add("max_waiting_time"); header.add("exchanged_messages_quantity");
		 * header.add("step_time"); for(int i = 0; i < census.length; i++) {
		 * header.add("census_strategy_"+i); } for(int i = 0; i < owned_cells.length;
		 * i++) { header.add("owned_cells_strategy_"+i); }
		 */
		header.add("agent_quantity");
		header.add("curr_step");
		header.add("hawk_quantity");
		header.add("dove_quantity");

		header.add("hawk_hawk_conflicts");
		header.add("hawk_dove_conflicts");
		header.add("dove_hawk_conflicts");
		header.add("dove_dove_conflicts");

		header.add("hawk_wins");
		header.add("hawk_losses");
		header.add("dove_wins");
		header.add("dove_losses");

		header.add("hawk_permanence");
		header.add("hawk_to_dove_conversion");
		header.add("dove_permanence");
		header.add("dove_to_hawk_conversion");

		header.add("hawk_new_vegetation_area");
		header.add("hawk_new_anthropic_area");
		header.add("dove_new_vegetation_area");
		header.add("dove_new_anthropic_area");

		header.add("min_distance");
		header.add("max_distance");
		header.add("mean_distance");
		header.add("mean_payoff");
		header.add("hawk_diff_payoff");
		header.add("dove_diff_payoff");

		writer.writeLine(header.toArray());
		resetVariables();
	}

	public void collectAndWrite() {
		ArrayList<Object> line = new ArrayList<>();
		/*
		 * line.add(CommonBeliefDB.getAgentArchitecture());
		 * line.add(CommonBeliefDB.getAgentResolveMethod());
		 * line.add(CommonBeliefDB.getChoosenGame().getGame());
		 * line.add(CommonBeliefDB.getAgent_quantity());
		 * line.add(CommonBeliefDB.getCurrStep()); Double[] fom = calculateFOM(); for
		 * (Double f : fom) { line.add(f); } Integer[] sust = calculateSustainability();
		 * for (Integer s : sust) { line.add(s); } Double[] conf =
		 * calculateConflictsAndWaitingTime(); for (Double c : conf) { line.add(c); }
		 * for(int i = 0; i < census.length; i++) { line.add(census[i]); } for(int i =
		 * 0; i < owned_cells.length; i++) { line.add(owned_cells[i]); }
		 */
		line.add(CommonBeliefDB.getAgent_quantity());
		line.add(CommonBeliefDB.getCurrStep());
		for (int h : hawk_dove_conflicts) {
			line.add(h);
		}
		Double[] mean_distance_and_payoff = calculateMeanDistanceAndMeanPayoff();
		for (double h : mean_distance_and_payoff) {
			line.add(h);
		}
		writer.writeLine(line.toArray());
		resetVariables();

	}

	public synchronized void register_hawk_dove_conflicts(int[] hawk_dove_conflicts) {
		for (int i = 0; i < this.hawk_dove_conflicts.length; i++) {
			this.hawk_dove_conflicts[i] += hawk_dove_conflicts[i];
		}
	}

	public void resetVariables() {
		new_spaces = 0;
		u_turns = 0;
		requisition_line_length = 0;
		conflicting_times = new ArrayList<>();
		waiting_times = new ArrayList<>();
		exchanged_messages_quantity = 0;
		step_time = 0;
		census = new int[JSONLoader.getInstance().getCurrGame().getStrategy_count()];
		owned_cells = new int[JSONLoader.getInstance().getCurrGame().getStrategy_count()];

		hawk_dove_conflicts = new int[18];
		positions = new ArrayList<Cell>();
		r0 = new ArrayList<Integer>();
		r1 = new ArrayList<Integer>();
	}

	public Double[] calculateFOM() {
		double figureOfMerit = 0;
		double nullModel = 0;
		double simulatedNullModel = 0;
		// simulado
		int statePreserved = 0;
		int stateChanged = 1;
		long observedQtyCorrectlyPreserved = 0;
		long observedQtyCorrectlyConverted = 0;
		long observedQtyIncorrectlyPreserved = 0;
		long observedQtyIncorrectlyConverted = 0;

		long simulatedQtyCorrectlyPreserved = 0;
		long simulatedQtyCorrectlyConverted = 0;
		long simulatedQtyIncorrectlyPreserved = 0;
		long simulatedQtyIncorrectlyConverted = 0;

		int type1 = 0;// correctly converted
		int type3 = 0;// incorrectly converted
		int type4 = 0;// incorrectly preserved
		int valid = 0;// valid positions (anything but borders and layers)

		// TODO verificar se isso vai dar certo
		int simulatedFinalState[][] = Space.getInstance().getExplorationValues();

		int[][] initialVersusFinalMap = new int[height][width];
		int preservedValue = SpatialLoader.getInstance().getMaxCellValue();

		// primeiro vamos comparar 2002 com 2008
		for (int j = 0; j < width; j++) {
			for (int i = 0; i < height; i++) {
				if (ignoredLayers[i][j] == 1 || initialSpace[i][j] < 0) {
					initialVersusFinalMap[i][j] = -1;
				} else if (initialSpace[i][j] == preservedValue && finalSpace[i][j] == preservedValue) {

					initialVersusFinalMap[i][j] = statePreserved;
					observedQtyCorrectlyPreserved++;

				} else if (initialSpace[i][j] < preservedValue && finalSpace[i][j] < preservedValue) {

					initialVersusFinalMap[i][j] = statePreserved;
					observedQtyCorrectlyConverted++;

				} else if (initialSpace[i][j] == preservedValue && finalSpace[i][j] < preservedValue) {
					initialVersusFinalMap[i][j] = stateChanged;
					observedQtyIncorrectlyPreserved++;

				} else if (initialSpace[i][j] < preservedValue && finalSpace[i][j] == preservedValue) {

					initialVersusFinalMap[i][j] = stateChanged;
					observedQtyIncorrectlyConverted++;

				}
			}
		}

		for (int j = 0; j < width; j++) {
			for (int i = 0; i < height; i++) {
				if (ignoredLayers[i][j] == 1 || simulatedFinalState[i][j] < 0) {
					// ignorar
				} else if (simulatedFinalState[i][j] == preservedValue && finalSpace[i][j] == preservedValue) {
					simulatedQtyCorrectlyPreserved++;
					valid++;
				} else if (simulatedFinalState[i][j] < preservedValue && finalSpace[i][j] < preservedValue) {
					if (initialVersusFinalMap[i][j] == stateChanged) {
						type1++;
					}
					simulatedQtyCorrectlyConverted++;
					valid++;
				} else if (simulatedFinalState[i][j] == preservedValue && finalSpace[i][j] < preservedValue) {
					type3++;
					simulatedQtyIncorrectlyPreserved++;
					valid++;
				} else if (simulatedFinalState[i][j] < preservedValue && finalSpace[i][j] == preservedValue) {
					type4++;
					simulatedQtyIncorrectlyConverted++;
					valid++;
				}
			}
		}

		// agora vamos calcular o nullModel
		long totalObserved = observedQtyCorrectlyPreserved + observedQtyCorrectlyConverted
				+ observedQtyIncorrectlyConverted + observedQtyIncorrectlyPreserved;
		long errorsObserved = observedQtyIncorrectlyConverted + observedQtyIncorrectlyPreserved;
		nullModel = (double) ((double) errorsObserved / (double) totalObserved);
		// calculando o simulatedNullModel
		long totalSimulated = simulatedQtyCorrectlyPreserved + simulatedQtyCorrectlyConverted
				+ simulatedQtyIncorrectlyConverted + simulatedQtyIncorrectlyPreserved;
		long errorsSimulated = simulatedQtyIncorrectlyConverted + simulatedQtyIncorrectlyPreserved;
		simulatedNullModel = (double) ((double) errorsSimulated / (double) totalSimulated);

		// calculando figura de m�rito
		double wrongchange = 100 * ((double) type4 / valid);
		double rightchange = 100 * ((double) type1 / valid);
		double wrongpersistance = 100 * ((double) type3 / valid);
		figureOfMerit = 100 * ((double) type1 / (type1 + type3 + type4));
		double producersaccuracy = 100 * ((double) type1 / (type1 + type3));
		double usersaccuracy = 100 * ((double) type1 / (type1 + type4));

		return new Double[] { figureOfMerit, producersaccuracy, usersaccuracy, wrongchange, rightchange,
				wrongpersistance, nullModel, simulatedNullModel };

	}

	public Integer[] calculateSustainability() {

		int natural_quantity = 0;
		int natural_sum = 0;
		int anthropized_quantity = 0;
		int anthropized_sum = 0;

		int[][] curr_step_exploration_values = Space.getInstance().getExplorationValues();
		int preservedValue = SpatialLoader.getInstance().getMaxCellValue();

		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				if (curr_step_exploration_values[i][j] == preservedValue) {
					natural_quantity++;
					natural_sum += preservedValue;
				} else if (curr_step_exploration_values[i][j] >= 0) {
					anthropized_quantity++;
					anthropized_sum += curr_step_exploration_values[i][j];
				}
			}
		}

		Integer[] aux = { natural_sum, anthropized_sum, natural_quantity, anthropized_quantity };
		// por enquanto n�o h� essa intera��o no Mase-Hippogriff
		boolean isUnsustainable = anthropized_sum > natural_sum;
		updateSustainabilityStatus(isUnsustainable);
		return aux;

	}

	public void updateSustainabilityStatus(boolean isUnsustainable) {
		Game game = JSONLoader.getInstance().getCurrGame();
		game.setUnsustainableScenario(isUnsustainable);
		JSONLoader.getInstance().setCurrGame(game);
	}

	public synchronized void registerNewSpaceAcquired() {
		new_spaces++;
	}

	public synchronized void registerUTurn() {
		u_turns++;
	}

	public synchronized void registerRequisitionLineLength(int length) {
		if (requisition_line_length < length) {
			requisition_line_length = length;
		}
	}

	public synchronized void registerConflictingTime(long conflicting_time) {
		conflicting_times.add(conflicting_time);
	}

	public synchronized void registerWaitingTime(long waiting_time) {
		waiting_times.add(waiting_time);
	}

	public synchronized void registerExchangedMessages(int n) {
		exchanged_messages_quantity += n;
	}

	public synchronized void registerPosition(Cell c) {
		positions.add(c);
	}

	public synchronized void registerPayoffRegisters(int r0_, int r1_) {
		r0.add(r0_);
		r1.add(r1_);
	}

	public synchronized void setStepTime(long time) {
		step_time = time;
	}

	public synchronized void registerCensus(int strategy_type) {
		census[strategy_type] += 1;
	}

	public synchronized void registerNewCell(int strategy_type) {
		owned_cells[strategy_type] += 1;
	}

	public double calculateMean(ArrayList<? extends Number> list) {
		if (list == null || list.size() == 0) {
			return 0;
		} else if (list.size() == 1) {
			return list.get(0).doubleValue();
		}
		double sum = 0;
		for (Number n : list) {
			sum += n.doubleValue();
		}
		return sum / list.size();

	}

	public double calculateVariance(ArrayList<? extends Number> list) {
		if (list == null || list.size() <= 1) {
			return 0;
		}
		double mean = calculateMean(list);
		double sum = 0;
		for (Number n : list) {
			double aux = mean - n.doubleValue();
			aux = Math.pow(aux, 2);
			sum += aux;
		}
		return sum / (list.size() - 1);
	}

	public Double[] calculateConflictsAndWaitingTime() {

		Collections.sort(conflicting_times);
		Collections.sort(waiting_times);
		double min_conflicting_time = conflicting_times.size() > 0 ? conflicting_times.get(0) : 0;
		double mean_conflicting_time = calculateMean(conflicting_times);
		double std_dev_conflicting_time = Math.sqrt(calculateVariance(conflicting_times));
		double max_conflicting_time = conflicting_times.size() > 0 ? conflicting_times.get(conflicting_times.size() - 1)
				: 0;
		double min_waiting_time = waiting_times.size() > 0 ? waiting_times.get(0) : 0;
		double mean_waiting_time = calculateMean(waiting_times);
		double std_dev_waiting_time = Math.sqrt(calculateVariance(waiting_times));
		double max_waiting_time = waiting_times.size() > 0 ? waiting_times.get(waiting_times.size() - 1) : 0;

		return new Double[] { (double) new_spaces, (double) u_turns, (double) requisition_line_length,
				(double) conflicting_times.size(), min_conflicting_time, mean_conflicting_time,
				std_dev_conflicting_time, max_conflicting_time, min_waiting_time, mean_waiting_time,
				std_dev_waiting_time, max_waiting_time, (double) exchanged_messages_quantity, (double) step_time };
	}

	public Double[] calculateMeanDistanceAndMeanPayoff() {
		double min_distance = Double.MAX_VALUE;
		double max_distance = Double.MIN_VALUE;
		double mean_distance = 0;
		int counter = 0;
		for (int i = 0; i < positions.size() - 1; i++) {
			Cell pivot = positions.get(i);
			int x0 = pivot.getIndex()[0];
			int y0 = pivot.getIndex()[1];
			for (int j = i + 1; j < positions.size(); j++) {
				Cell reference = positions.get(j);
				int x1 = reference.getIndex()[0];
				int y1 = reference.getIndex()[1];
				double aux_distance = Math.sqrt(Math.pow(x0 - x1, 2) + Math.pow(y0 - y1, 2));
				mean_distance += aux_distance;
				if (aux_distance < min_distance) {
					min_distance = aux_distance;
				}

				if (aux_distance > max_distance) {
					max_distance = aux_distance;
				}
				counter++;
			}
		}

		mean_distance = mean_distance / counter;

		double mean_payoff = 0;
		double hawk_payoff = 0;
		double dove_payoff = 0;
		double hawk_proportion = ((double) census[0] / ((double) census[0] + census[1]));
		double dove_proportion = ((double) census[1] / ((double) census[0] + census[1]));
		// calculando mean payoff
		switch (strategy_change_type) {
		case CommonBeliefDB.NONE_CHANGE_TYPE:
			break;
		case CommonBeliefDB.REVENGE_CHANGE_TYPE:

			mean_payoff = (hawk_proportion * hawk_proportion * chosen_game.get_revenge_payoff(0, 0))
					+ (hawk_proportion * dove_proportion * chosen_game.get_revenge_payoff(0, 1))
					+ (dove_proportion * hawk_proportion * chosen_game.get_revenge_payoff(1, 0))
					+ (dove_proportion * dove_proportion * chosen_game.get_revenge_payoff(1, 1));
			hawk_payoff = (hawk_proportion * hawk_proportion * chosen_game.get_revenge_payoff(0, 0))
					+ (hawk_proportion * dove_proportion * chosen_game.get_revenge_payoff(0, 1));
			dove_payoff = (dove_proportion * hawk_proportion * chosen_game.get_revenge_payoff(1, 0))
					+ (dove_proportion * dove_proportion * chosen_game.get_revenge_payoff(1, 1));
			break;
		case CommonBeliefDB.ACC_PAYOFF_CHANGE_TYPE:
			double total_r0 = 0;
			double total_r1 = 0;
			for (int r : r0) {
				total_r0 += r;
			}
			total_r0 = total_r0 / r0.size();
			for (int r : r1) {
				total_r1 += r;
			}
			total_r1 = total_r1 / r1.size();
			int stay_r0 = total_r0 > total_r1? 1:0;
			int change_r0_to_r1 = total_r0 > total_r1? 0:-1;
			int stay_r1 = total_r1 > total_r0? 1:0;
			int change_r1_to_r0 = total_r1 > total_r0? 0:-1;
			mean_payoff = (hawk_proportion * hawk_proportion * stay_r0)
					+ (hawk_proportion * dove_proportion * change_r0_to_r1)
					+ (dove_proportion * hawk_proportion * stay_r1)
					+ (dove_proportion * dove_proportion * change_r1_to_r0);
			hawk_payoff = (hawk_proportion * hawk_proportion * stay_r0)
					+ (hawk_proportion * dove_proportion * change_r0_to_r1);
			dove_payoff = (dove_proportion * hawk_proportion * stay_r1)
					+ (dove_proportion * dove_proportion * change_r1_to_r0);
			break;
		}

		return new Double[] { min_distance, max_distance, mean_distance, mean_payoff, hawk_payoff, dove_payoff };
	}

	public void generateImage() {
		generateImage(null);
	}

	public void generateImage(Integer step) {
		Cell[][] cells = Space.getInstance().getCells();
		int height = Space.getInstance().getHeight();
		int width = Space.getInstance().getWidth();
		int minimum_cell_group_height = Integer
				.parseInt(JSONLoader.getInstance().getPropertyValue("space.cell_dimensions.height"));
		int minimum_cell_group_width = Integer
				.parseInt(JSONLoader.getInstance().getPropertyValue("space.cell_dimensions.width"));
		height = height / minimum_cell_group_height;
		width = width / minimum_cell_group_width;

		BufferedImage improc = new BufferedImage(width * minimum_cell_group_width, height * minimum_cell_group_height,
				BufferedImage.TYPE_INT_RGB);
		Color[] type_color = CommonBeliefDB.getChoosenGame().getStrategiesColors();

		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {

				Cell cell = cells[i][j];
				int[][] localValues = cell.getLocalValues();
				AgentSignature owner = cell.getOwner();
				int owner_type = cell.getOnwerType();
				for (int a = 0; a < minimum_cell_group_height; a++) {
					for (int b = 0; b < minimum_cell_group_width; b++) {
						int choosen_color = 0x000000;
						int x = (j * minimum_cell_group_width) + b;
						int y = (i * minimum_cell_group_height) + a;
						if (owner == null) {
							if (localValues[a][b] == SpatialLoader.getInstance().getMaxCellValue()) {
								choosen_color = 0x006400;
							} else if (localValues[a][b] > 0) {
								choosen_color = 0xffff32;
							} else {
								choosen_color = 0xffffff;
							}
						} else {
							int type = owner_type;
							if (type >= 0) {
								choosen_color = type_color[type].getRGB();
							} else {
								choosen_color = 0xff00ff;

							}
						}
						improc.setRGB(x, y, choosen_color);
					}
				}

			}
		}
		if (step == null) {
			step = CommonBeliefDB.getCurrStep();
		}
		writer.writeImage(improc, "bmp", step);
	}

	public void generateConflictImage(Integer step) {
		int[][] cells = Space.getInstance().getConflictedSpaces();
		int height = Space.getInstance().getHeight();
		int width = Space.getInstance().getWidth();
		int minimum_cell_group_height = Integer
				.parseInt(JSONLoader.getInstance().getPropertyValue("space.cell_dimensions.height"));
		int minimum_cell_group_width = Integer
				.parseInt(JSONLoader.getInstance().getPropertyValue("space.cell_dimensions.width"));
		height = height / minimum_cell_group_height;
		width = width / minimum_cell_group_width;

		BufferedImage improc = new BufferedImage(width * minimum_cell_group_width, height * minimum_cell_group_height,
				BufferedImage.TYPE_INT_RGB);
		Color[] type_color = CommonBeliefDB.getChoosenGame().getStrategiesColors();

		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {

				int conflicted_value = cells[i][j];
				int[][] localValues = new int[minimum_cell_group_height][minimum_cell_group_width];
				for (int a = 0; a < localValues.length; a++) {
					for (int b = 0; b < localValues[a].length; b++) {
						localValues[a][b] = conflicted_value;
					}
				}

				for (int a = 0; a < minimum_cell_group_height; a++) {
					for (int b = 0; b < minimum_cell_group_width; b++) {
						int choosen_color = 0x000000;
						int x = (j * minimum_cell_group_width) + b;
						int y = (i * minimum_cell_group_height) + a;
						switch (localValues[a][b]) {
						case Space.UNCONFLICTED:
							choosen_color = 0xCCCCCC;
							break;
						case Space.ZERO_ZERO_CONFLICT:
							choosen_color = 0xFF0000;
							break;
						case Space.ZERO_ONE_CONFLICT:
							choosen_color = 0xFF00FF;
							break;
						case Space.ONE_ONE_CONFLICT:
							choosen_color = 0x0000FF;
							break;
						case Space.MANY_MANY_CONFLICT:
							choosen_color = 0xFFFF00;
							break;
						}
						improc.setRGB(x, y, choosen_color);
					}
				}

			}
		}
		if (step == null) {
			step = CommonBeliefDB.getCurrStep();
		}
		writer.writeImage("conflict", improc, "bmp", step);
	}

}
