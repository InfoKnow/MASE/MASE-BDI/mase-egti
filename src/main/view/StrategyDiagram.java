package main.view;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.text.DecimalFormat;

import main.loader.Loader;

public class StrategyDiagram extends GraphScreen {

	private static final long serialVersionUID = 1L;
	private int step_quantity = 0;
	private int agent_quantity = 0;
	private String strategy_names[];
	private Integer strategy_count[][];
	private int height;
	private int width;
	private Color colors[];
	private String graphName = "Strategy Dynamics Diagram";

	private int[] last_x;
	private int[] last_y;
	private double[] last_perc;

	public StrategyDiagram() {
	}

	public void updateGraph(int agent_quantity, int curr_step, String strategy_names[], Integer strategy_count[][]) {
		if (step_quantity == 0) {
			step_quantity = Integer.parseInt(Loader.getPropertyValue("step_quantity"));
			String game = Loader.getPropertyValue("game");
			int aux_strategies = Integer.parseInt(Loader.getPropertyValue(game + "_strategy_count"));
			colors = new Color[aux_strategies];
			for (int i = 0; i < aux_strategies; i++) {
				colors[i] = new Color(
						Integer.parseInt(Loader.getPropertyValue(game + "_strategy_" + i + "_color"), 16));
			}

			if (last_x == null) {
				last_x = new int[aux_strategies];
				last_y = new int[aux_strategies];
				last_perc = new double[aux_strategies];
			}
		}
		this.strategy_names = strategy_names;
		this.strategy_count = strategy_count;
		this.agent_quantity = agent_quantity;
		repaint();
	}

	public void paintComponent(Graphics g) {

		Graphics2D g2 = (Graphics2D) g;
		Dimension d = this.getSize();
		height = (int) d.getHeight();
		width = (int) d.getWidth();
		g2.setColor(Color.WHITE);
		g2.fillRect(0, 0, width, height);

		g.setColor(Color.BLACK);
		g.drawString(graphName, 10, 15);

		int padding_x = (int) (0.1 * width);
		int padding_y = (int) (0.2 * height);

		int x_axis_i = (padding_x);
		int x_axis_j = width - padding_x;
		int y_axis_i = height - padding_y;
		int y_axis_j = padding_y;

		g2.setColor(Color.BLACK);
		g2.setStroke(new BasicStroke(1));
		g2.drawLine(x_axis_i, y_axis_i, x_axis_j, y_axis_i);
		g2.drawLine(x_axis_i, y_axis_i, x_axis_i, y_axis_j);

		if (step_quantity == 0)
			return;

		double vertical_factor = (height - (2 * padding_y)) / (100d);
		double horizontal_factor = (width - (2 * padding_x)) / ((double) step_quantity);

		int threshold = 50;

		for (int i = 0; i < step_quantity - 1; i++) {

			g2.setStroke(new BasicStroke(1));
			g2.setColor(Color.DARK_GRAY);
			int f_i = padding_x + (int) Math.round(i * horizontal_factor);
			int g_i = (height - padding_y) - (int) (threshold * vertical_factor);
			int f_j = padding_x + (int) Math.round((i + 1) * horizontal_factor);
			int g_j = (height - padding_y) - (int) (threshold * vertical_factor);
			g2.drawLine(f_i, g_i, f_j, g_j);

			for (int a = 0; strategy_count != null && strategy_count[i] != null && a < strategy_count[i].length; a++) {

				if (strategy_count[i][a] == null) {
					continue;
				}

				if (strategy_count[i + 1][a] == null && i - 1 >= 0) {
					last_perc[a] = 100 * (double) strategy_count[i - 1][a] / (double) agent_quantity;
					continue;
				}
				int curr_strat_count = 100 * strategy_count[i][a] / agent_quantity;
				int next_strat_count = 100
						* (strategy_count[i + 1][a] == null ? strategy_count[i][a] : strategy_count[i + 1][a])
						/ agent_quantity;

				g2.setColor(colors[a]);

				int x_i = padding_x + (int) Math.round(i * horizontal_factor);
				int y_i = (height - padding_y) - (int) (Math.round(curr_strat_count * vertical_factor));
				int x_j = padding_x + (int) Math.round((i + 1) * horizontal_factor);
				int y_j = (height - padding_y) - (int) (Math.round(next_strat_count * vertical_factor));

				last_x[a] = x_i;
				last_y[a] = y_i;

				g2.setStroke(new BasicStroke(3));
				g2.drawLine(x_i, y_i, x_j, y_j);
			}

		}

		for (int a = 0; a < last_x.length; a++) {
			DecimalFormat df = new DecimalFormat();
			df.setMaximumFractionDigits(2);
			String string_perc = df.format(last_perc[a]).toString();
			g.setColor(Color.BLACK);
			g.drawString(String.format("%s:%s", strategy_names[a], string_perc), last_x[a], last_y[a]);
		}

		// desenhando a legenda
		int subpad_y = (int) (0.1 * height);
		int side = 15;
		int sub_y_axis_i = height - subpad_y;
		int leg_jump = (int) ((horizontal_factor * step_quantity) / colors.length);
		for (int a = 0; a < colors.length; a++) {
			g.setColor(colors[a]);
			g.fillRect(padding_x + a * leg_jump, sub_y_axis_i, side, side);
			g.setColor(Color.BLACK);
			g.drawString(strategy_names[a], padding_x + a * leg_jump + 20, sub_y_axis_i + 10);
		}

	}
}
