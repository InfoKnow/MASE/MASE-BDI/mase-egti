package main.view;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.text.DecimalFormat;

import main.loader.Loader;

public class FOMDiagram extends GraphScreen {

	private static final long serialVersionUID = 1L;
	private int step_quantity = 0;
	private Double fom[] = null;
	private Double nullModel[] = null;
	private Double simulatedNullModel[] = null;
	private int height;
	private int width;
	private String graphName = "Figure of Merit";
	private float jump = 1f / 100f;

	public FOMDiagram() {
	}

	public void updateGraph(int step, double fomX, double nullModelX, double simulatedNullModelX) {
		if (step_quantity == 0) {
			step_quantity = Integer.parseInt(Loader.getPropertyValue("step_quantity"));
			fom = new Double[step_quantity];
			nullModel = new Double[step_quantity];
			simulatedNullModel = new Double[step_quantity];
		}
		fom[step - 1] = fomX;
		nullModel[step - 1] = nullModelX;
		simulatedNullModel[step - 1] = simulatedNullModelX;
		repaint();
	}

	public void paintComponent(Graphics g) {

		Graphics2D g2 = (Graphics2D) g;
		Dimension d = this.getSize();
		height = (int) d.getHeight();
		width = (int) d.getWidth();
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, width, height);

		g.setColor(Color.BLACK);
		g.drawString(graphName, 10, 25);

		int padding_x = (int) (0.1 * width);
		int padding_y = (int) (0.2 * height);

		int x_axis_i = (padding_x);
		int x_axis_j = width - padding_x;
		int y_axis_i = height - padding_y;
		int y_axis_j = padding_y;

		g.setColor(Color.BLACK);
		g.drawLine(x_axis_i, y_axis_i, x_axis_j, y_axis_i);
		g.drawLine(x_axis_i, y_axis_i, x_axis_i, y_axis_j);

		if (step_quantity == 0) {
			return;
		}

		double vertical_factor = (height - (2 * padding_y)) / (100d);
		double horizontal_factor = (width - (2 * padding_x)) / ((double) step_quantity);

		int fom_threshold = 50;
		double last_fom = 0;
		for (int i = 0; i < step_quantity - 1; i++) {
			g.setColor(Color.DARK_GRAY);
			int f_i = padding_x + (int) Math.round(i * horizontal_factor);
			int g_i = (height - padding_y) - (int) (fom_threshold * vertical_factor);
			int f_j = padding_x + (int) Math.round((i + 1) * horizontal_factor);
			int g_j = (height - padding_y) - (int) (fom_threshold * vertical_factor);
			g2.setStroke(new BasicStroke(1));
			g.drawLine(f_i, g_i, f_j, g_j);

			if (fom[i] != null && fom[i + 1] != null) {
				g.setColor(new Color(Color.HSBtoRGB((float) (jump * fom[i]), 1f, 1f)));
				int x_i = padding_x + (int) Math.round(i * horizontal_factor);
				int y_i = (height - padding_y) - (int) (Math.round(fom[i].intValue() * vertical_factor));
				int x_j = padding_x + (int) Math.round((i + 1) * horizontal_factor);
				int y_j = (height - padding_y) - (int) (Math.round(fom[i + 1].intValue() * vertical_factor));
				g2.setStroke(new BasicStroke(3));
				g2.drawLine(x_i, y_i, x_j, y_j);
				last_fom = fom[i + 1];
			}

		}

		// desenhando a legenda
		int subpad_y = (int) (0.1 * height);
		int sub_y_axis_i = height - subpad_y;
		for (int a = 0; a < step_quantity - 1; a++) {
			int sub_x_axis_i = (int) (padding_x + (a * horizontal_factor));
			int sub_x_axis_j = (int) (padding_x + ((a + 1) * horizontal_factor));
			float curr_hue = (float) (a / (float) step_quantity);
			g.setColor(new Color(Color.HSBtoRGB(curr_hue, 1f, 1f)));
			g.drawLine(sub_x_axis_i, sub_y_axis_i, sub_x_axis_j, sub_y_axis_i);
		}
		g.setColor(Color.BLACK);
		g.drawString("0", padding_x - 7, height - subpad_y + 5);

		g.drawString("100", (int) (padding_x + (step_quantity * horizontal_factor)), height - subpad_y + 5);

		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(2);
		String string_fom = df.format(last_fom).toString();
		horizontal_factor = (width - (2 * padding_x)) / ((double)100);
		g.drawString(string_fom, (int) (padding_x + (last_fom * horizontal_factor)), height - subpad_y - 3);

	}

}
