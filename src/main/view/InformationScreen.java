package main.view;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SpringLayout;
import javax.swing.text.DefaultCaret;

import main.Main;

public class InformationScreen extends JPanel implements MouseListener {
	private static final long serialVersionUID = 1L;
	private JLabel modelName;
	private String str_modelName;
	private String strNrml_modelName;

	private JLabel currStep;
	private String str_currStep;
	private String strNrml_currStep;

	private JTextArea infoarea;
	private JScrollPane scroll;

	private JButton start;
	private boolean started = false;
	private JButton pause;
	private JButton stop;

	public InformationScreen() {
		strNrml_modelName = "Model name: %s";
		str_modelName = "Cerrado-DF (Default)";
		modelName = new JLabel(String.format(strNrml_modelName, str_modelName));

		strNrml_currStep = "Current Step: %s";
		str_currStep = "None";
		currStep = new JLabel(String.format(strNrml_currStep, str_currStep));

		infoarea = new JTextArea(5, 42);
		infoarea.setText("When the simulation starts, information will be displayed here.");
		DefaultCaret caret = (DefaultCaret) infoarea.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		scroll = new JScrollPane(infoarea);

		start = new JButton("Start");
		start.setEnabled(!started);
		start.addMouseListener(this);

		pause = new JButton("Pause");
		pause.setEnabled(started);
		pause.addMouseListener(this);

		stop = new JButton("Stop");
		stop.setEnabled(started);

		SpringLayout layout = new SpringLayout();
		this.setLayout(layout);

		this.add(modelName);
		this.add(currStep);
		this.add(scroll);

		layout.putConstraint(SpringLayout.WEST, modelName, 5, SpringLayout.WEST, this);
		layout.putConstraint(SpringLayout.WEST, currStep, 5, SpringLayout.WEST, this);
		layout.putConstraint(SpringLayout.WEST, scroll, 10, SpringLayout.WEST, this);
		layout.putConstraint(SpringLayout.NORTH, currStep, 3, SpringLayout.SOUTH, modelName);
		layout.putConstraint(SpringLayout.NORTH, scroll, 3, SpringLayout.SOUTH, currStep);

		this.add(start);
		this.add(pause);
		this.add(stop);

		layout.putConstraint(SpringLayout.SOUTH, start, -5, SpringLayout.SOUTH, this);
		layout.putConstraint(SpringLayout.SOUTH, pause, -5, SpringLayout.SOUTH, this);
		layout.putConstraint(SpringLayout.SOUTH, stop, -5, SpringLayout.SOUTH, this);
		layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, pause, 0, SpringLayout.HORIZONTAL_CENTER, this);
		layout.putConstraint(SpringLayout.EAST, start, -5, SpringLayout.WEST, pause);
		layout.putConstraint(SpringLayout.WEST, stop, 5, SpringLayout.EAST, pause);

	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
		if (e.getSource().equals(start)) {
			start.setEnabled(false);
			pause.setEnabled(true);
			stop.setEnabled(true);
			// iniciar simula��o default
			// TODO load appropriate model. For now is justing loading the default model
			Main.getInstance().initializeSimulation();
			Main.getInstance().updateSimulationState(SimulationScreen.RUNNING);
		} else if (e.getSource().equals(pause)) {
			// TODO pausar a aplica��o
		} else if (e.getSource().equals(stop)) {
			// TODO parar a aplica��o
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	public void setstr_modelName(String str_modelName) {
		this.str_modelName = str_modelName;
		modelName.setText(String.format(strNrml_modelName, str_modelName));
	}

	public void setstr_currStep(String str_currStep) {
		this.str_currStep = str_currStep;
		currStep.setText(String.format(strNrml_currStep, str_currStep));
	}

	public synchronized void appendToInfoArea(String text) {
		this.infoarea.append(text);
		this.infoarea.append("\n");
	}
}