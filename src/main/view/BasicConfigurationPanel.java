package main.view;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class BasicConfigurationPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	private JLabel label_modelname;
	private JLabel label_author;
	private JLabel label_description;
	private JTextField textfield_modelname;
	private JTextField textfield_author;
	private JTextField textfield_description;

	public BasicConfigurationPanel() {
		label_modelname = new JLabel("Model Name:");
		label_author = new JLabel("Author:");
		label_description = new JLabel("Description:");

		textfield_modelname = new JTextField(50);
		textfield_author = new JTextField(50);
		textfield_description = new JTextField(50);
		textfield_description.setSize(new Dimension(200, 300));

		GridBagLayout layout = new GridBagLayout();
		GridBagConstraints constraints = new GridBagConstraints();
		this.setLayout(layout);
		
		layout.columnWeights = new double[]{1, 1};
		layout.rowWeights = new double[]{1, 1, 2};
		
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.fill = GridBagConstraints.BOTH;
		this.add(label_modelname, constraints);
		
		constraints.gridx = 1;
		constraints.gridy = 0;
		constraints.fill = GridBagConstraints.BOTH;
		this.add(textfield_modelname, constraints);
		
		constraints.gridx = 0;
		constraints.gridy = 1;
		constraints.fill = GridBagConstraints.BOTH;
		this.add(label_author, constraints);
		
		constraints.gridx = 1;
		constraints.gridy = 1;
		constraints.fill = GridBagConstraints.BOTH;
		this.add(textfield_author, constraints);
		
		constraints.gridx = 0;
		constraints.gridy = 2;
		constraints.fill = GridBagConstraints.BOTH;
		this.add(label_description, constraints);
		
		constraints.gridx = 1;
		constraints.gridy = 2;
		constraints.fill = GridBagConstraints.BOTH;
		this.add(textfield_description, constraints);
		
		
	}

	public String getTextfield_modelname() {
		return textfield_modelname.getText();
	}

	public void setTextfield_modelname(String str_modelname) {
		this.textfield_modelname.setText(str_modelname);
	}

	public String getTextfield_author() {
		return textfield_author.getText();
	}

	public void setTextfield_author(String str_author) {
		this.textfield_author.setText(str_author);
	}

	public String getTextfield_description() {
		return textfield_description.getText();
	}

	public void setTextfield_description(String str_description) {
		this.textfield_description.setText(str_description);
	}
	

}
