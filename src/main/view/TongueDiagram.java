package main.view;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import main.loader.Loader;

public class TongueDiagram extends GraphScreen implements MouseListener {
	private static final long serialVersionUID = 1L;
	private int step_quantity = 0;
	private int height;
	private int width;
	private int preservedState;
	private String strategy_names[];
	private int exploration_per_step[];

	private int facet = 0;
	// facet = 0 means tongue model with exploration potential
	// facet = 1 means tongue model with area
	private String strNrml_graphName = "Tongue Model: %s";
	private String facetName[] = { "Exploration Potential", "Area" };
	// facet = 0
	private long[][] explorationPotentialValues;
	private long[][] explorationPotentialStrategyValues;

	// facet = 1
	private long[][] areaValues;
	private long[][] areaStrategyValues;

	private long explorationPotentialPivot = 0;
	private long areaPivot = 0;
	private long valid_cells = -1;
	private Color[] colors = null;
	private int strategy_count;

	public TongueDiagram() {
	}

	public void initTongueModel() {
		step_quantity = Integer.parseInt(Loader.getPropertyValue("step_quantity"));
		preservedState = Integer.parseInt(Loader.getPropertyValue("exploration_potential_max"));

		
		
		explorationPotentialValues = new long[step_quantity][2]; // duas classes, preservado e antropizado
		areaValues = new long[step_quantity][2];

		valid_cells = 0;
		explorationPotentialPivot = 0;
		areaPivot = 0;

		String choosen_game = Loader.getPropertyValue("game");
		strategy_count = Integer.parseInt(Loader.getPropertyValue(choosen_game + "_strategy_count"));
		exploration_per_step = new int[strategy_count];

		explorationPotentialStrategyValues = new long[step_quantity][strategy_count];
		areaStrategyValues = new long[step_quantity][strategy_count];

		
		for (int i = 0; i < strategy_count; i++) {
			exploration_per_step[i] = Integer
					.parseInt(Loader.getPropertyValue(choosen_game + "_exploration_per_step_strat_" + i));
		}

		if (colors == null) {
			colors = new Color[strategy_count];
			for (int i = 0; i < strategy_count; i++) {
				colors[i] = new Color(
						Integer.parseInt(Loader.getPropertyValue(choosen_game + "_strategy_" + i + "_color"), 16));
			}
		}
	}

	public void updateGraph(int curr_step, String[] strategy_names, int[][] cells, int owner_type[][]) {
		if (step_quantity == 0) {
			initTongueModel();
		}
		this.strategy_names = strategy_names;

		long count = 0;
		for (int i = 0; i < cells.length; i++) {
			for (int j = 0; j < cells[i].length; j++) {
				int state = cells[i][j];
				if (state >= 0) {
					count = count + 1;
					if (state == preservedState) {
						explorationPotentialValues[curr_step - 1][0] += state;
						areaValues[curr_step - 1][0] += 1;
					} else {
						explorationPotentialValues[curr_step - 1][1] += state;
						areaValues[curr_step - 1][1] += 1;
					}
					if (owner_type[i][j] >= 0) {
						explorationPotentialStrategyValues[curr_step
								- 1][owner_type[i][j]] += exploration_per_step[owner_type[i][j]];
						areaStrategyValues[curr_step - 1][owner_type[i][j]] += 1;
					}
				}

			}
		}
		if (valid_cells <= 0) {
			valid_cells = count;
		}
		if (explorationPotentialPivot == 0 || areaPivot == 0) {
			explorationPotentialPivot = explorationPotentialValues[0][1];
			areaPivot = areaValues[0][1];
		}

		repaint();
	}

	public void paintComponent(Graphics g) {

		Graphics2D g2 = (Graphics2D) g;
		Dimension d = this.getSize();
		height = (int) d.getHeight();
		width = (int) d.getWidth();
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, width, height);

		g.setColor(Color.BLACK);
		g.drawString(String.format(strNrml_graphName, facetName[facet]), 10, 15);

		int padding_x = (int) (0.1 * width);
		int padding_y = (int) (0.2 * height);

		int x_axis_i = (padding_x);
		int x_axis_j = width - padding_x;
		int y_axis_i = height - padding_y;
		int y_axis_j = padding_y;

		g.setColor(Color.BLACK);
		g2.setStroke(new BasicStroke(1));
		g.drawLine(x_axis_i, y_axis_i, x_axis_j, y_axis_i);
		g.drawLine(x_axis_i, y_axis_i, x_axis_i, y_axis_j);

		if (valid_cells <= 0 || step_quantity == 0)
			return;

		double virtual_height = (height - (2 * padding_y));
		double horizontal_factor = (width - (2 * padding_x)) / ((double) step_quantity);

		for (int i = 1; i < step_quantity - 1; i++) {
			double curr_preserved = 0;
			double curr_explored = 0;

			if (facet == 0) {
				curr_preserved = (explorationPotentialValues[i][0] / ((double) (preservedState * valid_cells)));
				curr_explored = (explorationPotentialValues[i][1] / ((double) (preservedState * valid_cells)));
			} else {
				curr_preserved = (areaValues[i][0] / ((double) valid_cells));
				curr_explored = (areaValues[i][1] / ((double) valid_cells));
			}

			int x_i = padding_x + (int) Math.round((i - 1) * horizontal_factor);
			int y_i = (height - padding_y) - (int) (Math.round(curr_preserved * virtual_height));
			int x_j = padding_x + (int) Math.round((i) * horizontal_factor);
			int y_j = (height - padding_y) - (int) (Math.round(curr_explored * virtual_height));

			if (curr_preserved >= curr_explored) {
				g.setColor(Color.GREEN);
				g.drawRect(x_i, y_i, x_j - x_i, y_j - y_i);
			} else {
				g.setColor(Color.GRAY);
				g.drawRect(x_i, y_i, x_j - x_i, y_i - y_j);
			}

			for (int j = 0; j < strategy_count; j++) {

				double past_explored_by_agents = 0;
				double curr_explored_by_agents = 0;
				if (facet == 0) {
					past_explored_by_agents = ((explorationPotentialPivot
							+ explorationPotentialStrategyValues[i - 1][j])
							/ ((double) (preservedState * valid_cells)));
					curr_explored_by_agents = ((explorationPotentialPivot + explorationPotentialStrategyValues[i][j])
							/ ((double) (preservedState * valid_cells)));
				} else {
					past_explored_by_agents = ((areaValues[i - 1][j]) / ((double) valid_cells));
					curr_explored_by_agents = ((areaValues[i][j]) / ((double) valid_cells));
				}

				int a_i = padding_x + (int) Math.round((i - 1) * horizontal_factor);
				int b_i = (height - padding_y) - (int) (Math.round(past_explored_by_agents * virtual_height));
				int a_j = padding_x + (int) Math.round((i) * horizontal_factor);
				int b_j = (height - padding_y) - (int) (Math.round(curr_explored_by_agents * virtual_height));
				g.setColor(colors[j]);
				g2.setStroke(new BasicStroke(3));
				g2.drawLine(a_i, b_i, a_j, b_j);
			}
		}
		// desenhando a legenda
		int subpad_y = (int) (0.1 * height);
		int side = 15;
		int sub_y_axis_i = height - subpad_y;
		int leg_jump = (int) ((horizontal_factor * step_quantity) / colors.length);
		for (int a = 0; a < colors.length; a++) {
			g.setColor(colors[a]);
			g.fillRect(padding_x + a * leg_jump, sub_y_axis_i, side, side);
			g.setColor(Color.BLACK);
			g.drawString(strategy_names[a], padding_x + a * leg_jump + 20, sub_y_axis_i + 10);
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}
}
