package main.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

public class ConfigurationScreen implements MouseListener {

	private JFrame jframe;
	private JButton jbutton_saveAndDeploy;
	private JButton jbutton_cancel;

	private JTabbedPane tabs;
	private BasicConfigurationPanel panel_basic;
	private JPanel panel_space;
	private JPanel panel_agents;
	private JPanel panel_time;
	private JPanel panel_games;
	private JPanel panel_view;

	public ConfigurationScreen() {
		jframe = new JFrame();
		jframe.setTitle("Configuration");
		jframe.setSize(new Dimension(800, 600));
		jframe.setLayout(new BorderLayout());
		jframe.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		jframe.setVisible(false);

		initializeTabs();
		initializeButtons();
	}

	public void makeVisible() {
		jframe.setVisible(true);
	}

	private void initializeTabs() {
		tabs = new JTabbedPane();
		initializePanelBasic();
		initializePanelSpace();
		initializePanelAgents();
		initializePanelTime();
		initializePanelGames();
		initializePanelView();
		jframe.getContentPane().add(tabs, BorderLayout.CENTER);
	}

	private void initializeButtons() {
		JPanel buttonContainer = new JPanel();
		buttonContainer.setLayout(new FlowLayout(FlowLayout.CENTER));

		jbutton_cancel = new JButton("Cancel");
		jbutton_cancel.addMouseListener(this);
		buttonContainer.add(jbutton_cancel);

		jbutton_saveAndDeploy = new JButton("Save & Deploy");
		jbutton_saveAndDeploy.addMouseListener(this);
		buttonContainer.add(jbutton_saveAndDeploy);

		jframe.getContentPane().add(buttonContainer, BorderLayout.SOUTH);
	}

	private void initializePanelBasic() {
		panel_basic = new BasicConfigurationPanel();
		tabs.addTab("Basic", panel_basic);
	}

	private void initializePanelSpace() {
		panel_space = new JPanel();
		tabs.addTab("Space", panel_space);
	}

	private void initializePanelAgents() {
		panel_agents = new JPanel();
		tabs.addTab("Agents", panel_agents);
	}

	private void initializePanelTime() {
		panel_time = new JPanel();
		tabs.add("Time", panel_time);
	}

	private void initializePanelGames() {
		panel_games = new JPanel();
		tabs.add("Games", panel_games);
	}

	private void initializePanelView() {
		panel_view = new JPanel();
		tabs.add("View", panel_view);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
		if (e.getSource().equals(jbutton_cancel)) {
			jframe.setVisible(false);
		} else if (e.getSource().equals(jbutton_saveAndDeploy)) {

		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	// just for testing
	public static void main(String[] args) {
		ConfigurationScreen cs = new ConfigurationScreen();
		cs.jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		cs.jframe.setVisible(true);

	}

}
