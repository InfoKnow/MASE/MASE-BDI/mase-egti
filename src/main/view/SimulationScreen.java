package main.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JPanel;

import main.loader.Loader;

public class SimulationScreen extends JPanel {
	public static final String WAITING = "waiting";
	public static final String RUNNING = "running";
	public static final String PAUSED = "paused";
	public static final String STOPPED = "stopped";

	private static final long serialVersionUID = 1L;
	private int rows;
	private int columns;
	private int height;
	private int width;
	private int cellHeight;
	private int cellWidth;
	private String strNrml_simulation = "Simulation: %s";
	private String state = "waiting";
	private Color colors[] = null;

	private int[][] cells = null;
	private int[][] owned;

	public void setCells(int agent_quantity, int curr_step, int step_quantity, int[][] cells, int[][] owned) {
		if (colors == null) {
			String game = Loader.getPropertyValue("game");
			int aux_strategies = Integer.parseInt(Loader.getPropertyValue(game + "_strategy_count"));
			colors = new Color[aux_strategies];
			for (int i = 0; i < aux_strategies; i++) {
				colors[i] = new Color(
						Integer.parseInt(Loader.getPropertyValue(game + "_strategy_" + i + "_color"), 16));
			}
		}
		this.cells = cells;
		rows = cells.length;
		columns = cells[0].length;
		this.owned = owned;
		repaint();
	}

	public void paintComponent(Graphics g) {
		Dimension d = this.getSize();
		height = (int) d.getHeight();
		width = (int) d.getWidth();

		cellHeight = 1;
		cellWidth = 1;

		g.setColor(Color.black);

		for (int a = 0; a < height; a++) {
			for (int b = 0; b < width; b++) {
				g.fillRect((b * cellHeight), (a * cellWidth), cellHeight, cellWidth);
			}
		}
		g.setColor(Color.WHITE);
		// g.setFont(font);

		if (cells != null) {

			int delta_x = (width - columns) / 2;
			int delta_y = (height - rows) / 2;
			int i = 0;
			int j = 0;
			for (i = 0; i < rows; i++) {
				for (j = 0; j < columns; j++) {

					int curr_exploration = cells[i][j];
					Color c = Color.BLACK;
					if (owned[i][j] != -1) {
						c = colors[owned[i][j]];
					} else if (curr_exploration == 1500) {
						c = new Color(0, 255, 0);
					} else if (curr_exploration > 0) {
						c = new Color(255, 255, 0);
					}

					g.setColor(c);
					g.fillRect(delta_x + (j * cellHeight), delta_y + (i * cellWidth), cellHeight, cellWidth);
				}
			}
		}

		g.setColor(Color.GREEN);
		g.drawString(String.format(strNrml_simulation, state), 5, height - 10);
	}

	public void updateState(String state) {
		this.state = state;
		repaint();
	}
}
