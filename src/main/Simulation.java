package main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import main.loader.CommonBeliefDB;
import main.loader.JSONLoader;
import main.stats.StatsCollector;
import spatial.Space;

public class Simulation {
	private static Simulation instance;
	private long step_time_t0;
	private boolean has_manager = false;
	private boolean has_agents = false;
	private int image_generation_interval;

	public static void initSimulation() {
		instance = new Simulation();
	}

	public static Simulation getInstance() {
		return instance;
	}

	private Simulation() {
		initAll();
	}

	private void initAll() {
		CommonBeliefDB.initOneTimeCommonBeliefDB();
		CommonBeliefDB.initRecurrentCommonBeliefDB();
		image_generation_interval = Integer
				.parseInt(JSONLoader.getInstance().getPropertyValue("mase.imageGenerationInterval"));
		Space.initSpace();
		initAgents();
	}

	private void reinitVariables() {
		System.out.println("restarting platform");
		Platform.getInstance().stopPlatform();
		Platform.initPlatform();
		StatsCollector.initStatsCollector();
		CommonBeliefDB.initRecurrentCommonBeliefDB();
		Space.initSpace();
		initAgents();
	}

	private void initAgents() {
		Platform p = Platform.getInstance();
		String manager_class = CommonBeliefDB.getManager_class();
		if (manager_class != null && !manager_class.equals("none")) {
			Map<String, Object> agParam = new HashMap<String, Object>();
			agParam.put("index", -1);
			p.deployAgent(manager_class, agParam);
			has_manager = true;
		}

		String agent_class = CommonBeliefDB.getAgent_class();
		if (agent_class != null && !agent_class.equals("none")) {
			has_agents = true;
			int agent_quantity = CommonBeliefDB.getAgent_quantity();
			
			
			for (int i = 0; i < agent_quantity; i++) {
				Map<String, Object> agParam = new HashMap<String, Object>();
				agParam.put("index", i);
				int type = CommonBeliefDB.getStrategy_distribution(i);
				
				agParam.put("type", type);
				p.deployAgent(agent_class, agParam);
			}
			
			
		}

	}

	public void startSimulation() {
		CommonBeliefDB.increaseSimulation();
		startStep();
	}

	public void startStep() {
		System.out.println(">>>>>>>>>> Step " + CommonBeliefDB.getCurrStep() + " START! <<<<<<<<<<");
		step_time_t0 = System.currentTimeMillis();
		CommonBeliefDB.resetFinishedAgents();

		int[] agent_list = null;
		int i = 0;
		if (has_manager && has_agents) {
			agent_list = new int[CommonBeliefDB.getAgent_quantity() + 1];
			for (; i < CommonBeliefDB.getAgent_quantity(); i++) {
				agent_list[i] = i;
			}
			agent_list[i] = -1;
		} else if (has_manager) {
			agent_list = new int[1];
			agent_list[i] = -1;
		} else if (has_agents) {
			agent_list = new int[CommonBeliefDB.getAgent_quantity()];
			for (; i < CommonBeliefDB.getAgent_quantity(); i++) {
				agent_list[i] = i;
			}
		}
		CommonBeliefDB.broadcastMessage(agent_list, CommonBeliefDB.PLEASE_START_MESSAGE, "");
	}

	public void endStep() {
		System.out.println("============ Step " + CommonBeliefDB.getCurrStep() + " END! ============");
		long delta = Math.abs(step_time_t0 - System.currentTimeMillis());

		StatsCollector.getInstance().setStepTime(delta);
		StatsCollector.getInstance().collectAndWrite();
		if (CommonBeliefDB.getCurrStep() % image_generation_interval == 0) {
			//StatsCollector.getInstance().generateImage(null);
			//StatsCollector.getInstance().generateConflictImage(null);
		}
		CommonBeliefDB.increaseStep();

		if (CommonBeliefDB.isSimulationDone()) {
			endSimulation(false);
		} else {
			startStep();
		}
	}

	public void endSimulation(boolean failed) {
		StatsCollector.getInstance().generateImage(CommonBeliefDB.getStepQuantity());
		//StatsCollector.getInstance().generateConflictImage(CommonBeliefDB.getStepQuantity());
		if (failed) {
			System.out.println("Simulation failed!");

		} else {
			System.out.println("Simulation ended!");
		}
		if (CommonBeliefDB.areOverallSimulationsDone()) {
			System.out.println("All simulations ended!");
			System.exit(0);
		} else {
			restartSimulation();
		}
	}

	private void restartSimulation() {
		// killAgents();
		reinitVariables();
		startSimulation();
	}

	public void killAgents() {
		int[] agent_list = null;
		int i = 0;
		if (has_manager && has_agents) {
			agent_list = new int[CommonBeliefDB.getAgent_quantity() + 1];
			for (; i < CommonBeliefDB.getAgent_quantity(); i++) {
				agent_list[i] = i;
			}
			agent_list[i] = -1;
		} else if (has_manager) {
			agent_list = new int[1];
			agent_list[i] = -1;
		} else if (has_agents) {
			agent_list = new int[CommonBeliefDB.getAgent_quantity()];
			for (; i < CommonBeliefDB.getAgent_quantity(); i++) {
				agent_list[i] = i;
			}
		}
		CommonBeliefDB.broadcastMessage(agent_list, CommonBeliefDB.PLEASE_END_EXECUTION, "");
	}

}
