
package main.loader;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Set;

import main.Main;

//TODO break this class to load any model
public class Loader {
	private static HashMap<String, String> configs;
	private static Set<String> keys;
	private static String iniPath = "conf/";
	private static String iniExt = ".ini";
	private static String modelsPath = "models";
	private static String gamesPath = "games";
	private static String general_configs = "general_";
	private static String space_configs = "space_";
	private static String agents_configs = "agents_";

	public static void initConfiguration(String model) {
		String choosen_model = model;
		String choosen_game = null;
		configs = new HashMap<>();
		if (model == null) {
			processFile(iniPath + modelsPath + iniExt, "Arquivo models.ini nao encontrado", null);
			choosen_model = configs.get("model_1");
		}

		Main.getInstance().getInformationScreen().appendToInfoArea("The choosen model is " + choosen_model);

		// abrindo os arquivos [general], [space] e [agents]
		processFile(iniPath + general_configs + choosen_model + iniExt,
				"Arquivo " + general_configs + choosen_model + " nao encontrado", null);
		processFile(iniPath + space_configs + choosen_model + iniExt,
				"Arquivo " + space_configs + choosen_model + " nao encontrado", null);
		processFile(iniPath + agents_configs + choosen_model + iniExt,
				"Arquivo " + agents_configs + choosen_model + " nao encontrado", null);
		// agora vamos definir o jogo
		choosen_game = configs.get("game");

		Main.getInstance().getInformationScreen().appendToInfoArea("The choosen game is " + choosen_game);

		processFile(iniPath + gamesPath + iniExt, "File games.ini was not found!", choosen_game);

	}

	public static void processFile(String path, String exceptionStr, String substringStartsWith) {
		try {
			BufferedReader br = new BufferedReader(new FileReader(path));
			String aux = null;
			while ((aux = br.readLine()) != null) {
				if (aux.trim().length() == 0 || aux.startsWith("%")) {
					continue;
				}

				if (substringStartsWith != null && !aux.startsWith(substringStartsWith)) {
					continue;
				}

				String[] available_models = aux.split("=");
				configs.put(available_models[0].trim(), available_models[1].trim());
			}
			keys = configs.keySet();
			br.close();
		} catch (Exception e) {
			Main.getInstance().getInformationScreen().appendToInfoArea(exceptionStr);
			e.printStackTrace();
		}
	}

	public static String getPropertyValue(String property) {
		if (keys.contains(property.trim())) {
			return configs.get(property.trim());
		}
		return null;
	}
}
