package main.loader;

import java.io.File;

public class SpatialLoader {

	private static SpatialLoader instance;
	private int max_cell_value = 0;
	private String model_path = null;
	private String[] observed_space_sources = null;
	private String[] space_values = null;
	private String[] space_factors = null;
	private String[] spatial_attributes_sources = null;
	private String[] spatial_attributes_factors = null;
	private String publicPolicy = null;
	private String[] public_policy_factors = null;
	private String[] public_policy_values = null;

	private String sep = " ";

	public static SpatialLoader getInstance() {

		if (instance == null) {
			instance = new SpatialLoader();
		}

		return instance;
	}

	public void flushValues() {
		max_cell_value = 0;
		model_path = null;
		observed_space_sources = null;
		space_values = null;
		space_factors = null;
		spatial_attributes_sources = null;
		spatial_attributes_factors = null;
		publicPolicy = null;
		public_policy_factors = null;
		public_policy_values = null;
	}

	public int getMaxCellValue() {
		if (max_cell_value == 0) {
			int countPreservedValue = 0;
			while (JSONLoader.getInstance().getPropertyValue(
					String.format("space.initial_observed_space.classes.%d.value", countPreservedValue)) != null) {
				if (max_cell_value < Integer.parseInt(JSONLoader.getInstance().getPropertyValue(
						String.format("space.initial_observed_space.classes.%d.value", countPreservedValue)))) {
					max_cell_value = Integer.parseInt(JSONLoader.getInstance().getPropertyValue(
							String.format("space.initial_observed_space.classes.%d.value", countPreservedValue)));
				}
				countPreservedValue++;
			}
		}
		return max_cell_value;
	}

	public int getMinCellValue() {
		return 0;
	}

	public String getModelPath() {
		if (model_path == null) {
			model_path = JSONLoader.getInstance().getPropertyValue("space.model_path");
		}
		return model_path;
	}

	public String[] getObservedSpaceSources() {
		if (observed_space_sources == null) {
			observed_space_sources = new String[2];
			observed_space_sources[0] = getModelPath() + File.separator
					+ JSONLoader.getInstance().getPropertyValue("space.initial_observed_space.source");
			observed_space_sources[1] = getModelPath() + File.separator
					+ JSONLoader.getInstance().getPropertyValue("space.final_observed_space.source");
		}
		return observed_space_sources;
	}

	public String[] getSpaceValues() {
		if (space_values == null) {
			String result = "";
			int count = 0;
			while (JSONLoader.getInstance()
					.getPropertyValue(String.format("space.initial_observed_space.classes.%d.value", count)) != null) {
				result = result + JSONLoader.getInstance()
						.getPropertyValue(String.format("space.initial_observed_space.classes.%d.value", count)) + sep;
				count++;
			}

			result = result.trim();
			space_values = result.split(sep);
		}
		return space_values;
	}

	public String[] getSpaceFactors() {
		if (space_factors == null) {
			String result = "";
			int count = 0;
			while (JSONLoader.getInstance()
					.getPropertyValue(String.format("space.initial_observed_space.classes.%d.weight", count)) != null) {
				result = result + JSONLoader.getInstance()
						.getPropertyValue(String.format("space.initial_observed_space.classes.%d.weight", count)) + sep;
				count++;
			}

			result = result.trim();
			space_factors = result.split(sep);
		}
		return space_factors;
	}

	public String getStrSpatialAttributesSources() {
		String result = "";
		int count = 0;
		while (JSONLoader.getInstance()
				.getPropertyValue(String.format("space.spatial_attributes.%d.source", count)) != null) {
			result = result + getModelPath() + File.separator + JSONLoader.getInstance()
					.getPropertyValue(String.format("space.spatial_attributes.%d.source", count)) + sep;
			count++;
		}
		return result.trim();
	}

	public String[] getSpatialAttributesSources() {
		if (spatial_attributes_sources == null) {
			spatial_attributes_sources = getStrSpatialAttributesSources().split(sep);
		}
		return spatial_attributes_sources;
	}

	public String[] getSpatialAttributesFactors() {
		if (spatial_attributes_factors == null) {
			int count = getSpatialAttributesSources().length;
			spatial_attributes_factors = new String[count];
			for (int i = 0; i < count; i++) {
				spatial_attributes_factors[i] = JSONLoader.getInstance()
						.getPropertyValue(String.format("space.spatial_attributes.%d.weight", i));
			}
			return spatial_attributes_factors;
		}
		return spatial_attributes_factors;
	}

	// TODO transformar em array
	public String getPublicPolicy() {
		if (publicPolicy == null) {
			publicPolicy = getModelPath() + File.separator
					+ JSONLoader.getInstance().getPropertyValue("space.public_policies.0.source");
		}

		return publicPolicy;
	}

	public String[] getPublicPolicyFactors() {
		if (public_policy_factors == null) {
			String result = "";
			int count = 0;
			while (JSONLoader.getInstance()
					.getPropertyValue(String.format("space.public_policies.0.classes.%d.factor", count)) != null) {
				result = result + JSONLoader.getInstance()
						.getPropertyValue(String.format("space.public_policies.0.classes.%d.factor", count)) + sep;
				count++;
			}

			result = result.trim();
			public_policy_factors = result.split(sep);
		}
		return public_policy_factors;
	}

	public String[] getPublicPolicyValues() {
		if (public_policy_values == null) {
			String result = "";
			int count = 0;
			while (JSONLoader.getInstance()
					.getPropertyValue(String.format("space.public_policies.0.classes.%d.value", count)) != null) {
				result = result + JSONLoader.getInstance()
						.getPropertyValue(String.format("space.public_policies.0.classes.%d.value", count)) + sep;
				count++;
			}

			result = result.trim();
			public_policy_values = result.split(sep);
		}
		return public_policy_values;
	}

	public static void main(String[] args) {

	}

}
