package main.loader;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import game.Game;

public class JSONLoader {

	/* vari�vel com o objeto de configura��o em si */
	private static JSONLoader instance;

	/* vari�veis defaults */
	private String mase_config_default_path = "conf/mase.json";
	private static String model_config_default_path = "conf/cerrado_df.json";
	private String games_config_default_path = "conf/games.json";
	private String batch_config_default_path = "conf/batch.json";

	/* vari�veis com as configura��es em si */
	private HashMap<String, Object> configuration_values_configs;
	private HashMap<String, Object> initial_setting_configs;
	private HashMap<String, Object> interaction_steps_configs;
	private HashMap<String, Object> halting_conditions_configs;
	private HashMap<String, Object> debug_configs;

	/* vari�vel especial com a especifica��o dos jogos */
	private ArrayList<Game> games;

	/* vari�veis com configura��es pontuais */
	private boolean window_mode = false;
	private boolean batch_mode = false;
	private boolean debug_mode = false;
	private int n_simulations = 0;

	public static void initInstance() {
		instance = new JSONLoader(null);
	}

	public static void initInstance(HashMap<String, Object> configuration_values_configs) {
		instance = new JSONLoader(configuration_values_configs);
	}

	public static JSONLoader getInstance() {
		return instance;
	}

	// Passos
	// 1. Ler e carregar o arquivo mase.json
	// 2. Verificar se MASE vai entrar em Window mode ou batch mode
	// 3. Carregar os tipos de jogos
	// 4. Carregar o modelo a ser utilizado
	// 5. Carregar as vari�veis de batch e de debug
	private JSONLoader(HashMap<String, Object> additional_initial_configuration_values_configs) {
		if (configuration_values_configs != null) {
			// a leitura j� foi feita. Pode sair do m�todo
			return;
		}
		if (additional_initial_configuration_values_configs == null) {
			this.configuration_values_configs = new HashMap<>();
		} else {
			this.configuration_values_configs = additional_initial_configuration_values_configs;
		}
		initial_setting_configs = new HashMap<>();
		interaction_steps_configs = new HashMap<>();
		halting_conditions_configs = new HashMap<>();
		debug_configs = new HashMap<>();
		games = new ArrayList<>();
		try {
			// 1. Ler e carregar o arquivo mase.json
			loadMaseConfig();
			// 2. Verificar se MASE vai entrar em Window mode ou batch mode
			verifyBatchAndDebugMode();
			// 3. Carregar os tipos de jogos
			loadGame();
			// 4. Carregar o modelo a ser utilizado
			loadModel();
			// 5. Carregar as vari�veis de batch e de debug
			if (batch_mode) {
				loadBatch();
			}

			if (debug_mode) {
				loadDebug();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setProperty(String key, String property) {

		Set<String> keys = configuration_values_configs.keySet();
		for (String k : keys) {
			if (k.equals(key)) {
				configuration_values_configs.put(key, property);
				return;
			}
		}

		keys = initial_setting_configs.keySet();
		for (String k : keys) {
			if (k.equals(key)) {
				initial_setting_configs.put(key, property);
				return;
			}
		}

		keys = interaction_steps_configs.keySet();
		for (String k : keys) {
			if (k.equals(key)) {
				interaction_steps_configs.put(key, property);
				return;
			}
		}

		keys = halting_conditions_configs.keySet();
		for (String k : keys) {
			if (k.equals(key)) {
				halting_conditions_configs.put(key, property);
				return;
			}
		}

		keys = debug_configs.keySet();
		for (String k : keys) {
			if (k.equals(key)) {
				debug_configs.put(key, property);
				return;
			}
		}
	}

	public void loadMaseConfig() throws Exception {
		File f = new File(mase_config_default_path);
		BufferedReader br = new BufferedReader(new FileReader(f));
		StringBuffer sb = new StringBuffer();
		String aux = null;
		while ((aux = br.readLine()) != null) {
			sb.append(aux);
		}
		br.close();
		JSONObject obj = new JSONObject(sb.toString());
		Set<String> maseKeys = obj.keySet();
		for (String key : maseKeys) {
			JSONObject maseObj = obj.getJSONObject(key);
			Set<String> subkeys = maseObj.keySet();
			for (String subkey : subkeys) {
				Object value = maseObj.get(subkey);
				if (!configuration_values_configs.containsKey(String.format("%s.%s", key, subkey))) {
					configuration_values_configs.put(String.format("%s.%s", key, subkey), value);
				}
			}
		}
	}

	public void verifyBatchAndDebugMode() {
		if (Boolean.parseBoolean((String) configuration_values_configs.get("mase.batch"))) {
			batch_mode = true;
			window_mode = false;
		} else {
			batch_mode = false;
			window_mode = true;
		}

		if (Boolean.parseBoolean((String) configuration_values_configs.get("mase.debug"))) {
			debug_mode = true;
		} else {
			debug_mode = false;
		}
	}

	public void loadGame() throws Exception {
		File f = new File(games_config_default_path);
		BufferedReader br = new BufferedReader(new FileReader(f));
		StringBuffer sb = new StringBuffer();
		String aux = null;
		while ((aux = br.readLine()) != null) {
			sb.append(aux);
		}
		br.close();
		JSONObject obj = new JSONObject(sb.toString());
		Set<String> gameKeys = obj.keySet();
		games = new ArrayList<>();
		for (String gameKey : gameKeys) {
			JSONObject aGame = obj.getJSONObject(gameKey);
			int strategy_count = aGame.getInt("strategy_count");
			Game theGame = new Game(gameKey, strategy_count);
			JSONArray strategies = aGame.getJSONArray("strategies");
			for (int i = 0; i < strategies.length(); i++) {
				String label = ((JSONObject) (strategies.get(i))).getString("label");
				int index = ((JSONObject) (strategies.get(i))).getInt("index");
				JSONArray aPayoffs = ((JSONObject) (strategies.get(i))).getJSONArray("payoffs");
				double[] payoff = new double[strategy_count];
				for (int j = 0; j < strategy_count; j++) {
					payoff[j] = ((JSONObject) aPayoffs.get(0)).getInt("" + j);
				}
				Color color = new Color(Integer.parseInt(((JSONObject) (strategies.get(i))).getString("color"), 16));
				int exploration = ((JSONObject) (strategies.get(i))).getInt("exploration");
				String enable_condition = ((JSONObject) (strategies.get(i))).getString("enable_condition");
				String disable_condition = ((JSONObject) (strategies.get(i))).getString("disable_condition");
				theGame.setStrategy(index, label, payoff, color, exploration, enable_condition, disable_condition);
			}
			games.add(theGame);
		}
	}

	public void loadModel() throws Exception {
		if (configuration_values_configs != null && configuration_values_configs.get("mase.default_model") != null) {
			model_config_default_path = (String) configuration_values_configs.get("mase.default_model");
		}
		File f = new File(model_config_default_path);
		BufferedReader br = new BufferedReader(new FileReader(f));
		StringBuffer sb = new StringBuffer();
		String aux = null;
		while ((aux = br.readLine()) != null) {
			sb.append(aux);
		}
		br.close();
		JSONObject obj = new JSONObject(sb.toString());
		Set<String> keys = obj.keySet();
		for (String key : keys) {
			JSONObject subobj = obj.getJSONObject(key);
			switch (key) {
			case "general":
				processGeneralConf(key, subobj);
				break;
			case "space":
				processSpaceConf(key, subobj);
				break;
			case "agents":
				processAgentsConf(key, subobj);
				break;
			case "games":
				processGamesConf(key, subobj);
				break;
			}
		}
	}

	public void processGeneralConf(String key, JSONObject obj) {
		String model_name = obj.getString("model_name");
		configuration_values_configs.put(key + ".model_name", model_name);
		String author = obj.getString("author");
		configuration_values_configs.put(key + ".author", author);
		String description = obj.getString("description");
		configuration_values_configs.put(key + ".description", description);
	}

	public void processSpaceConf(String key, JSONObject obj) {
		String aux = "model_path";
		Object object = obj.get(aux);
		configuration_values_configs.put(String.format("%s.%s", key, aux), object);

		String[] aux1 = new String[] { "initial_observed_space", "final_observed_space" };
		JSONObject initial_observed_space = obj.getJSONObject(aux1[0]);
		String subaux = "source";
		object = initial_observed_space.get(subaux);
		configuration_values_configs.put(String.format("%s.%s.%s", key, aux1[0], subaux), object);

		JSONObject final_observed_space = obj.getJSONObject(aux1[1]);
		object = final_observed_space.get(subaux);
		configuration_values_configs.put(String.format("%s.%s.%s", key, aux1[1], subaux), object);

		String ssubaux = "classes";
		JSONArray[] arrays = new JSONArray[] { ((JSONArray) initial_observed_space.get(ssubaux)),
				((JSONArray) final_observed_space.get(ssubaux)) };
		String[] sssubaux = new String[] { "color", "value", "label", "weight" };
		for (int h = 0; h < arrays.length; h++) {
			for (int i = 0; i < arrays[h].length(); i++) {
				for (int j = 0; j < sssubaux.length; j++) {
					object = ((JSONObject) arrays[h].get(i)).get(sssubaux[j]);
					configuration_values_configs
							.put(String.format("%s.%s.%s.%s.%s", key, aux1[h], ssubaux, i, sssubaux[j]), object);
				}
			}
		}

		aux = "spatial_attributes";
		JSONArray spatial_attributes = obj.getJSONArray(aux);
		sssubaux = new String[] { "source", "weight", "label" };
		for (int i = 0; i < spatial_attributes.length(); i++) {
			for (int j = 0; j < sssubaux.length; j++) {
				object = ((JSONObject) spatial_attributes.get(i)).get(sssubaux[j]);
				configuration_values_configs.put(String.format("%s.%s.%s.%s", key, aux, i, sssubaux[j]), object);
			}
		}

		aux = "public_policies";
		JSONArray public_policies = obj.getJSONArray(aux);
		for (int i = 0; i < public_policies.length(); i++) {
			subaux = "source";
			object = ((JSONObject) public_policies.get(i)).get(subaux);
			configuration_values_configs.put(String.format("%s.%s.%s.%s", key, aux, i, subaux), object);
			ssubaux = "classes";
			JSONArray classes = ((JSONObject) public_policies.get(i)).getJSONArray(ssubaux);
			sssubaux = new String[] { "color", "value", "factor", "label" };
			for (int j = 0; j < classes.length(); j++) {
				for (int k = 0; k < sssubaux.length; k++) {
					object = ((JSONObject) classes.get(j)).get(sssubaux[k]);
					configuration_values_configs
							.put(String.format("%s.%s.%s.%s.%s.%s", key, aux, i, ssubaux, j, sssubaux[k]), object);
				}
			}
		}

		aux = "height";
		object = obj.get(aux);
		configuration_values_configs.put(String.format("%s.%s", key, aux), object);

		aux = "width";
		object = obj.get(aux);
		configuration_values_configs.put(String.format("%s.%s", key, aux), object);

		aux = "cell_dimensions";
		JSONObject cell_dimensions = obj.getJSONObject(aux);
		subaux = "height";
		object = cell_dimensions.get(subaux);
		configuration_values_configs.put(String.format("%s.%s.%s", key, aux, subaux), object);
		subaux = "width";
		object = cell_dimensions.get(subaux);
		configuration_values_configs.put(String.format("%s.%s.%s", key, aux, subaux), object);

		aux = "step_quantity";
		object = obj.get(aux);
		configuration_values_configs.put(String.format("%s.%s", key, aux), object);
		
		aux = "simulation_quantity";
		object = obj.get(aux);
		configuration_values_configs.put(String.format("%s.%s", key, aux), object);

	}

	public void processAgentsConf(String key, JSONObject obj) {
		String aux[] = { "agent_architecture", "conflict_resolution_type", "manager_class", "agent_quantity",
				"agent_quantity_increase", "agent_quantity_limit", "agent_vision", "agent_class", "init_random" };
		for (int i = 0; i < aux.length; i++) {
			Object object = obj.get(aux[i]);
			configuration_values_configs.put(String.format("%s.%s", key, aux[i]), object);
		}
	}

	public void processGamesConf(String key, JSONObject obj) {
		String aux = "choosen_game";
		Object object = obj.get(aux);
		configuration_values_configs.put(String.format("%s.%s", key, aux), object);

		aux = "initial_distribution";
		JSONArray initial_distribution = obj.getJSONArray(aux);
		String subaux[] = new String[] { "strategy", "distribution" };
		for (int i = 0; i < initial_distribution.length(); i++) {
			for (int j = 0; j < subaux.length; j++) {
				object = ((JSONObject) initial_distribution.get(i)).get(subaux[j]);
				configuration_values_configs.put(String.format("%s.%s.%s.%s", key, aux, i, subaux[j]), object);
			}
		}

		aux = "strategy_change_condition";
		object = obj.get(aux);
		configuration_values_configs.put(String.format("%s.%s", key, aux), object);

		aux = "strategy_change_type";
		object = obj.get(aux);
		configuration_values_configs.put(String.format("%s.%s", key, aux), object);

	}

	public void loadBatch() {
		File f = new File(batch_config_default_path);
		StringBuffer sb = new StringBuffer();
		try {
			BufferedReader br;
			br = new BufferedReader(new FileReader(f));
			String aux = null;
			while ((aux = br.readLine()) != null) {
				sb.append(aux);
			}
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("Arquivo de batch n�o encontrado!");
		} catch (Exception e) {
			e.printStackTrace();
		}

		JSONObject obj = new JSONObject(sb.toString());
		initial_setting_configs = new HashMap<>();
		JSONObject initial_setting_obj = obj.getJSONObject("batch_job").getJSONObject("initial_setting");
		String model_file = initial_setting_obj.getString("model_file");
		model_config_default_path = model_file;
		JSONArray initial_variables = initial_setting_obj.getJSONArray("variables");
		for (int i = 0; i < initial_variables.length(); i++) {
			String key = initial_variables.getJSONObject(i).getString("key");
			Object value = initial_variables.getJSONObject(i).get("value");
			initial_setting_configs.put(key, value);
		}

		/// increase e n
		interaction_steps_configs = new HashMap<>();
		JSONObject interaction_step_obj = obj.getJSONObject("batch_job").getJSONObject("interaction_step");
		n_simulations = interaction_step_obj.getJSONObject("increase_condition").getInt("after_n_simulations");
		JSONArray interaction_variables = interaction_step_obj.getJSONObject("increase_condition")
				.getJSONArray("variables");
		for (int i = 0; i < interaction_variables.length(); i++) {
			String key = interaction_variables.getJSONObject(i).getString("key");
			Object value = interaction_variables.getJSONObject(i).get("value");
			interaction_steps_configs.put(key, value);
		}

		halting_conditions_configs = new HashMap<>();
		JSONObject halting_conditions_obj = obj.getJSONObject("batch_job").getJSONObject("halting_conditions");
		JSONArray halting_variables = halting_conditions_obj.getJSONArray("variables");
		for (int i = 0; i < halting_variables.length(); i++) {
			String key = halting_variables.getJSONObject(i).getString("key");
			Object value = halting_variables.getJSONObject(i).get("value");
			halting_conditions_configs.put(key, value);
		}

	}

	public void loadDebug() throws Exception {
		File f = new File(model_config_default_path);
		BufferedReader br = new BufferedReader(new FileReader(f));
		StringBuffer sb = new StringBuffer();
		String aux = null;
		while ((aux = br.readLine()) != null) {
			sb.append(aux);
		}
		br.close();
		JSONObject obj = new JSONObject(sb.toString());
		debug_configs = new HashMap<>();
		// TODO flexibilizar isso
		JSONObject debug_obj = obj.optJSONObject("debug");
		if (debug_obj != null) {
			Object b = debug_obj.optBoolean("spatial_attributes");
			debug_configs.put("debug.spatial_attributes", b);
		}

	}

	public String getPropertyValue(String key) {
		if (configuration_values_configs.containsKey(key)) {
			return configuration_values_configs.get(key).toString();
		} else {
			return null;
		}
	}

	public Game getGame(String choosen_game) {
		for (Game game : games) {
			if (game.getGame().equals(choosen_game)) {
				return game;
			}
		}
		return null;
	}

	public int getGameIndex(String choosen_game) {
		for (int i = 0; i < games.size(); i++) {
			Game game = games.get(i);
			if (game.getGame().equals(choosen_game)) {
				return i;
			}
		}
		return -1;
	}

	public Game getCurrGame() {
		String choosen_game = this.getPropertyValue("games.choosen_game");
		return getGame(choosen_game);
	}

	public void setCurrGame(Game game) {
		String choosen_game = this.getPropertyValue("games.choosen_game");
		int index = getGameIndex(choosen_game);
		games.set(index, game);
	}

	public boolean is_batch_mode() {
		return batch_mode;
	}

	public boolean is_window_mode() {
		return window_mode;
	}

	public boolean is_debug_mode() {
		return debug_mode;
	}

	public int get_number_of_simulations() {
		return n_simulations;
	}

}
