package main.loader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import agent.feature.AgentSignature;
import game.Game;
import jadex.bridge.IComponentIdentifier;
import main.Platform;
import main.stats.StatsCollector;
import spatial.Space;

public class CommonBeliefDB {
	// public constants
	public static final String DICTATORSHIP_ARCHITECTURE = "dictatorship";
	public static final String NOTARYOFFICER_ARCHITECTURE = "notaryofficer";
	public static final String SELFORGANIZINGAGENTS_ARCHITECTURE = "selforganizingagents";
	public static final String SINGLEMANAGER_ARCHITECTURE = "singlemanager";

	public static final String RESOLVE_FTA = "first_takes_all";
	public static final String RESOLVE_SNG = "stop_n_go";
	public static final String RESOLVE_WFN = "wait_for_neighbours";

	public static final String PLEASE_START_MESSAGE = "PLEASE_START_MESSAGE";
	public static final String I_NEED_A_NEW_CELL = "I_NEED_A_NEW_CELL";
	public static final String PLEASE_VALIDATE_MY_CELL = "PLEASE_VALIDATE_MY_CELL";
	public static final String PLEASE_PROCESS_QUEUE = "PLEASE_PROCESS_MESSAGE";
	public static final String PLEASE_END_EXECUTION = "PLEASE_END_EXECUTION";

	public static final String SEPARATOR = " ";

	public static final String NONE_CHANGE_TYPE = "none";
	public static final String REVENGE_CHANGE_TYPE = "revenge";
	public static final String ACC_PAYOFF_CHANGE_TYPE = "accumulated_payoff";

	// simulation related beliefs
	private static int overall_simulation_count;

	// agent related beliefs
	private static String agent_architecture;
	private static String agent_resolve_method;
	private static String agent_class;
	private static String manager_class;
	private static int agent_quantity;
	private static int agent_quantity_increase;
	private static int agent_quantity_limit;
	private static int agent_vision;
	private static AgentSignature manager_signature;
	private static HashMap<Integer, AgentSignature> signatures;
	private static HashMap<IComponentIdentifier, AgentSignature> aid_signatures;

	// game related beliefs
	private static Game choosen_game;
	private static int strategy_quantity;
	private static int[] strategy_exploration;
	private static int[] strategy_distribution;

	// step related beliefs
	private static int step_quantity;
	private static int simulation_quantity;
	private static int current_step;
	private static int curr_simulation;
	private static ArrayList<Integer> moving_agents;
	private static ArrayList<Integer> finished_agents;
	private static ArrayList<Long> finished_agents_time;

	// for WTN
	private static HashMap<Integer, ArrayList<Integer>> a_waiting_b;
	private static HashMap<Integer, ArrayList<Integer>> b_waiting_a;

	public static void initOneTimeCommonBeliefDB() {
		overall_simulation_count = 0;
		curr_simulation = 0;
	}

	public static void initRecurrentCommonBeliefDB() {
		signatures = new HashMap<>();
		aid_signatures = new HashMap<>();
		populateAgentBeliefs();
		populateGameBeliefs();
		populateStepBeliefs();

	}

	private static void populateAgentBeliefs() {
		agent_architecture = JSONLoader.getInstance().getPropertyValue("agents.agent_architecture");
		agent_resolve_method = JSONLoader.getInstance().getPropertyValue("agents.conflict_resolution_type");
		agent_quantity_increase = Integer
				.parseInt(JSONLoader.getInstance().getPropertyValue("agents.agent_quantity_increase"));
		agent_quantity_limit = Integer
				.parseInt(JSONLoader.getInstance().getPropertyValue("agents.agent_quantity_limit"));
		agent_vision = Integer.parseInt(JSONLoader.getInstance().getPropertyValue("agents.agent_vision"));
		agent_quantity = Integer.parseInt(JSONLoader.getInstance().getPropertyValue("agents.agent_quantity"))
				+ (overall_simulation_count * agent_quantity_increase);
		agent_class = JSONLoader.getInstance().getPropertyValue("agents.agent_class");
		manager_class = JSONLoader.getInstance().getPropertyValue("agents.manager_class");
	}

	
	private static void populateGameBeliefs() {
		String choosen_game_str = JSONLoader.getInstance().getPropertyValue("games.choosen_game");
		choosen_game = JSONLoader.getInstance().getGame(choosen_game_str);

		strategy_quantity = choosen_game.getStrategy_count();

		double strategy_proportion[] = new double[strategy_quantity];
		double diff = agent_quantity;
		for (int i = 0; i < strategy_quantity; i++) {
			strategy_proportion[i] = Double.parseDouble(JSONLoader.getInstance()
					.getPropertyValue(String.format("games.initial_distribution.%d.distribution", i)));
			strategy_proportion[i] = Math.round(strategy_proportion[i] * agent_quantity);
			diff = diff - (int) strategy_proportion[i];
		}
		diff = Math.round(diff);
		if (diff != 0) {
			strategy_proportion[strategy_quantity - 1] = strategy_proportion[strategy_quantity - 1] + diff;
		}
		for (int i = 1; i < strategy_quantity; i++) {
			strategy_proportion[i] = strategy_proportion[i] + strategy_proportion[i - 1];
		}
		/*TODO h� um erro aqui. Do jeito que est�, a propor��o de agentes sempre come�a com 0.5 0.5 agentes*/
		strategy_distribution = new int[agent_quantity];
		int count = 0;
		for (int i = 0; i < agent_quantity; i++) {
			if (i >= strategy_proportion[count]) {
				count++;
			}
			int subindex = ((i*strategy_quantity)+count)%agent_quantity;
			strategy_distribution[subindex] = count;
		}

		String[] possibleStrategies = choosen_game.getAllExplorationPerStep().split(",");
		strategy_exploration = new int[strategy_quantity];
		for (int i = 0; i < strategy_quantity; i++) {
			strategy_exploration[i] = Integer.parseInt(possibleStrategies[i]);
		}

	}

	private static void populateStepBeliefs() {
		step_quantity = (Integer.parseInt(JSONLoader.getInstance().getPropertyValue("space.step_quantity")));
		simulation_quantity = (Integer
				.parseInt(JSONLoader.getInstance().getPropertyValue("space.simulation_quantity")));
		current_step = 0;
		moving_agents = new ArrayList<>();
		finished_agents = new ArrayList<>();
		finished_agents_time = new ArrayList<>();
		a_waiting_b = new HashMap<Integer, ArrayList<Integer>>();
		b_waiting_a = new HashMap<Integer, ArrayList<Integer>>();
		for (int i = 0; i < agent_quantity; i++) {
			a_waiting_b.put(i, new ArrayList<Integer>());
			b_waiting_a.put(i, new ArrayList<Integer>());
		}
	}

	public static void setManagerSignature(AgentSignature signature) {
		manager_signature = signature;
	}

	public static AgentSignature getManagerSignature() {
		return manager_signature;
	}

	public static synchronized void updateAgentSignature(int index, AgentSignature signature) {
		if (index >= 0) {
			signatures.put(index, signature);
			aid_signatures.put(signature.getAgent_aid(), signature);
		} else {
			setManagerSignature(signature);
		}
	}

	public static AgentSignature getAgentSignature(int index) {
		return signatures.get(index);
	}

	public static AgentSignature getAgentSignature(IComponentIdentifier aid) {
		return aid_signatures.get(aid);

	}

	public static synchronized boolean registerFinish(int index, int x, int y) {
		finished_agents.add(index);
		long time = System.currentTimeMillis();
		finished_agents_time.add(time);

		if (agent_resolve_method == RESOLVE_WFN) {
			int[] agent_list = reduceWaitDependencies(index, false);
			String str_agent_list = stringfy(agent_list);
			if (agent_architecture.equals(NOTARYOFFICER_ARCHITECTURE)) {
				agent_list = new int[] { manager_signature.getAgent_id() };
			}
			if (agent_list != null) {
				broadcastMessage(agent_list, CommonBeliefDB.PLEASE_PROCESS_QUEUE, str_agent_list);
			}
		} else if (moving_agents.size() > 0 && moving_agents.size() + finished_agents.size() == agent_quantity) {
			sendMovementOverMessage(index, x, y);
		} else if (finished_agents.size() == agent_quantity) {
			for (int i = 0; i < finished_agents_time.size(); i++) {
				long e = finished_agents_time.get(i);
				e = Math.abs(e - time);
				StatsCollector.getInstance().registerWaitingTime(e);
			}
			return true;
		}

		return false;
	}

	public static int countFinished() {
		return finished_agents.size();
	}

	public static void resetFinishedAgents() {
		finished_agents.clear();
		finished_agents_time.clear();
	}

	public static synchronized void registerMovement(int index, int x, int y) {
		moving_agents.add(index);
		sendMovementOverMessage(index, x, y);

	}

	private static void sendMovementOverMessage(int index, int x, int y) {

		if (agent_resolve_method.equals(RESOLVE_FTA)) {
			return;
		}

		if (agent_resolve_method.equals(RESOLVE_SNG)
				&& moving_agents.size() + finished_agents.size() != agent_quantity) {
			return;
		}

		int[] agent_list = null;
		switch (agent_resolve_method) {
		case RESOLVE_SNG:
			if (agent_architecture.equals(NOTARYOFFICER_ARCHITECTURE)) {
				agent_list = new int[] { manager_signature.getAgent_id() };
			} else if (agent_architecture.equals(SELFORGANIZINGAGENTS_ARCHITECTURE)) {
				agent_list = new int[moving_agents.size()];
				for (int i = 0; i < moving_agents.size(); i++) {
					agent_list[i] = moving_agents.get(i);
				}
			}
			moving_agents.clear();
			System.out.println("index " + index + " sent process queue");
			broadcastMessage(agent_list, CommonBeliefDB.PLEASE_PROCESS_QUEUE, "");
			break;

		case RESOLVE_WFN:

			ArrayList<Integer> near_agents = Space.getInstance().getAgentsNearCell(x, y, getAgent_vision());
			ArrayList<Integer> undefined_agents = new ArrayList<>();
			for (Integer n : near_agents) {
				if (n == index) {
					continue;
				} else if (!finished_agents.contains(n) && !moving_agents.contains(n)) {
					undefined_agents.add(n);
				}
			}
			updateWaitDependencies(index, undefined_agents);
			agent_list = reduceWaitDependencies(index, true);
			String str_agent_list = stringfy(agent_list);

			if (agent_architecture.equals(NOTARYOFFICER_ARCHITECTURE)) {
				agent_list = new int[] { manager_signature.getAgent_id() };
			}

			if (agent_list != null) {
				broadcastMessage(agent_list, CommonBeliefDB.PLEASE_PROCESS_QUEUE, str_agent_list);
			}
			break;
		}

	}

	public static synchronized void updateWaitDependencies(int index, ArrayList<Integer> undefined_agents) {
		if (undefined_agents == null) {
			undefined_agents = new ArrayList<Integer>();
		}
		a_waiting_b.put(index, undefined_agents);

		for (Integer u : undefined_agents) {
			ArrayList<Integer> aux = b_waiting_a.get(u);
			aux.add(index);
			b_waiting_a.put(u, aux);
		}

	}

	public static synchronized int[] reduceWaitDependencies(int index, boolean include_index) {
		int[] result = null;
		ArrayList<Integer> done = new ArrayList<Integer>();
		if (b_waiting_a.get(index) == null || b_waiting_a.get(index).size() == 0) {
			if (include_index) {
				done.add(index);
			}
			ArrayList<Integer> dependents = a_waiting_b.get(index);
			for (Integer d : dependents) {
				ArrayList<Integer> aux = b_waiting_a.get(d);
				aux.remove((Integer) index);
				if (aux.size() == 0) {
					done.add(d);
				}
				b_waiting_a.put(d, aux);
			}

			result = new int[done.size()];
			int counter = 0;
			for (Integer d : done) {
				result[counter] = d;
				counter++;
			}
		}

		return result;
	}

	public static String stringfy(int[] agent_list) {
		String result = "";
		if (agent_list != null) {
			for (int i = 0; i < agent_list.length; i++) {
				result = result + agent_list[i];
				if (i < agent_list.length - 1) {
					result = result + SEPARATOR;
				}
			}
		}
		return result;
	}

	public static int[] destringify(String str_agent_list) {
		String[] arr_agent_list = str_agent_list.split(SEPARATOR);
		int[] result = new int[arr_agent_list.length];
		for (int i = 0; i < arr_agent_list.length; i++) {
			result[i] = Integer.parseInt(arr_agent_list[i]);
		}
		return result;
	}

	public static Integer getMovedAgent(Integer agent) {
		return moving_agents.indexOf(agent);
	}

	public static synchronized void clearMovement() {
		moving_agents.clear();
	}

	public static synchronized void deRegisterMovement(int index) {
		moving_agents.remove(index);
	}

	public static String getAgentArchitecture() {
		return agent_architecture;
	}

	public static int getAgentSignatureSize() {
		return signatures.size();
	}

	public static int getSimulation_count() {
		return overall_simulation_count;
	}

	public static String getAgent_class() {
		return agent_class;
	}

	public static String getAgentResolveMethod() {
		return agent_resolve_method;
	}

	public static String getManager_class() {
		return manager_class;
	}

	public static void setSimulation_count(int simulation_count) {
		CommonBeliefDB.overall_simulation_count = simulation_count;
	}

	public static int getAgent_quantity() {
		return agent_quantity;
	}

	public static int getAgent_quantity_limit() {
		return agent_quantity_limit;
	}

	public static int getStrategy_quantity() {
		return strategy_quantity;
	}

	public static int getAgent_vision() {
		return agent_vision;
	}

	public static Game getChoosenGame() {
		return choosen_game;
	}

	public static int getStrategy_distribution(int index) {
		return strategy_distribution[index];
	}

	public static int getStrategy_exploration(int index) {
		return strategy_exploration[index];
	}

	public static int getCurrStep() {
		return current_step;
	}

	public static int getStepQuantity() {
		return step_quantity;
	}

	public static void increaseStep() {
		current_step++;
	}

	public static boolean isSimulationDone() {
		return current_step >= step_quantity;
	}

	public static int getCurrSimulation() {
		return curr_simulation;
	}

	public static int getSimulationQuantity() {
		return simulation_quantity;
	}

	public static void increaseSimulation() {
		curr_simulation++;
	}

	public static boolean areSimulationsDone() {
		boolean result = curr_simulation >= simulation_quantity;
		if (result) {
			curr_simulation = 0;
			overall_simulation_count++;
		}
		return result;
	}

	public static boolean areOverallSimulationsDone() {
		areSimulationsDone();
		return agent_quantity + (overall_simulation_count * agent_quantity_increase) > agent_quantity_limit;
	}

	public static void broadcastMessage(int[] agent_list, String performative, String content) {
		String str_agent_list = "";
		for (int i = 0; i < agent_list.length; i++) {
			str_agent_list = str_agent_list + agent_list[i];
			if (i < agent_list.length - 1) {
				str_agent_list = str_agent_list + CommonBeliefDB.SEPARATOR;
			}
		}

		String agent_class = "bin/agent/special/MercuryAgentBDI.class";

		Map<String, Object> agParam = new HashMap<String, Object>();
		agParam.put("agent_list", str_agent_list);
		agParam.put("performative", performative);
		agParam.put("content", content);

		Platform.getInstance().deployAgent(agent_class, agParam);
	}

}
