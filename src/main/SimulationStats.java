package main;

import java.io.*;

import main.loader.Loader;
import util.ImageUtil;

public class SimulationStats {
	private static SimulationStats instance;

	// common knowledge
	private int height;
	private int width;
	private int preservedValue;
	private int agent_quantity;
	private int step_quantity;

	// space
	private int[][] initialSpace;
	private int[][] finalSpace;
	private int[][] ignoredLayers;
	private String csv_sep = ";";

	// image generation
	private boolean generateIm;
	private int generationInterval;

	// stats calculation - fom
	private File fom_file;
	private BufferedWriter fom_bw;
	private boolean[] fom_calculated;
	private double min_fom = 101;
	private int step_min_fom = 0;
	private double max_fom = -1;
	private int step_max_fom = 0;

	double figureOfMerit = 0;
	double nullModel = 0;
	double simulatedNullModel = 0;
	int statePreserved = 0;
	int stateChanged = 1;

	long observedQtyCorrectlyPreserved = 0;
	long observedQtyCorrectlyConverted = 0;
	long observedQtyIncorrectlyPreserved = 0;
	long observedQtyIncorrectlyConverted = 0;

	long simulatedQtyCorrectlyPreserved = 0;
	long simulatedQtyCorrectlyConverted = 0;
	long simulatedQtyIncorrectlyPreserved = 0;
	long simulatedQtyIncorrectlyConverted = 0;

	double type1 = 0;// correctly converted
	double type3 = 0;// incorrectly converted
	double type4 = 0;// incorrectly preserved
	double valid = 0;// valid positions (anything but borders and layers)

	double fomMeasurements[]; // all the computed foms
	double nullModelMeasurements[]; // all the null models
	double simulatedNullModelMeasurements[]; // all the simulated null models

	// stats calculation - strategy change
	String strategyNames[];
	Integer strategyChangesMeasurements[][];

	// stats calculation - tongue model
	

	public static void initSimulationStats() {
		instance = new SimulationStats();
	}

	public static SimulationStats getInstance() {
		return instance;
	}

	public SimulationStats() {
		// initial and final spaces
		String model_path = Loader.getPropertyValue("model_path");
		String observed_space_sources[] = Loader.getPropertyValue("observed_space_sources").replace('{', ' ')
				.replace('}', ' ').split(",");
		String spatial_attributes_sources[] = Loader.getPropertyValue("spatial_attributes_sources").replace('{', ' ')
				.replace('}', ' ').split(",");

		String path_spatial_attributes_sources[] = new String[spatial_attributes_sources.length];
		for (int i = 0; i < spatial_attributes_sources.length; i++) {
			path_spatial_attributes_sources[i] = model_path + File.separator + spatial_attributes_sources[i].trim();
		}

		initialSpace = ImageUtil
				.filtragem_classes_iniciais(model_path + File.separator + observed_space_sources[0].trim());
		finalSpace = ImageUtil
				.filtragem_classes_iniciais(model_path + File.separator + observed_space_sources[1].trim());

		step_quantity = Integer.parseInt(Loader.getPropertyValue("step_quantity"));
		agent_quantity = Integer.parseInt(Loader.getPropertyValue("agent_quantity"));

		fom_calculated = new boolean[step_quantity];
		fomMeasurements = new double[step_quantity];
		nullModelMeasurements = new double[step_quantity];
		simulatedNullModelMeasurements = new double[step_quantity];
		for (int i = 0; i < step_quantity; i++) {
			fom_calculated[i] = false;
			fomMeasurements[i] = 0;
			nullModelMeasurements[i] = 0;
			simulatedNullModelMeasurements[i] = 0;
		}

		height = Integer.parseInt(Loader.getPropertyValue("height"));
		width = Integer.parseInt(Loader.getPropertyValue("width"));
		generateIm = Boolean.parseBoolean(Loader.getPropertyValue("generateImage"));
		generationInterval = Integer.parseInt(Loader.getPropertyValue("generateInterval"));
		ignoredLayers = ImageUtil.filtragem_layers_basica(model_path + File.separator, spatial_attributes_sources,
				width, height);
		preservedValue = Integer.parseInt(Loader.getPropertyValue("cell_state_natural"));

		// fom
		int count = 0;
		do {
			fom_file = new File("agent_quantity_" + Simulation.getInstance().agent_quantity + "_" + "simulation_"
					+ (count++) + "_" + Loader.getPropertyValue("figureOfMeritSaveFile"));
		} while (fom_file.exists());
	}

	public void refreshMeasurements() {
		int count = 0;
		do {
			fom_file = new File("agent_quantity_" + Simulation.getInstance().agent_quantity + "_" + "simulation_"
					+ (count++) + "_" + Loader.getPropertyValue("figureOfMeritSaveFile"));
		} while (fom_file.exists());
		for (int i = 0; i < step_quantity; i++) {
			fom_calculated[i] = false;
			fomMeasurements[i] = 0;
			nullModelMeasurements[i] = 0;
			simulatedNullModelMeasurements[i] = 0;
		}
		max_fom = -1;
		step_max_fom = 0;
		min_fom = 101;
		step_min_fom = 0;
		agent_quantity = Integer.parseInt(Loader.getPropertyValue("agent_quantity"));

	}

	private void initFomVars() {
		figureOfMerit = 0;
		nullModel = 0;
		simulatedNullModel = 0;
		// simulado
		statePreserved = 0;
		stateChanged = 1;

		observedQtyCorrectlyPreserved = 0;
		observedQtyCorrectlyConverted = 0;
		observedQtyIncorrectlyPreserved = 0;
		observedQtyIncorrectlyConverted = 0;

		simulatedQtyCorrectlyPreserved = 0;
		simulatedQtyCorrectlyConverted = 0;
		simulatedQtyIncorrectlyPreserved = 0;
		simulatedQtyIncorrectlyConverted = 0;

		type1 = 0;// correctly converted
		type3 = 0;// incorrectly converted
		type4 = 0;// incorrectly preserved
		valid = 0;// valid positions (anything but borders and layers)

	}

	public void calculate_curr_fom(int step, int simulatedFinalState[][], int owned_cells[][],
			boolean is_the_first_step, boolean is_the_last_step) {
		initFomVars();
		int[][] initialVersusFinalMap = new int[height][width];

		// primeiro vamos comparar 2002 com 2008
		for (int j = 0; j < width; j++) {
			for (int i = 0; i < height; i++) {
				if (ignoredLayers[i][j] == 1 || initialSpace[i][j] < 0) {
					initialVersusFinalMap[i][j] = -1;
				} else if (initialSpace[i][j] == preservedValue && finalSpace[i][j] == preservedValue) {

					initialVersusFinalMap[i][j] = statePreserved;
					observedQtyCorrectlyPreserved++;

				} else if (initialSpace[i][j] < preservedValue && finalSpace[i][j] < preservedValue) {

					initialVersusFinalMap[i][j] = statePreserved;
					observedQtyCorrectlyConverted++;

				} else if (initialSpace[i][j] == preservedValue && finalSpace[i][j] < preservedValue) {
					initialVersusFinalMap[i][j] = stateChanged;
					observedQtyIncorrectlyPreserved++;

				} else if (initialSpace[i][j] < preservedValue && finalSpace[i][j] == preservedValue) {

					initialVersusFinalMap[i][j] = stateChanged;
					observedQtyIncorrectlyConverted++;

				}
			}
		}

		for (int j = 0; j < width; j++) {
			for (int i = 0; i < height; i++) {
				if (ignoredLayers[i][j] == 1 || simulatedFinalState[i][j] < 0) {
					// ignorar
				} else if (simulatedFinalState[i][j] == preservedValue && finalSpace[i][j] == preservedValue) {
					simulatedQtyCorrectlyPreserved++;
					valid++;
				} else if (simulatedFinalState[i][j] < preservedValue && finalSpace[i][j] < preservedValue) {
					if (initialVersusFinalMap[i][j] == stateChanged) {
						type1++;
					}
					simulatedQtyCorrectlyConverted++;
					valid++;
				} else if (simulatedFinalState[i][j] == preservedValue && finalSpace[i][j] < preservedValue) {
					type3++;
					simulatedQtyIncorrectlyPreserved++;
					valid++;
				} else if (simulatedFinalState[i][j] < preservedValue && finalSpace[i][j] == preservedValue) {
					type4++;
					simulatedQtyIncorrectlyConverted++;
					valid++;
				}
			}
		}

		// agora vamos calcular o nullModel
		long totalObserved = observedQtyCorrectlyPreserved + observedQtyCorrectlyConverted
				+ observedQtyIncorrectlyConverted + observedQtyIncorrectlyPreserved;
		long errorsObserved = observedQtyIncorrectlyConverted + observedQtyIncorrectlyPreserved;
		nullModel = (double) ((double) errorsObserved / (double) totalObserved);
		// calculando o simulatedNullModel
		long totalSimulated = simulatedQtyCorrectlyPreserved + simulatedQtyCorrectlyConverted
				+ simulatedQtyIncorrectlyConverted + simulatedQtyIncorrectlyPreserved;
		long errorsSimulated = simulatedQtyIncorrectlyConverted + simulatedQtyIncorrectlyPreserved;
		simulatedNullModel = (double) ((double) errorsSimulated / (double) totalSimulated);

		// calculando figura de m�rito
		double wrongchange = 100 * (type4 / valid);
		double rightchange = 100 * (type1 / valid);
		double wrongpersistance = 100 * (type3 / valid);
		figureOfMerit = 100 * (type1 / (type1 + type3 + type4));
		double producersaccuracy = 100 * (type1 / (type1 + type3));
		double usersaccuracy = 100 * (type1 / (type1 + type4));

		write_fom(String.format("%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s\n", step, csv_sep, figureOfMerit, csv_sep,
				producersaccuracy, csv_sep, usersaccuracy, csv_sep, wrongchange, csv_sep, rightchange, csv_sep,
				wrongpersistance, csv_sep, nullModel, csv_sep, simulatedNullModel, csv_sep));
		if (generateIm && (step % generationInterval == 0 || is_the_first_step || is_the_last_step)) {
			ImageUtil.generateIm(width, height, simulatedFinalState, owned_cells,
					"agent_quantity_" + Simulation.getInstance().agent_quantity + "_" + "simulation_" + step + ".bmp");
		}

		fomMeasurements[step] = figureOfMerit;
		nullModelMeasurements[step] = nullModel;
		simulatedNullModelMeasurements[step] = simulatedNullModel;

		if (min_fom > figureOfMerit) {
			min_fom = figureOfMerit;
			step_min_fom = step;
		}
		if (max_fom < figureOfMerit) {
			max_fom = figureOfMerit;
			step_max_fom = step;
		}
		Main.getInstance().updateFOMGraph(step, figureOfMerit, nullModel, simulatedNullModel);

	}

	public int getStep_min_fom() {
		return step_min_fom;
	}

	public int getStep_max_fom() {
		return step_max_fom;
	}

	public double[] getFomMeasurements() {
		return fomMeasurements;
	}

	public double[] getNullModelMeasurements() {
		return nullModelMeasurements;
	}

	public double[] getSimulatedNullModelMeasurements() {
		return simulatedNullModelMeasurements;
	}

	public void setStrategyNames(String strategyNames[]) {
		this.strategyNames = strategyNames;
	}

	public void setStrategyChangesMeasurements(int step, Integer strategyChangesMeasurements[][]) {
		this.strategyChangesMeasurements = strategyChangesMeasurements;
		Main.getInstance().updateStrGraph(agent_quantity, step, strategyNames, strategyChangesMeasurements);
	}

	public void calculateTongueModel(int step, int cells[][]) {
		
	}
	
	public void write_fom(String str) {
		try {
			fom_bw = new BufferedWriter(new FileWriter(fom_file, true));
			fom_bw.write(str);
			fom_bw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
