package main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

import main.loader.Loader;
import main.view.*;

public class Main implements MouseListener {
	private static Main instance;
	private static final String NOGUI = "NOGUI";

	private boolean gui = true;

	private JFrame jframe;
	private JMenuBar menubar;
	private JMenu menu;
	private JMenuItem menu_new_model;
	private JMenuItem menu_load_model;
	private JMenuItem menu_config_model;
	private JMenuItem menu_about;
	private JMenuItem menu_quit;
	private JPanel subContent;
	private SimulationScreen simulation;
	private InformationScreen information;
	private FOMDiagram graph0;
	private StrategyDiagram graph1;
	private TongueDiagram graph2;
	private ConfigurationScreen configScreen;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		final StringBuffer sb = new StringBuffer();
		sb.append(true);
		if (args != null && args.length >= 2 && args[0].contentEquals("-mode")) {
			if (args[1].equalsIgnoreCase(NOGUI)) {
				sb.delete(0, sb.length() - 1);
				sb.append(false);
			}
		}
		initialize(Boolean.parseBoolean(sb.toString()));
	}

	/**
	 *
	 * Create the instance
	 */

	private static void initialize(boolean gui) {
		instance = new Main(gui);
	}

	/**
	 * get the instance
	 */
	public static Main getInstance() {
		return instance;
	}

	/**
	 * Create the application.
	 */
	private Main(boolean gui) {
		this.gui = gui;
		if (gui) {
			initializeGUI();
		} else {
			// entering command line mode
		}
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initializeGUI() {
		jframe = new JFrame();
		jframe.setTitle("MASE-EGTI");
		jframe.setExtendedState(JFrame.MAXIMIZED_BOTH);
		jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jframe.setLayout(new BorderLayout());

		initializeConfigurationScreen();
		initializeMenu();
		initializeSubContent();
		jframe.setVisible(true);
	}

	private void initializeMenu() {
		menubar = new JMenuBar();

		menu = new JMenu("Menu");

		menu_new_model = new JMenuItem("New Model");
		menu_new_model.addMouseListener(this);
		menu_load_model = new JMenuItem("Load Model");
		menu_load_model.addMouseListener(this);
		menu_config_model = new JMenuItem("Config Model");
		menu_config_model.addMouseListener(this);
		menu_about = new JMenuItem("About");
		menu_about.addMouseListener(this);
		menu_quit = new JMenuItem("Quit");
		menu_quit.addMouseListener(this);
		menubar.add(menu);

		menu.add(menu_new_model);
		menu.add(menu_load_model);
		menu.add(menu_config_model);
		menu.add(menu_about);
		menu.add(menu_quit);
		jframe.getContentPane().add(menubar, BorderLayout.NORTH);
	}

	public void initializeSubContent() {
		subContent = new JPanel();
		subContent.setForeground(Color.BLACK);
		jframe.getContentPane().add(subContent, BorderLayout.CENTER);

		GridBagLayout grid = new GridBagLayout();
		GridBagConstraints constraints = new GridBagConstraints();
		subContent.setLayout(grid);

		grid.columnWeights = new double[] { 3, 1 };
		grid.rowWeights = new double[] { 0.7, 1, 1, 1 };

		simulation = new SimulationScreen();
		constraints.fill = GridBagConstraints.BOTH;
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.gridheight = 4;
		subContent.add(simulation, constraints);

		information = new InformationScreen();
		constraints.fill = GridBagConstraints.BOTH;
		constraints.gridwidth = 1;
		constraints.gridx = 1;
		constraints.gridy = 0;
		constraints.gridheight = 1;
		subContent.add(information, constraints);

		graph0 = new FOMDiagram();
		constraints.fill = GridBagConstraints.BOTH;
		constraints.gridx = 1;
		constraints.gridy = 1;
		constraints.gridheight = 1;
		subContent.add(graph0, constraints);

		graph1 = new StrategyDiagram();
		constraints.fill = GridBagConstraints.BOTH;
		constraints.gridx = 1;
		constraints.gridy = 2;
		constraints.gridheight = 1;
		subContent.add(graph1, constraints);

		graph2 = new TongueDiagram();
		constraints.fill = GridBagConstraints.BOTH;
		constraints.gridx = 1;
		constraints.gridy = 3;
		constraints.gridheight = 1;
		subContent.add(graph2, constraints);
	}

	public void initializeConfigurationScreen() {
		configScreen = new ConfigurationScreen();
	}

	public boolean is_gui() {
		return gui;
	}

	public void initializeSimulation() {
		// TODO adapt this code to reflect the choosen model
		Loader.initConfiguration(null);
		Platform.initPlatform();
		Simulation.initSimulation();
		SimulationStats.initSimulationStats();
		Simulation.getInstance().startSimulation();
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
		Object clickedObj = e.getSource();
		if (clickedObj.equals(menu_new_model)) {
			configScreen.makeVisible();
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {

	}

	@Override
	public void mouseEntered(MouseEvent e) {

	}

	@Override
	public void mouseExited(MouseEvent e) {

	}

	public InformationScreen getInformationScreen() {return information;}
	
	public void updateSimulationScreen(int agent_quantity, int step, int step_quantity, int[][] cells,
			int owned[][]) {
		simulation.setCells(agent_quantity, step, step_quantity, cells, owned);
	}

	public void updateSimulationState(String state) {
		simulation.updateState(state);
	}

	public void updateFOMGraph(int step, double fom, double nullModel, double simulatedNullmodel) {
		((FOMDiagram) graph0).updateGraph(step, fom, nullModel, simulatedNullmodel);
	}

	public void updateStrGraph(int agent_quantity, int step, String strategy_names[], Integer strategy_count[][]) {
		((StrategyDiagram) graph1).updateGraph(agent_quantity, step, strategy_names, strategy_count);
	}

	public void updateTonGraph(int step, String strategy_names[], int[][] cells, int owner_type[][]) {
		((TongueDiagram) graph2).updateGraph(step, strategy_names, cells, owner_type);
	}
	
	public void initTonGraph() {
		((TongueDiagram) graph2).initTongueModel();
	}
}
