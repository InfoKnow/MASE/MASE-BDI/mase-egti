package util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;

import spatial.Cell;

public class MatrixWriter {

	public static void write(String filename, double[][] matrix) {
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(new File(filename)));
			for (double[] submatrix : matrix) {
				String line = "";
				for (double el : submatrix) {
					line = line + el + "\t";
				}
				bw.write(line + "\n");
			}
			bw.close();
		} catch (Exception e) {
		}
	}
	
	public static void write(String filename, String[] positions) {
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(new File(filename)));
			for (String p : positions) {
				bw.write(p + "\n");
			}
			bw.close();
		} catch (Exception e) {
		}
	}

	public static void write(String filename, ArrayList<Cell> best_positions, int minimum_cell_group_width,
			int minimum_cell_group_height, int width, int height) {
		try {
			double[][] result = new double[height][width];
			String[] positions = new String[height*width];
			int counter = 0;
			for (Cell c : best_positions) {
				int[] index = c.getIndex();
				int x = index[0] * minimum_cell_group_height;
				int y = index[1] * minimum_cell_group_width;
				for (int i = 0; i < minimum_cell_group_height; i++) {
					for (int j = 0; j < minimum_cell_group_width; j++) {
						int pivot_x = x + i;
						int pivot_y = y + j;
						result[pivot_x][pivot_y] = counter;
						positions[counter] = String.format("%d, %d", pivot_x, pivot_y);
						counter++;
						
					}
				}
			}
			write(filename, result);
			write("positions_"+filename, positions);
		} catch (Exception e) {
		}
	}
}
