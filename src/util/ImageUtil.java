package util;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import main.loader.JSONLoader;

public abstract class ImageUtil {

	private static int height = -1;
	private static int width = -1;

	protected static void setHeightAndWidth(int height, int width) {
		ImageUtil.height = height;
		ImageUtil.width = width;
	}

	private static void loadHeightAndWidth() {

		try {
			height = Integer.parseInt(JSONLoader.getInstance().getPropertyValue("space.height"));
			width = Integer.parseInt(JSONLoader.getInstance().getPropertyValue("space.width"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// TODO melhorar esse metodo
	public static int[][] filtragem_classes_iniciais(String nomeArquivo) {
		if (height == -1 || width == -1) {
			loadHeightAndWidth();
		}
		try {
			BufferedImage buffer = ImageIO.read(new File(nomeArquivo));
			int[][] result = new int[height][width];
			for (int j = 0; j < height; j++) {
				for (int i = 0; i < width; i++) {
					Color c = new Color(buffer.getRGB(i, j));
					int red = c.getRed();
					int green = c.getGreen();
					int blue = c.getBlue();
					if ((red == 0 && green == 100 && blue == 0) || (red == 0 && green == 255 && blue == 0)
							|| (red == 76 && green == 230 && blue == 0)) {
						result[j][i] = 1500;
					} else if ((red == 255 && green == 255 && blue == 100) || (red == 255 && green == 255 && blue == 0)
							|| (red == 255 && green == 255 && blue == 50)) {
						result[j][i] = 500;
					} else {
						result[j][i] = -1;
					}
				}
			}
			return result;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static int[][] filtragem_adicional_layers(String[] nomesArquivos) {
		if (height == -1 || width == -1) {
			loadHeightAndWidth();
		}
		int[][] result = new int[height][width];

		for (int j = 0; j < height; j++) {
			for (int i = 0; i < width; i++) {
				result[j][i] = 0;
			}
		}
		try {
			for (String nomeArquivo : nomesArquivos) {
				BufferedImage image = ImageIO.read(new File(nomeArquivo));
				for (int j = 0; j < height; j++) {
					for (int i = 0; i < width; i++) {
						Color c = new Color(image.getRGB(i, j));
						int pixel[] = new int[] { c.getRed(), c.getBlue(), c.getGreen() };

						if (pixel[0] > 10 && pixel[1] > 10 && pixel[2] > 10) {
							result[j][i] += 0;
						} else {
							result[j][i] += 1;
						}
					}

				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return result;
	}

	public static int[][] filtragem_relevo(String nomeArquivo) {
		if (height == -1 || width == -1) {
			loadHeightAndWidth();
		}
		// ImagePlus image = IJ.openImage(nomeArquivo);
		// ImageProcessor improc = image.getProcessor();
		try {
			BufferedImage improc = ImageIO.read(new File(nomeArquivo));
			int width = improc.getWidth();
			int height = improc.getHeight();
			int[][] result = new int[height][width];

			for (int j = 0; j < height; j++) {
				for (int i = 0; i < width; i++) {
					Color c = new Color(improc.getRGB(i, j));
					int pixel[] = new int[] { c.getRed(), c.getBlue(), c.getGreen() };

					if (pixel[0] == 0) {
						result[j][i] = 0;
					} else if (pixel[0] == 52) {
						result[j][i] = -1;
					} else if (pixel[0] == 78) {
						result[j][i] = 0;
					} else if (pixel[0] == 104) {
						result[j][i] = +1;
					} else if (pixel[0] == 130) {
						result[j][i] = 0;
					} else {
						result[j][i] = 0;
					}

				}

			}
			return result;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static int[][] filtragem_solo(String nomeArquivo) {
		if (height == -1 || width == -1) {
			loadHeightAndWidth();
		}
		// ImagePlus image = IJ.openImage(nomeArquivo);
		// ImageProcessor improc = image.getProcessor();
		try {
			BufferedImage improc = ImageIO.read(new File(nomeArquivo));

			int width = improc.getWidth();
			int height = improc.getHeight();
			int[][] result = new int[height][width];
			for (int j = 0; j < height; j++) {
				for (int i = 0; i < width; i++) {
					Color c = new Color(improc.getRGB(i, j));
					int pixel[] = new int[] { c.getRed(), c.getBlue(), c.getGreen() };

					if (pixel[0] == 210 || pixel[0] == 38) {
						result[j][i] = -1;
					} else if (pixel[0] == 112) {
						result[j][i] = 0;
					} else if (pixel[0] == 74 || pixel[0] == 53 || pixel[0] == 250 || pixel[0] == 154) {
						result[j][i] = 1;
					} else if (pixel[0] == 176 || pixel[0] == 133 || pixel[0] == 91 || pixel[0] == 232) {
						result[j][i] = 1;
					} else if (pixel[0] == 189) {
						result[j][i] = 1;
					} else if (pixel[0] == 45) {
						result[j][i] = 0;
					} else if (pixel[0] == 104 || pixel[0] == 84 || pixel[0] == 141) {
						result[j][i] = 0;
					} else if (pixel[0] == 201 || pixel[0] == 98) {
						result[j][i] = 0;
					} else if (pixel[0] == 79) {
						result[j][i] = 0;
					} else if (pixel[0] == 216) {
						result[j][i] = 1;
					} else {
						result[j][i] = 0;
					}

				}

			}
			return result;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static int[][] marcacao_layers(String[] nomesArquivos) {
		if (height == -1 || width == -1) {
			loadHeightAndWidth();
		}
		int[][] result = new int[height][width];
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				result[i][j] = 0;
			}
		}

		try {
			for (int i = 0; i < nomesArquivos.length; i++) {
				String nomeArquivo = nomesArquivos[i];
				BufferedImage a = ImageIO.read(new File(nomeArquivo));
				for (int x = 0; x < height; x++) {
					for (int y = 0; y < width; y++) {
						Color c = new Color(a.getRGB(y, x));
						if (!(c.getRed() == 255 && c.getGreen() == 255 && c.getBlue() == 255)) {
							result[x][y] = 1;
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public static int[][] filtragem_gaussiana(String[] nomesArquivos, String[] pesos) {
		if (height == -1 || width == -1) {
			loadHeightAndWidth();
		}
		int[][] result = new int[height][width];
		int[][] result_aux = new int[height][width];
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				result[i][j] = 0;
				result_aux[i][j] = 0;
			}
		}
		try {

			int min = Integer.MAX_VALUE;
			int max = Integer.MIN_VALUE;

			for (int i = 0; i < nomesArquivos.length; i++) {
				String nomeArquivo = nomesArquivos[i];
				double peso = Double.parseDouble(pesos[i].trim());
				BufferedImage a = ImageIO.read(new File(nomeArquivo));
				int[] filter = { 16, 8, 16, 8, 16, 8, 16, 32, 16, 8, 16, 8, 16, 8, 16 };
				int filterWidth = 5;

				BufferedImage blurred = blur(a, filter, filterWidth);

				for (int y = 0; y < height; y++) {
					for (int x = 0; x < width; x++) {
						int rgb = blurred.getRGB(x, y);
						int r = rgb >>> 16 & 0xFF;
						int g = rgb >>> 8 & 0xFF;
						int b = rgb & 0xFF;
						result[y][x] = result[y][x] + (int) Math.round(peso * ((r + g + b) / 3));

						rgb = a.getRGB(x, y);
						r = rgb >>> 16 & 0xFF;
						g = rgb >>> 8 & 0xFF;
						b = rgb & 0xFF;
						int sum = (result_aux[y][x] + (((r + g + b) / 3)));
						if (sum > 255) {
							result_aux[y][x] = 255;
						} else {
							result_aux[y][x] = sum;
						}
						if (i == nomesArquivos.length - 1) {
							if (min > result[y][x]) {
								min = result[y][x];
							}

							if (max < result[y][x]) {
								max = result[y][x];
							}
						}
					}
				}
			}
			int delta = max - min;
			double transfactor = 255d / (max + delta);
			for (int y = 0; y < height; y++) {
				for (int x = 0; x < width; x++) {
					// realizando normalização
					result[y][x] = (int) Math.round((result[y][x] + delta) * transfactor);
					// invertendo o valor
					result[y][x] = 255 - result[y][x];
					// removendo a camada original
					if (result_aux[y][x] != 255) {
						result[y][x] = 0;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	// https://stackoverflow.com/questions/39684820/java-implementation-of-gaussian-blur
	public static BufferedImage blur(BufferedImage image, int[] filter, int filterWidth) {
		if (filter.length % filterWidth != 0) {
			throw new IllegalArgumentException("filter contains a incomplete row");
		}

		final int width = image.getWidth();
		final int height = image.getHeight();
		int sum = 0;
		for (int x : filter) {
			sum += x;
		}

		int[] input = image.getRGB(0, 0, width, height, null, 0, width);

		int[] output = new int[input.length];

		final int pixelIndexOffset = width - filterWidth;
		final int centerOffsetX = filterWidth / 2;
		final int centerOffsetY = filter.length / filterWidth / 2;

		// apply filter
		for (int h = height - filter.length / filterWidth + 1, w = width - filterWidth + 1, y = 0; y < h; y++) {
			for (int x = 0; x < w; x++) {
				int r = 0;
				int g = 0;
				int b = 0;
				for (int filterIndex = 0, pixelIndex = y * width
						+ x; filterIndex < filter.length; pixelIndex += pixelIndexOffset) {
					for (int fx = 0; fx < filterWidth; fx++, pixelIndex++, filterIndex++) {
						int col = input[pixelIndex];
						int factor = filter[filterIndex];

						// sum up color channels seperately
						r += ((col >>> 16) & 0xFF) * factor;
						g += ((col >>> 8) & 0xFF) * factor;
						b += (col & 0xFF) * factor;
					}
				}
				r /= sum;
				g /= sum;
				b /= sum;
				// combine channels with full opacity
				output[x + centerOffsetX + (y + centerOffsetY) * width] = (r << 16) | (g << 8) | b | 0xFF000000;
			}
		}

		BufferedImage result = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		result.setRGB(0, 0, width, height, output, 0, width);

		return result;
	}

	public static int[][] filtragem_layers_basica(String[] nomesArquivos, int width, int height) {
		if (height == -1 || width == -1) {
			loadHeightAndWidth();
		}
		int[][] result = new int[height][width];
		for (int j = 0; j < width; j++) {
			for (int i = 0; i < height; i++) {
				result[i][j] = 0;
			}
		}
		try {
			for (String nomeArquivo : nomesArquivos) {
				BufferedImage buffer = ImageIO.read(new File((nomeArquivo.trim())));

				for (int j = 0; j < width; j++) {
					for (int i = 0; i < height; i++) {
						Color c = new Color(buffer.getRGB(j, i));
						int red = c.getRed();
						int valorFinal = red;

						if (red < 240) {
							valorFinal = 1;
						}

						result[i][j] = (short) valorFinal;
					}
				}

			}
			return result;
		} catch (IOException i) {
			i.printStackTrace();
		}
		return null;
	}

	public static int[][] filtragem_pdot(String nomeArquivo) {
		if (height == -1 || width == -1) {
			loadHeightAndWidth();
		}
		try {
			BufferedImage improc = ImageIO.read(new File(nomeArquivo));
			int[][] result = new int[height][width];
			for (int i = 0; i < height; i++) {
				for (int j = 0; j < width; j++) {
					Color c = new Color(improc.getRGB(j, i));
					int rgb[] = new int[] { c.getRed(), c.getBlue(), c.getGreen() };
					int max = max(rgb);
					short valor = 0;
					if (rgb[0] == max) {
						valor = 1; // proibido
					} else if (rgb[2] == max) {
						valor = 2; // incentivado
					} else if (rgb[1] == max) {
						valor = 3; // normal
					}
					result[i][j] = valor;

				}
			}
			return result;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static int max(int[] values) {
		int max = Integer.MIN_VALUE;
		for (int v : values) {
			if (max < v) {
				max = v;
			}
		}
		return max;
	}

	public static int[][] filtragem_spatial_attributes_debug() {
		if (height == -1 || width == -1) {
			loadHeightAndWidth();
		}
		try {
			BufferedImage improc = ImageIO.read(new File("model/cerradomapbiomas/spaces/spaceComparison20002018.bmp"));
			int[][] result = new int[height][width];
			for (int i = 0; i < height; i++) {
				for (int j = 0; j < width; j++) {
					Color c = new Color(improc.getRGB(j, i));
					int rgb[] = new int[] { c.getRed(), c.getBlue(), c.getGreen() };
					short valor = 0;
					if (rgb[0] == 255) {
						valor = 1; // proibido
					} else if (rgb[2] == 255) {
						valor = 2; // incentivado
					} else if (rgb[1] == 255) {
						valor = 3; // normal
					}
					result[i][j] = valor;

				}
			}
			return result;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void debug_image(BufferedImage img) {
		if (height == -1 || width == -1) {
			loadHeightAndWidth();
		}
		JFrame frame = new JFrame();
		frame.getContentPane().setLayout(new FlowLayout());
		frame.getContentPane().add(new JLabel(new ImageIcon(img)));
		frame.setPreferredSize(new Dimension(width, height));
		frame.pack();
		frame.setVisible(true);
	}

	public static String generateIm(int width, int height, int[][] map, int[][] occupied_cells, String imname) {
		try {
			BufferedImage improc = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
			for (int j = 0; j < height; j++) {
				for (int i = 0; i < width; i++) {
					if (occupied_cells[j][i] != -1) {
						// TODO get strategy colors
						improc.setRGB(i, j, 0xff00ff);
					} else if (map[j][i] == 1500) {
						improc.setRGB(i, j, 0x006400);
					} else if (map[j][i] < 1500 && map[j][i] >= 0) {
						improc.setRGB(i, j, 0xffff32);
					} else {
						improc.setRGB(i, j, 0xffffff);
					}

				}
			}

			String path = "";
			RenderedImage a = improc;
			ImageIO.write(a, "bmp", new File(imname));
			return path;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

}
