package util;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;

import javax.imageio.ImageIO;

public class ImageAnalyst {

	private static BufferedImage initialIm;
	private static BufferedImage finalIm;

	private static String initialFilePath = "model\\cerrado\\spaces\\2002.bmp";
	private static String finalFilePath = "model\\cerrado\\spaces\\2008.bmp";
	private static String resultFilePath = "model\\cerrado\\spaces\\spaceComparison.bmp";

	private static String[] layers = { "model\\cerrado\\layers\\corposdagua.bmp",
			"model\\cerrado\\layers\\cursosdagua.bmp", "model\\cerrado\\layers\\edificacoes.bmp",
			"model\\cerrado\\layers\\ferrovias.bmp", "model\\cerrado\\layers\\rodovias.bmp",
			"model\\cerrado\\layers\\ruas.bmp", "model\\cerrado\\layers\\ucs.bmp" };
	private static String resultProximalMatrixPath = "model\\cerrado\\spaces\\proximalMatrix.bmp";
	private static String goodDirectionsPath = "model\\cerrado\\spaces\\goodDirections.bmp";

	private static String[] weights = { "1", "1", "-1", "1", "1", "-1", "-1" };

	private static String pdot = "model\\cerrado\\pdot\\pdot.bmp";
	/*
	private static String initialFilePath = "C:\\Users\\RNP\\eclipse-workspace\\MASE-Doctorate\\model\\cerradomapbiomas\\spaces\\df2000_cropped.bmp";
	private static String finalFilePath = "C:\\Users\\RNP\\eclipse-workspace\\MASE-Doctorate\\model\\cerradomapbiomas\\spaces\\df2018_cropped.bmp";
	private static String resultFilePath = "C:\\Users\\RNP\\eclipse-workspace\\MASE-Doctorate\\model\\cerradomapbiomas\\spaces\\spaceComparison20002018.bmp";
	private static String[] layers = { "model\\cerradomapbiomas\\layers\\layer4_cropped.bmp",
			"model\\cerradomapbiomas\\layers\\cursosdagua_cropped.bmp", "model\\cerradomapbiomas\\layers\\layer3_cropped.bmp",
			"model\\cerradomapbiomas\\layers\\ferrovias_cropped.bmp", "model\\cerradomapbiomas\\layers\\rodovias_cropped.bmp",
			"model\\cerradomapbiomas\\layers\\ruas_cropped.bmp", "model\\cerradomapbiomas\\layers\\ucs_cropped.bmp" };
	private static String resultProximalMatrixPath = "model\\cerradomapbiomas\\spaces\\proximalMatrix.bmp";
	private static String goodDirectionsPath = "model\\cerradomapbiomas\\spaces\\goodDirections.bmp";

	private static String[] weights = { "1", "1", "-1", "1", "1", "-1", "-1" };

	private static String pdot = "model\\cerradomapbiomas\\pdot\\pdot_cropped.bmp";
	*/
	private static int classAnthropic = 0;
	private static int classNaturalLandscape = 1;
	private static int classIgnored = -1;

	private static double totalPlaces = 0;
	private static double badPlaces = 0;
	private static double neutralPlaces = 0;
	private static double goodPlaces = 0;

	public static void main(String[] args) {
		try {
			initialIm = ImageIO.read(new File(initialFilePath));
			finalIm = ImageIO.read(new File(finalFilePath));
			BufferedImage result = identifyCorrectChangePlaces(initialIm, finalIm);
			RenderedImage a = result;
			ImageIO.write(a, "bmp", new File(resultFilePath));

			BufferedImage result2 = recreateProximalMatrix(initialIm);
			RenderedImage b = result2;
			ImageIO.write(b, "bmp", new File(resultProximalMatrixPath));

			BufferedImage result3 = recreateGoodDirections(result, result2);
			RenderedImage c = result3;
			ImageIO.write(c, "bmp", new File(goodDirectionsPath));
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static BufferedImage identifyCorrectChangePlaces(BufferedImage initialIm, BufferedImage finalIm) {
		int width = initialIm.getWidth();
		int height = initialIm.getHeight();
		BufferedImage result = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				int classA = getPixelClass(new Color(initialIm.getRGB(i, j)));
				int classB = getPixelClass(new Color(finalIm.getRGB(i, j)));
				Color c = Color.WHITE;
				if (classA == classNaturalLandscape && classB == classAnthropic) {
					goodPlaces += 1;
					totalPlaces += 1;
					c = Color.GREEN;
				} else if (classA == classNaturalLandscape && classB == classNaturalLandscape) {
					badPlaces += 1;
					totalPlaces += 1;
					c = Color.RED;
				} else if (classA == classAnthropic && classB == classAnthropic) {
					neutralPlaces += 1;
					totalPlaces += 1;
					c = Color.BLUE;
				}
				result.setRGB(i, j, c.getRGB());
			}
		}

		System.out.println("Bad places: " + (100 * (badPlaces / totalPlaces)));
		System.out.println("Neutral places: " + (100 * (neutralPlaces / totalPlaces)));
		System.out.println("Good places: " + (100 * (goodPlaces / totalPlaces)));

		return result;

	}

	public static int getPixelClass(Color c) {
		int red = c.getRed();
		int green = c.getGreen();
		int blue = c.getBlue();
		if ((red == 0 && green == 100 && blue == 0) || (red == 0 && green == 255 && blue == 0)
				|| (red == 76 && green == 230 && blue == 0)) {
			return classNaturalLandscape;
		} else if ((red == 255 && green == 255 && blue == 100) || (red == 255 && green == 255 && blue == 0)
				|| (red == 255 && green == 255 && blue == 50)) {
			return classAnthropic;
		} else {
			return classIgnored;
		}
	}

	public static BufferedImage recreateProximalMatrix(BufferedImage initialIm) {
		int width = initialIm.getWidth();
		int height = initialIm.getHeight();
		ImageUtil.setHeightAndWidth(height, width);

		int[][] proximalMatrix = ImageUtil.filtragem_gaussiana(layers, weights);
		int[][] pdotMatrix = ImageUtil.filtragem_pdot(pdot);

		long max = Long.MIN_VALUE;
		BufferedImage result = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

		for (int i = 0; i < proximalMatrix.length; i++) {
			for (int j = 0; j < proximalMatrix[i].length; j++) {
				if(initialIm.getRGB(j, i) == (Color.WHITE.getRGB())) {
					proximalMatrix[i][j] = 0;
				}else if (pdotMatrix[i][j] == 1) {
					proximalMatrix[i][j] = -1;
				} else if (pdotMatrix[i][j] == 2) {
					proximalMatrix[i][j] *= 2;
				}

				if (max < proximalMatrix[i][j]) {
					max = proximalMatrix[i][j];
				}

			}
		}
		for (int i = 0; i < proximalMatrix.length; i++) {
			for (int j = 0; j < proximalMatrix[i].length; j++) {
				int r = 0;
				int g = 0;
				int b = 0;
				if (proximalMatrix[i][j] < 0) {
					r = 255;
				} else if (proximalMatrix[i][j] > 0) {
					g = (int) (255 * ((double) proximalMatrix[i][j] / (double) max));
				}

				result.setRGB(j, i, new Color(r, g, b).getRGB());
			}
		}
		return result;
	}

	public static BufferedImage recreateGoodDirections(BufferedImage correctChangePlaces,
			BufferedImage proximalMatrix) {
		int width = correctChangePlaces.getWidth();
		int height = correctChangePlaces.getHeight();

		BufferedImage result = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				int r = 0;
				int b = 0;
				int g = 0;
				if(new Color(correctChangePlaces.getRGB(i, j)).equals(Color.RED)) {
					r = new Color(proximalMatrix.getRGB(i, j)).getGreen();
				}else if(new Color(correctChangePlaces.getRGB(i, j)).equals(Color.BLUE)) {
					r = Color.GRAY.getRed();
					b = Color.GRAY.getBlue();
					g = Color.GRAY.getGreen();
				}else{
					g = new Color(proximalMatrix.getRGB(i, j)).getGreen();
				}
				
				result.setRGB(i, j, new Color(r, g, b).getRGB());
			}
		}

		return result;
	}

}
