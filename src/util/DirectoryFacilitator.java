package util;

import java.util.HashMap;
import java.util.Set;

import jadex.bridge.IComponentIdentifier;
import main.Main;

public class DirectoryFacilitator {
	private HashMap<String, IComponentIdentifier> yellow_pages;
	private static DirectoryFacilitator this_instance;
	private static boolean debug = false;

	static {
		try {
			//TODO load debug in preprocessing time
			//debug = Boolean.parseBoolean(Configuration.getPropertyValue("debug"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private DirectoryFacilitator() {
		yellow_pages = new HashMap<String, IComponentIdentifier>();
	}

	public static DirectoryFacilitator getInstance() {
		if (this_instance == null) {
			this_instance = new DirectoryFacilitator();
		}
		return this_instance;
	}

	public synchronized void registerAgent(String agentName, IComponentIdentifier AID) {
		yellow_pages.put(agentName, AID);
		if (debug) {
			Main.getInstance().getInformationScreen().appendToInfoArea(agentName + " was registered! Now the yellow pages have "
					+ yellow_pages.size() + " entries.");
		}
	}

	public IComponentIdentifier getAgentAID(String agentName) {
		return this_instance.yellow_pages.get(agentName);
	}

	public String getAgentName(IComponentIdentifier AID) {
		Set<String> keys = this_instance.yellow_pages.keySet();
		for (String key : keys) {
			if (this_instance.yellow_pages.get(key).equals(AID)) {
				return key;
			}
		}
		return null;
	}

	public String createName(String classname, int index) {
		return classname + "_" + index;
	}
}
