package spatial;

import java.util.ArrayList;

import agent.feature.AgentSignature;
import main.loader.SpatialLoader;

public class Cell implements Comparable<Cell> {
	/* defini��es est�ticas */
	public static final int INVALID = -1;
	public static final int NATURAL = 1;
	public static final int ANTHROPIZED = 2;

	/* vari�veis de posi��o */
	private int[] index;

	/* vari�veis de potencial de uso da terra */
	private int state;
	private int minimum_local_cell_value;
	private int maximum_local_cell_value;
	private int[][] local_cell_values;

	/* vari�veis de matriz proximal */
	private double[][] local_proximal_values;
	private double general_proximal_value;

	/* vari�veis de registro de agentes */
	private AgentSignature owner = null;
	private ArrayList<AgentSignature> occupied_by = null;
	private ArrayList<AgentSignature> wanted_by = null;
	private ArrayList<Double> wanted_by_bet = null;
	private int owner_type = -1;

	public Cell(int[] index, int[][] local_cell_values, double[][] local_proximal_values) {
		this.index = index;
		this.local_cell_values = local_cell_values;
		this.local_proximal_values = local_proximal_values;
		minimum_local_cell_value = SpatialLoader.getInstance().getMinCellValue();
		maximum_local_cell_value = SpatialLoader.getInstance().getMaxCellValue();

		owner = null;
		occupied_by = new ArrayList<>();
		wanted_by = new ArrayList<>();
		wanted_by_bet = new ArrayList<>();
		setState();
		setGeneralProximalValue();
	}

	private void setState() {
		state = NATURAL;
		for (int i = 0; i < local_cell_values.length; i++) {
			for (int j = 0; j < local_cell_values[i].length; j++) {
				if (local_cell_values[i][j] < minimum_local_cell_value) {
					state = INVALID;
					break;
				} else if (local_cell_values[i][j] < maximum_local_cell_value) {
					state = ANTHROPIZED;
					break;
				}
			}
		}
	}

	public int getState() {
		return state;
	}

	private void setGeneralProximalValue() {
		if (state == INVALID) {
			general_proximal_value = -1;
		} else {
			general_proximal_value = -1;
			for (int i = 0; i < local_proximal_values.length; i++) {
				for (int j = 0; j < local_proximal_values[i].length; j++) {
					general_proximal_value = Math.max(general_proximal_value, local_proximal_values[i][j]);
				}
			}
		}
	}

	public int getMaxLocalValue() {
		int max = Integer.MIN_VALUE;
		for (int i = 0; i < local_cell_values.length; i++) {
			for (int j = 0; j < local_cell_values[i].length; j++) {
				if (max < local_cell_values[i][j]) {
					max = local_cell_values[i][j];
				}
			}
		}
		return max;
	}

	public Double getGeneralProximalValue() {
		return general_proximal_value;
	}

	public void setOwner(AgentSignature owner) {
		this.owner = owner;
		owner_type = owner.getAgent_type();
	}

	public AgentSignature getOwner() {
		return owner;
	}
	
	public int getOnwerType() {
		return owner_type;
	}
	

	public synchronized void occupy_cell(AgentSignature signature) {
		occupied_by.add(signature);
	}

	public synchronized void leave_cell(AgentSignature signature) {
		occupied_by.remove(signature);
	}

	public synchronized void want_cell(AgentSignature signature) {
		wanted_by.add(signature);
		wanted_by_bet.add(Math.random());
	}

	public synchronized void unwant_cell(AgentSignature signature) {
		int index = wanted_by.indexOf(signature);
		wanted_by.remove(index);
		wanted_by_bet.remove(index);
	}

	

	public ArrayList<AgentSignature> getWantedList() {
		return wanted_by;
	}
	
	public ArrayList<Double> getBets(){
		return wanted_by_bet;
	}
	
	public String getWantedListString() {
		String result = "[";
		for(AgentSignature s: wanted_by) {
			result = result + s.getAgent_id()+ " ";
		}
		result = result + "]";
		return result;
	} 
	public int[] getIndex() {
		return index;
	}

	public int transformCell(int amount) {
		// essa variavel ira dizer se essa eh uma operacao de exploracao ou recuperacao
		// do espaco
		boolean transformation_recovering = (amount < 0);
		// essa variavel indica quanto de exploracao/recuperacao maxima foi feita sobre
		// a celula
		int max_remainder = 0;
		for (int i = 0; i < local_cell_values.length; i++) {
			for (int j = 0; j < local_cell_values[i].length; j++) {
				int local_value = local_cell_values[i][j];
				int local_transformation = local_value - amount;
				int local_remainder = 0;

				if (!transformation_recovering && local_transformation < minimum_local_cell_value) {
					local_remainder = 0 - local_transformation;
					local_transformation = minimum_local_cell_value;
				} else if (transformation_recovering && local_transformation > maximum_local_cell_value) {
					local_remainder = maximum_local_cell_value - local_transformation;
					local_transformation = maximum_local_cell_value;
				}

				local_cell_values[i][j] = local_transformation;

				if (Math.abs(local_remainder) > Math.abs(max_remainder)) {
					max_remainder = local_remainder;
				}
			}
		}
		return max_remainder;

	}

	public boolean equals(Object obj) {
		if (obj instanceof Cell) {
			int[] index_aux = ((Cell) obj).getIndex();
			return index_aux != null && index[0] == index_aux[0] && index[1] == index_aux[1];
		}
		return false;
	}

	@Override
	public int compareTo(Cell o) {
		return this.getGeneralProximalValue().compareTo(o.getGeneralProximalValue());
	}

	public int[][] getLocalValues() {
		return local_cell_values;
	}

}