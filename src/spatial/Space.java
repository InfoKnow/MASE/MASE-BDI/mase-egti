package spatial;

import java.util.ArrayList;
import java.util.Collections;

import agent.feature.AgentSignature;
import main.Simulation;
import main.loader.JSONLoader;
import main.loader.SpatialLoader;
import main.stats.StatsCollector;
import util.ImageUtil;
import util.MatrixWriter;
import util.PrettyPrinter;

public class Space {
	public static final int UNCONFLICTED = 0;
	public static final int ZERO_ZERO_CONFLICT = 1;
	public static final int ZERO_ONE_CONFLICT = 2;
	public static final int ONE_ONE_CONFLICT = 3;
	public static final int MANY_MANY_CONFLICT = 4;

	/* vari�vel com o objeto de Espa�o em si */
	private static Space space;

	/* As c�lulas em si */
	private Cell[][] cells;
	private int[][] conflicted_spaces;

	/* vari�veis de dimens�o */
	private int height, width;
	private int minimum_cell_group_height, minimum_cell_group_width;

	/* listas �teis */
	private ArrayList<Cell> best_cells;

	public static void initSpace() {
		space = new Space();
	}

	public static Space getInstance() {
		return space;
	}

	// 1. obter vari�veis do JSONLoader
	// 2. ler e processar imagens com ImageUtil
	// 3. instanciar c�lulas indexando-as corretamente
	// 4. construir listas �teis
	private Space() {

		// 1. obter vari�veis do JSONLoader
		this.height = Integer.parseInt(JSONLoader.getInstance().getPropertyValue("space.height"));
		this.width = Integer.parseInt(JSONLoader.getInstance().getPropertyValue("space.width"));
		this.minimum_cell_group_height = Integer
				.parseInt(JSONLoader.getInstance().getPropertyValue("space.cell_dimensions.height"));
		this.minimum_cell_group_width = Integer
				.parseInt(JSONLoader.getInstance().getPropertyValue("space.cell_dimensions.width"));

		// 2. ler e processar imagens com ImageUtil
		int[][] cell_values = getInitialCellValues();

		double[][] proximal_values = getProximalValues(cell_values);
		// MatrixWriter.write("proximal_matrix.mat", proximal_values);

		// 3. instanciar c�lulas indexando-as corretamente
		int indexed_height = height / minimum_cell_group_height;
		int indexed_width = width / minimum_cell_group_width;
		cells = new Cell[indexed_height][indexed_width];
		conflicted_spaces = new int[indexed_height][indexed_width];
		for (int i = 0; i < height; i = i + minimum_cell_group_height) {
			for (int j = 0; j < width; j = j + minimum_cell_group_width) {
				int indexed_i = i / minimum_cell_group_height;
				int indexed_j = j / minimum_cell_group_width;

				int[] index = new int[] { indexed_i, indexed_j };
				int[][] local_cell_values = new int[minimum_cell_group_height][minimum_cell_group_width];
				double[][] local_proximal_values = new double[minimum_cell_group_height][minimum_cell_group_width];
				for (int a = 0; a < minimum_cell_group_height; a++) {
					for (int b = 0; b < minimum_cell_group_width; b++) {
						local_cell_values[a][b] = cell_values[i + a][j + b];
						local_proximal_values[a][b] = proximal_values[i + a][j + b];
					}
				}
				cells[indexed_i][indexed_j] = new Cell(index, local_cell_values, local_proximal_values);
				conflicted_spaces[indexed_i][indexed_j] = Space.UNCONFLICTED;
			}
		}

		// 4. construir listas �teis
		setBestCells();
	}

	private int[][] getInitialCellValues() {
		String observed_space_sources[] = SpatialLoader.getInstance().getObservedSpaceSources();
		int[][] cell_values = ImageUtil.filtragem_classes_iniciais(observed_space_sources[0].trim());
		return cell_values;
	}

	private double[][] getProximalValues(int[][] cell_values) {
		double[][] proximal_values = null;

		// TODO melhorar isso
		if ((Boolean) (JSONLoader.getInstance().is_debug_mode())) {
			int[][] proximal_values_raw = ImageUtil.filtragem_spatial_attributes_debug();
			proximal_values = calculateProximalMatrixDebug(cell_values, proximal_values_raw);
		} else {
			String spatial_attributes_sources[] = SpatialLoader.getInstance().getSpatialAttributesSources();
			String spatial_attributes_factors[] = SpatialLoader.getInstance().getSpatialAttributesFactors();
			String pdot_path = SpatialLoader.getInstance().getPublicPolicy();
			int[][] proximal_values_raw = ImageUtil.filtragem_gaussiana(spatial_attributes_sources,
					spatial_attributes_factors);
			int[][] pdot_values = ImageUtil.filtragem_pdot(pdot_path);
			proximal_values = calculateProximalMatrix(cell_values, proximal_values_raw, pdot_values);
		}
		return proximal_values;
	}

	public double[][] calculateProximalMatrix(int[][] cell_config, int[][] proximalValues, int[][] pdot) {
		double[][] result = new double[proximalValues.length][proximalValues[0].length];
		String[] space_values = SpatialLoader.getInstance().getSpaceValues();
		String[] space_factors = SpatialLoader.getInstance().getSpaceFactors();
		String[] public_policy_factors = SpatialLoader.getInstance().getPublicPolicyFactors();
		String[] public_policy_values = SpatialLoader.getInstance().getPublicPolicyValues();
		for (int i = 0; i < proximalValues.length; i++) {
			for (int j = 0; j < proximalValues[i].length; j++) {
				int factor = pdot[i][j];
				for (int k = 0; k < public_policy_factors.length; k++) {
					if (factor == Integer.parseInt(public_policy_factors[k])) {
						result[i][j] = proximalValues[i][j] * Double.parseDouble(public_policy_values[k]);
					}
				}

				if (result[i][j] < 0) {
					continue;
				}

				int curr_exploration = cell_config[i][j];
				for (int k = 0; k < space_values.length; k++) {
					if (curr_exploration == Integer.parseInt(space_values[k])) {
						result[i][j] *= Double.parseDouble(space_factors[k]);
						break;
					}
				}
			}
		}

		return result;
	}

	public double[][] calculateProximalMatrixDebug(int[][] cell_config, int[][] proximalValues) {
		// 1 -> -1
		// 2 -> 100
		// 3 -> 10
		double[][] result = new double[proximalValues.length][proximalValues[0].length];
		for (int i = 0; i < proximalValues.length; i++) {
			for (int j = 0; j < proximalValues[i].length; j++) {
				switch (proximalValues[i][j]) {
				case 2:
					result[i][j] = 100;
					break;
				case 3:
					result[i][j] = 10;
					break;
				default:
					result[i][j] = -1;
				}
			}
		}

		return result;
	}

	private void setBestCells() {
		best_cells = new ArrayList<Cell>();
		for (int i = 0; i < cells.length; i++) {
			for (int j = 0; j < cells[i].length; j++) {
				best_cells.add(cells[i][j]);
			}
		}
		if (Boolean.parseBoolean(JSONLoader.getInstance().getPropertyValue("agents.init_random"))) {
			Collections.shuffle(best_cells);
		} else {
			Collections.sort(best_cells);
			Collections.reverse(best_cells);
		}

		// MatrixWriter.write("proximal_matrix_ordered.mat", best_cells,
		// minimum_cell_group_width,
		// minimum_cell_group_height, width, height);
	}

	public synchronized Cell getNextCell() {
		//Cell chosen = getAverageCell();
		//if (chosen == null) {
			return getNextBestCell();
		//}
		//return chosen;
	}

	private synchronized Cell getNextBestCell() {
		Cell choosen = null;
		int x = -1;
		int y = -1;
		do {
			if (best_cells != null && best_cells.size() > 0) {
				choosen = best_cells.remove(0);
				int cell_index[] = choosen.getIndex();
				x = cell_index[0];
				y = cell_index[1];
			} else if (best_cells.size() == 0) {
				PrettyPrinter.print("[WARNING] There are no cells left");
				Simulation.getInstance().endSimulation(true);
				return null;
			}
		} while (choosen == null || cells[x][y].getOwner() != null || cells[x][y].getMaxLocalValue() < 0
				|| cells[x][y].getGeneralProximalValue() < 0);

		return choosen;
	}

	private synchronized Cell getWorstAnthropizedCell() {
		Cell choosen = null;

		for (int i = best_cells.size() - 1; i >= 0; i--) {
			choosen = best_cells.get(i);
			int cell_index[] = choosen.getIndex();
			int x = cell_index[0];
			int y = cell_index[1];
			if (choosen == null || choosen.getState() != Cell.ANTHROPIZED || cells[x][y].getOwner() != null
					|| cells[x][y].getMaxLocalValue() < 0 || cells[x][y].getGeneralProximalValue() < 0) {
				continue;
			} else {
				best_cells.remove(i);
				break;
			}
		}

		return choosen;
	}
	private synchronized Cell getAverageCell() {
		Cell choosen = null;

		for (int i = best_cells.size()/2; i >= 0; i--) {
			choosen = best_cells.get(i);
			int cell_index[] = choosen.getIndex();
			int x = cell_index[0];
			int y = cell_index[1];
			if (choosen == null || cells[x][y].getOwner() != null
					|| cells[x][y].getMaxLocalValue() < 0 || cells[x][y].getGeneralProximalValue() < 0) {
				continue;
			} else {
				best_cells.remove(i);
				break;
			}
		}

		return choosen;
	}

	public Cell getCell(int x, int y) {
		return cells[x][y];
	}

	public Cell[][] getCells() {
		return cells;
	}
	
	public int[][] getConflictedSpaces() {
		return conflicted_spaces;
	}

	public synchronized void setConflictedCell(int x, int y, int status) {
		if (status > conflicted_spaces[x][y]) {
			conflicted_spaces[x][y] = status;
		}
	}

	public synchronized int transformCell(int x, int y, int amount) {
		return cells[x][y].transformCell(amount);
	}

	public synchronized void wantCell(int x, int y, AgentSignature signature) {
		cells[x][y].want_cell(signature);
	}

	public synchronized void occupyCell(int x, int y, AgentSignature signature) {
		cells[x][y].occupy_cell(signature);
	}

	public synchronized void leaveCell(int x, int y, AgentSignature signature) {
		cells[x][y].leave_cell(signature);
	}

	public synchronized boolean ownCell(int x, int y, AgentSignature signature) {
		if (cells[x][y].getOwner() == null) {
			StatsCollector.getInstance().registerNewSpaceAcquired();
			cells[x][y].setOwner(signature);
			return true;
		}
		return false;
	}

	public Cell[] getNeighbourhood(int i, int j, int vision) {
		if (!is_cell_in_bounds(i, j)) {
			return new Cell[] {};
		}
		// building the array
		int qty = 0;
		if (vision == 1) {
			qty = 4;
		} else {
			for (int a = 0; a <= vision; a++) {
				qty = qty + (1 + (a * vision));
			}
			for (int a = vision - 1; a >= 0; a--) {
				qty = qty + (1 + (a * vision));
			}
		}

		// remove the central point
		qty = qty - 1;

		Cell[] neighbours = new Cell[qty];
		// populating the array
		int count = 0;
		for (int a = -(vision); a <= (vision); a++) {
			int aux_i = i + a;
			for (int b = -((vision) - Math.abs(a)); b <= ((vision) - Math.abs(a)); b++) {
				int aux_j = j + b;
				if (aux_i == i && aux_j == j) {
					continue;
				}
				if (is_cell_in_bounds(aux_i, aux_j)) {
					neighbours[count] = cells[aux_i][aux_j];
					count++;
				}
			}
		}

		return neighbours;
	}

	private boolean is_cell_in_bounds(int i, int j) {
		return (i >= 0 && i < cells.length && j >= 0 && j < cells[0].length && cells[i][j].getState() != Cell.INVALID);
	}

	public int[][] getExplorationValues() {
		int[][] result = new int[height][width];

		for (int i = 0; i < cells.length; i++) {
			for (int j = 0; j < cells[i].length; j++) {
				int[][] local = cells[i][j].getLocalValues();
				for (int a = 0; a < local.length; a++) {
					for (int b = 0; b < local[a].length; b++) {
						result[i + a][j + b] = local[a][b];
					}
				}

			}
		}
		return result;
	}

	public ArrayList<Integer> getAgentsNearCell(int i, int j, int vision) {
		ArrayList<Integer> result = new ArrayList<>();
		ArrayList<int[]> new_near_agents = new ArrayList<>();
		Cell[] neighbours = getNeighbourhood(i, j, vision);
		for (int a = 0; a < neighbours.length; a++) {
			if (neighbours[a] != null) {
				for (AgentSignature as : neighbours[a].getWantedList()) {
					if (!result.contains(as.getAgent_id())) {
						result.add(as.getAgent_id());
						new_near_agents.add(neighbours[a].getIndex());
					}
				}
			}

		}
		return result;
	}

	public int getHeight() {
		return height;
	}

	public int getWidth() {
		return width;
	}
}