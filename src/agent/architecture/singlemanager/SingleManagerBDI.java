package agent.architecture.singlemanager;

import java.util.Map;

import agent.capability.GetNewPositionCapability;
import agent.capability.UpdateSignatureCapability;
import agent.feature.AgentSignature;
import jadex.bdiv3.BDIAgent;
import jadex.bdiv3.annotation.Belief;
import jadex.bdiv3.annotation.Capability;
import jadex.bdiv3.annotation.Plan;
import jadex.bridge.fipa.SFipa;
import jadex.bridge.service.types.message.MessageType;
import jadex.micro.annotation.Agent;
import jadex.micro.annotation.AgentArgument;
import jadex.micro.annotation.AgentBody;
import jadex.micro.annotation.AgentCreated;
import jadex.micro.annotation.AgentMessageArrived;
import jadex.micro.annotation.Argument;
import jadex.micro.annotation.Arguments;
import main.Simulation;
import main.loader.CommonBeliefDB;
import spatial.Space;

@Agent
@Arguments({ @Argument(name = "index", clazz = Integer.class) })

public class SingleManagerBDI {
	@Agent
	protected BDIAgent agent;
	@AgentArgument
	private int index;
	@Belief
	private AgentSignature signature;
	@Belief
	private int agent_quantity;
	@Belief
	private int[] agent_distribution;
	@Belief
	private int[] current_cell_x;
	@Belief
	private int[] current_cell_y;
	@Belief
	private int[] base_exploration;
	@Belief
	private int[] current_exploration;
	@Belief
	private int agent_vision;

	@Capability
	protected UpdateSignatureCapability upd_sig_capa = new UpdateSignatureCapability();
	@Capability
	protected GetNewPositionCapability new_pos_capa = new GetNewPositionCapability();

	@AgentCreated
	public void created() {
		agent_quantity = CommonBeliefDB.getAgent_quantity();
		agent_distribution = new int[agent_quantity];
		current_cell_x = new int[agent_quantity];
		current_cell_y = new int[agent_quantity];
		base_exploration = new int[agent_quantity];
		current_exploration = new int[agent_quantity];
		for (int i = 0; i < agent_quantity; i++) {
			int type = CommonBeliefDB.getStrategy_distribution(i);
			agent_distribution[i] = type;
			base_exploration[i] = CommonBeliefDB.getStrategy_exploration(type);
			current_cell_x[i] = -1;
			current_cell_y[i] = -1;
		}
		agent_vision = CommonBeliefDB.getAgent_vision();
	}

	@AgentBody
	public void body() {
		signature = new AgentSignature(index, agent.getComponentIdentifier(), -1);
		upd_sig_capa.setIndex(index);
		upd_sig_capa.setSignature(signature);
		agent.dispatchTopLevelGoal(upd_sig_capa.new Update()).get();
	}

	@AgentMessageArrived
	public void messageArrived(final Map<String, Object> msg, final MessageType mt) {
		final String performative = (String) msg.get(SFipa.PERFORMATIVE);
		switch (performative) {
		case CommonBeliefDB.PLEASE_START_MESSAGE:
			agent.adoptPlan("start");
			break;
		}
		agent.adoptPlan("deliberate");
	}

	@Plan
	public void start() {
		for (int i = 0; i < agent_quantity; i++) {
			current_exploration[i] = base_exploration[i];
		}
	}

	@Plan
	public void deliberate() {
		for (int i = 0; i < agent_quantity; i++) {
			individuallyDeliberate(i);
		}
	}

	public void individuallyDeliberate(int i) {
		if (current_cell_x[i] < 0 && current_cell_y[i] < 0) {
			getNewSpace(i);
		} else if (current_exploration[i] > 0
				&& Space.getInstance().getCell(current_cell_x[i], current_cell_y[i]).getMaxLocalValue() > 1) {
			act(i);
		} else if (current_exploration[i] > 0
				&& Space.getInstance().getCell(current_cell_x[i], current_cell_y[i]).getMaxLocalValue() < 1) {
			getNewSpace(i);
		} else if (current_exploration[i] == 0) {
			finish(i);
		}
	}

	public void getNewSpace(int i) {
		Object[] parameters = new Object[5];
		parameters[0] = current_cell_x[i];
		parameters[1] = current_cell_y[i];
		parameters[2] = signature;
		parameters[3] = agent_vision;
		parameters[4] = null;
		String str_new_cell = (String) agent.dispatchTopLevelGoal(new_pos_capa.new GetNewCell(parameters)).get();
		String positions[] = str_new_cell.split(CommonBeliefDB.SEPARATOR);
		current_cell_x[i] = Integer.parseInt(positions[0]);
		current_cell_y[i] = Integer.parseInt(positions[1]);
		Space.getInstance().ownCell(current_cell_x[i], current_cell_y[i], signature);
		individuallyDeliberate(i);

	}

	public void act(int i) {
		while (current_exploration[i] > 0) {
			int curr_cell_potential = Space.getInstance().getCell(current_cell_x[i], current_cell_y[i])
					.getMaxLocalValue();
			if (curr_cell_potential < 1) {
				break;
			} else {
				current_exploration[i] = Space.getInstance().transformCell(current_cell_x[i], current_cell_y[i],
						current_exploration[i]);
			}
		}
		individuallyDeliberate(i);
	}

	public void finish(int i) {
		boolean finished_last = CommonBeliefDB.registerFinish(i, current_cell_x[i], current_cell_y[i]);
		if (finished_last) {
			Simulation.getInstance().endStep();
		}
	}
}
