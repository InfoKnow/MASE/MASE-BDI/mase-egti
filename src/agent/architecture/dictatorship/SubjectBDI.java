package agent.architecture.dictatorship;

import java.util.HashMap;
import java.util.Map;

import agent.capability.UpdateSignatureCapability;
import agent.feature.AgentSignature;
import jadex.bdiv3.BDIAgent;
import jadex.bdiv3.annotation.*;
import jadex.bridge.IComponentIdentifier;
import jadex.bridge.fipa.SFipa;
import jadex.bridge.service.types.message.MessageType;
import jadex.commons.SUtil;
import jadex.micro.annotation.*;
import main.Simulation;
import main.loader.CommonBeliefDB;
import main.stats.StatsCollector;
import spatial.Space;

@Agent
@Arguments({ @Argument(name = "index", clazz = Integer.class), @Argument(name = "type", clazz = Integer.class) })
public class SubjectBDI {
	@Agent
	public BDIAgent agent;

	@AgentArgument
	private int index;
	@AgentArgument
	private int type;
	@Belief
	private AgentSignature signature;
	@Belief
	private int current_cell_x;
	@Belief
	private int current_cell_y;
	@Belief
	private int base_exploration;
	@Belief
	private int current_exploration;
	@Belief
	private String content;
	@Belief
	private String performative;
	@Belief
	private IComponentIdentifier manager_aid;
	@Belief
	private long total_conflicting_time;
	@Belief
	private long conflicting_time_t0;

	@Capability
	protected UpdateSignatureCapability upd_sig_capa = new UpdateSignatureCapability();

	@AgentCreated
	public void created() {
		current_cell_x = -1;
		current_cell_y = -1;
		base_exploration = CommonBeliefDB.getStrategy_exploration(type);
		content = null;
		performative = null;
		manager_aid = null;

	}

	@AgentBody
	public void body() {
		signature = new AgentSignature(index, agent.getComponentIdentifier(), type);
		upd_sig_capa.setIndex(index);
		upd_sig_capa.setSignature(signature);
		agent.dispatchTopLevelGoal(upd_sig_capa.new Update()).get();
	}

	@AgentMessageArrived
	public void messageArrived(final Map<String, Object> msg, final MessageType mt) {
		
		final String performative = (String) msg.get(SFipa.PERFORMATIVE);
		switch (performative) {
		case CommonBeliefDB.PLEASE_START_MESSAGE:
			agent.adoptPlan("start");
			break;
		case CommonBeliefDB.I_NEED_A_NEW_CELL:
			registerConflictingTimeEnd();
			String str_position = (String) msg.get(SFipa.CONTENT);
			String str_positions[] = str_position.split(CommonBeliefDB.SEPARATOR);
			current_cell_x = Integer.parseInt(str_positions[0]);
			current_cell_y = Integer.parseInt(str_positions[1]);
			break;
		}
		agent.adoptPlan("deliberate");
		return;
	}

	@Plan
	public void start() {
		current_exploration = base_exploration;
	}

	@Plan
	public void deliberate() {

		if (current_cell_x < 0 && current_cell_y < 0) {
			agent.adoptPlan("askForNewSpace");
		} else if (current_exploration > 0
				&& Space.getInstance().getCell(current_cell_x, current_cell_y).getMaxLocalValue() > 1) {
			agent.adoptPlan("act");
		} else if (current_exploration > 0
				&& Space.getInstance().getCell(current_cell_x, current_cell_y).getMaxLocalValue() < 1) {
			agent.adoptPlan("askForNewSpace");
		} else if (current_exploration == 0) {
			agent.adoptPlan("finish");
		}
	}

	@Plan
	public void askForNewSpace() {
		content = String.format("%s%s%s", current_cell_x, CommonBeliefDB.SEPARATOR, current_cell_y);
		performative = CommonBeliefDB.I_NEED_A_NEW_CELL;
		while (manager_aid == null) {
			AgentSignature sig = CommonBeliefDB.getManagerSignature();
			if (sig == null) {
				try {
					Thread.sleep(100);
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				manager_aid = sig.getAgent_aid();
			}
		}
		agent.adoptPlan("sendMessage");

	}

	@Plan
	public void act() {
		while (current_exploration > 0) {
			int curr_cell_potential = Space.getInstance().getCell(current_cell_x, current_cell_y).getMaxLocalValue();
			if (curr_cell_potential < 1) {
				break;
			} else {
				current_exploration = Space.getInstance().transformCell(current_cell_x, current_cell_y,
						current_exploration);
			}
		}
		agent.adoptPlan("deliberate");
	}

	@Plan
	public void finish() {
		StatsCollector.getInstance().registerConflictingTime(total_conflicting_time);
		boolean finished_last = CommonBeliefDB.registerFinish(index, current_cell_x, current_cell_y);
		if (finished_last) {
			Simulation.getInstance().endStep();
		}
	}

	@Plan
	private void sendMessage() {
		StatsCollector.getInstance().registerExchangedMessages(1);

		Map<String, Object> msg = new HashMap<>();
		msg.put(SFipa.CONTENT, content);
		msg.put(SFipa.PERFORMATIVE, performative);
		String convid = SUtil.createUniqueId("" + System.currentTimeMillis());
		msg.put(SFipa.CONVERSATION_ID, convid);
		msg.put(SFipa.RECEIVERS, new IComponentIdentifier[] { manager_aid });
		msg.put(SFipa.SENDER, agent.getComponentIdentifier());
		agent.sendMessage(msg, SFipa.FIPA_MESSAGE_TYPE);

		
		registerConflictingTimeStart();
	}

	public void registerConflictingTimeStart() {
		conflicting_time_t0 = System.currentTimeMillis();
	}

	public void registerConflictingTimeEnd() {
		total_conflicting_time += System.currentTimeMillis() - conflicting_time_t0;
	}

}
