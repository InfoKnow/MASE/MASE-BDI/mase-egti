package agent.architecture.dictatorship;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import agent.capability.GetNewPositionCapability;
import agent.capability.UpdateSignatureCapability;
import agent.feature.AgentSignature;
import jadex.bdiv3.BDIAgent;
import jadex.bdiv3.annotation.Belief;
import jadex.bdiv3.annotation.Capability;
import jadex.bdiv3.annotation.Plan;
import jadex.bdiv3.annotation.Trigger;
import jadex.bridge.IComponentIdentifier;
import jadex.bridge.fipa.SFipa;
import jadex.bridge.service.types.message.MessageType;
import jadex.micro.annotation.Agent;
import jadex.micro.annotation.AgentArgument;
import jadex.micro.annotation.AgentBody;
import jadex.micro.annotation.AgentCreated;
import jadex.micro.annotation.AgentMessageArrived;
import jadex.micro.annotation.Argument;
import jadex.micro.annotation.Arguments;
import main.loader.CommonBeliefDB;
import main.stats.StatsCollector;
import spatial.Space;

@Agent
@Arguments({ @Argument(name = "index", clazz = Integer.class) })

public class DictatorBDI {

	@Agent
	protected BDIAgent agent;
	@AgentArgument
	private int index;
	@Belief
	private AgentSignature signature;
	@Capability
	protected UpdateSignatureCapability upd_sig_capa = new UpdateSignatureCapability();
	@Capability
	protected GetNewPositionCapability new_pos_capa = new GetNewPositionCapability();

	@Belief
	private List<Object> msg_queue;
	@Belief
	private int msg_queue_step_counter;
	@Belief
	private int max_msg_queue_step_counter;
	@Belief
	private int msg_queue_general_counter;

	@AgentCreated
	public void created() {
		signature = new AgentSignature(index, agent.getComponentIdentifier(), -1);
		upd_sig_capa.setIndex(index);
		upd_sig_capa.setSignature(signature);
		msg_queue = new ArrayList<>();
		msg_queue_general_counter = 0;
		msg_queue_step_counter = 0;
	}

	@AgentBody
	public void body() {
		agent.dispatchTopLevelGoal(upd_sig_capa.new Update()).get();
	}

	@AgentMessageArrived
	public void messageArrived(final Map<String, Object> msg, final MessageType mt) {
		final String performative = (String) msg.get(SFipa.PERFORMATIVE);
		switch (performative) {
		case CommonBeliefDB.PLEASE_START_MESSAGE:
			agent.adoptPlan("start");
			break;
		case CommonBeliefDB.I_NEED_A_NEW_CELL:
			msg_queue_step_counter++;
			if (max_msg_queue_step_counter < msg_queue_step_counter) {
				max_msg_queue_step_counter = msg_queue_step_counter;
			}
			putMsgOnQueue(msg);
			break;
		}
		return;
	}

	@Plan
	public void start() {
		StatsCollector.getInstance().registerRequisitionLineLength(max_msg_queue_step_counter);
		msg_queue_step_counter = 0;
		max_msg_queue_step_counter = 0;
	}

	public Map<String, Object> getMsgFromQueue() {
		return modifyQueue(null);
	}

	public void putMsgOnQueue(Map<String, Object> new_message) {
		modifyQueue(new_message);
	}

	// HACK
	@SuppressWarnings("unchecked")
	private synchronized Map<String, Object> modifyQueue(Map<String, Object> new_message) {
		if (new_message != null) {
			msg_queue.add(new_message);
			msg_queue_general_counter++;
		} else if (msg_queue_general_counter > 0 && msg_queue.size() != 0) {
			msg_queue_step_counter--;
			return (Map<String, Object>) msg_queue.remove(0);
		}
		return null;
	}

	@Plan(trigger = @Trigger(factchangeds = "msg_queue_general_counter"))
	public void processMessageQueue() {
		if (msg_queue_general_counter > 0) {
			Map<String, Object> msg = null;
			while ((msg = getMsgFromQueue()) != null) {
				String content = (String) msg.get(SFipa.CONTENT);
				String performative = (String) msg.get(SFipa.PERFORMATIVE);
				IComponentIdentifier agent_aid = (IComponentIdentifier) msg.get(SFipa.SENDER);
				String positions[] = content.split(CommonBeliefDB.SEPARATOR);
				int position_x = Integer.parseInt(positions[0]);
				int position_y = Integer.parseInt(positions[1]);
				int agent_vision = CommonBeliefDB.getAgent_vision();
				AgentSignature agent_signature = CommonBeliefDB.getAgentSignature(agent_aid);
				Object[] parameters = new Object[5];
				parameters[0] = position_x;
				parameters[1] = position_y;
				parameters[2] = agent_signature;
				parameters[3] = agent_vision;
				parameters[4] = null;

				String str_new_cell = (String) agent.dispatchTopLevelGoal(new_pos_capa.new GetNewCell(parameters)).get();

				// no need to perform ResolveConflict, since we know agents will never clash
				positions = str_new_cell.split(CommonBeliefDB.SEPARATOR);
				position_x = Integer.parseInt(positions[0]);
				position_y = Integer.parseInt(positions[1]);
				Space.getInstance().ownCell(position_x, position_y, agent_signature);

				StatsCollector.getInstance().registerExchangedMessages(1);
				Map<String, Object> reply = agent.createReply(msg, SFipa.FIPA_MESSAGE_TYPE);
				reply.put(SFipa.CONTENT, str_new_cell);
				reply.put(SFipa.PERFORMATIVE, performative);
				agent.sendMessage(reply, SFipa.FIPA_MESSAGE_TYPE);

			}

		}
	}

}
