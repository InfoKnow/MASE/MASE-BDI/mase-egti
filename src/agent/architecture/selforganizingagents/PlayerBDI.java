package agent.architecture.selforganizingagents;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import agent.capability.GetNewPosition;
import agent.capability.GetNewPositionCapability;
import agent.capability.ResolveConflictFTACapability;
import agent.capability.ResolveConflictFTA;
import agent.capability.ResolveConflictSNG;
import agent.capability.ResolveConflictSNGCapability;
import agent.capability.UpdateSignatureCapability;
import agent.feature.AgentSignature;
import jadex.bdiv3.BDIAgent;
import jadex.bdiv3.annotation.Belief;
import jadex.bdiv3.annotation.Capability;
import jadex.bdiv3.annotation.Plan;
import jadex.bridge.IComponentIdentifier;
import jadex.bridge.fipa.SFipa;
import jadex.bridge.service.types.message.MessageType;
import jadex.micro.annotation.Agent;
import jadex.micro.annotation.AgentArgument;
import jadex.micro.annotation.AgentBody;
import jadex.micro.annotation.AgentCreated;
import jadex.micro.annotation.AgentMessageArrived;
import jadex.micro.annotation.Argument;
import jadex.micro.annotation.Arguments;
import main.Simulation;
import main.loader.CommonBeliefDB;
import main.loader.JSONLoader;
import main.loader.SpatialLoader;
import main.stats.StatsCollector;
import spatial.Cell;
import spatial.Space;

@Agent
@Arguments({ @Argument(name = "index", clazz = Integer.class), @Argument(name = "type", clazz = Integer.class) })

public class PlayerBDI {
	@Agent
	public BDIAgent agent;

	@AgentArgument
	private int index;
	@AgentArgument
	private int type;
	@Belief
	private int initial_type;
	@Belief
	private AgentSignature signature;
	@Belief
	private int current_cell_x;
	@Belief
	private int current_cell_y;
	@Belief
	private int next_cell_x;
	@Belief
	private int next_cell_y;
	@Belief
	private int base_exploration;
	@Belief
	private int current_exploration;
	@Belief
	private String content;
	@Belief
	private String performative;
	@Belief
	private IComponentIdentifier manager_aid;
	@Belief
	private long total_conflicting_time;
	@Belief
	private long conflicting_time_t0;
	@Belief
	private int agent_vision;
	@Belief
	private List<int[]> failed_positions;
	@Belief
	private int winner_id;
	@Belief
	private String strategy_change_type;
	@Belief
	private int[] acc_payoff;

	int message_quantity;
	int[] hawk_dove_conflicts_register;

	/* hawk_dove_conflicts */

	private int hawk_hawk_conflicts;
	private int hawk_dove_conflicts;
	private int dove_hawk_conflicts;
	private int dove_dove_conflicts;

	private int hawk_wins;
	private int hawk_losses;
	private int dove_wins;
	private int dove_losses;

	private int hawk_permanence;
	private int hawk_to_dove_conversion;
	private int dove_permanence;
	private int dove_to_hawk_conversion;

	private int hawk_new_vegetation_area;
	private int hawk_new_anthropic_area;
	private int dove_new_vegetation_area;
	private int dove_new_anthropic_area;

	@Capability
	protected UpdateSignatureCapability upd_sig_capa = new UpdateSignatureCapability();
	@Capability
	protected GetNewPositionCapability new_pos_capa = new GetNewPositionCapability();
	@Capability
	protected ResolveConflictFTACapability res_conf_FTA_capa = new ResolveConflictFTACapability();
	@Capability
	protected ResolveConflictSNGCapability res_conf_SNG_capa = new ResolveConflictSNGCapability();

	@AgentCreated
	public void created() {
		System.out.println(index + " created");
		current_cell_x = -1;
		current_cell_y = -1;
		base_exploration = CommonBeliefDB.getStrategy_exploration(type);
		content = null;
		performative = null;
		manager_aid = null;
		strategy_change_type = JSONLoader.getInstance().getPropertyValue("games.strategy_change_type");
		acc_payoff = new int[JSONLoader.getInstance().getCurrGame().getStrategy_count()];
	}

	@AgentBody
	public void body() {
		signature = new AgentSignature(index, agent.getComponentIdentifier(),
				CommonBeliefDB.getStrategy_distribution(index));
		upd_sig_capa.setIndex(index);
		upd_sig_capa.setSignature(signature);
		agent.dispatchTopLevelGoal(upd_sig_capa.new Update()).get();
	}

	@AgentMessageArrived
	public void messageArrived(final Map<String, Object> msg, final MessageType mt) {
		message_quantity++;
		final String performative = (String) msg.get(SFipa.PERFORMATIVE);
		switch (performative) {
		case CommonBeliefDB.PLEASE_START_MESSAGE:
			// agent.adoptPlan("start");
			start();
			break;
		case CommonBeliefDB.PLEASE_PROCESS_QUEUE:
			// agent.adoptPlan("validateSpace");
			validateSpace();
			break;
		case CommonBeliefDB.PLEASE_END_EXECUTION:
			System.out.println("ending player " + index);
			agent.killAgent().get();
			return;
		}
		// agent.adoptPlan("deliberate");
		deliberate();
		return;
	}

	@Plan
	public void start() {
		current_exploration = base_exploration;
		agent_vision = CommonBeliefDB.getAgent_vision();
		failed_positions = new ArrayList<int[]>();
		message_quantity = 0;
		initial_type = type;
		resetVariables();
	}

	private boolean stillHaveExploration() {
		if (base_exploration > 0) {
			return current_exploration > 0;
		} else {
			return current_exploration != 0;
		}
	}

	private boolean spaceStillHavePotential() {
		return Space.getInstance().getCell(current_cell_x, current_cell_y).getMaxLocalValue() > 1;
	}

	@Plan
	public void deliberate() {
		// TODO HACK - apenas para for�ar o reset de uma posi��o de agente
		// TODO � preciso consertar c�lulas desejadas por agentes fantasmas
		boolean forceMode = false;
		if (message_quantity > 10) {
			// System.out.println("");
			forceMode = true;
			message_quantity = 0;
		}

		if (current_cell_x < 0 && current_cell_y < 0) {
			// agent.adoptPlan("getNewSpace");
			getNewSpace(forceMode);
		} else if (stillHaveExploration() && spaceStillHavePotential()) {
			// agent.adoptPlan("act");
			act();
		} else if (stillHaveExploration() && !spaceStillHavePotential()) {
			// agent.adoptPlan("getNewSpace");
			getNewSpace(forceMode);
		} else if (!stillHaveExploration()) {
			// agent.adoptPlan("finish");
			finish();
		}
	}

	@Plan
	public void act() {
		if (base_exploration > 0) {
			while (current_exploration > 0) {
				int curr_cell_potential = Space.getInstance().getCell(current_cell_x, current_cell_y)
						.getMaxLocalValue();
				if (curr_cell_potential < 1) {
					break;
				} else {
					current_exploration = Space.getInstance().transformCell(current_cell_x, current_cell_y,
							current_exploration);
				}
			}
		} else {
			while (current_exploration != 0) {
				int curr_cell_potential = Space.getInstance().getCell(current_cell_x, current_cell_y)
						.getMaxLocalValue();
				if (curr_cell_potential >= SpatialLoader.getInstance().getMaxCellValue()) {
					break;
				} else {
					current_exploration = Space.getInstance().transformCell(current_cell_x, current_cell_y,
							current_exploration);
				}
			}
		}
		// agent.adoptPlan("deliberate");
		deliberate();
	}

	@Plan
	public void getNewSpace(boolean forceMode) {
		Object[] parameters = new Object[5];
		parameters[0] = current_cell_x;
		parameters[1] = current_cell_y;
		parameters[2] = signature;
		parameters[3] = agent_vision;
		parameters[4] = failed_positions;
		String str_new_cell = null;
		while (str_new_cell == null) {
			try {
				// str_new_cell = (String) agent.dispatchTopLevelGoal(new_pos_capa.new
				// GetNewCell(parameters)).get();
				if (!forceMode) {
					str_new_cell = new GetNewPosition().getNextCell(parameters);
				} else {
					System.out.println("forcing agent " + index + " to get brand new position");
					str_new_cell = new GetNewPosition().forceResetPosition(signature);

				}
			} catch (Exception e) {
				System.out.println("getNewSpace");
			}
		}
		String positions[] = str_new_cell.split(CommonBeliefDB.SEPARATOR);
		next_cell_x = Integer.parseInt(positions[0]);
		next_cell_y = Integer.parseInt(positions[1]);
		CommonBeliefDB.registerMovement(index, next_cell_x, next_cell_y);
		StatsCollector.getInstance().registerNewCell(type);
		if (CommonBeliefDB.getAgentResolveMethod().equals(CommonBeliefDB.RESOLVE_FTA)) {
			// agent.adoptPlan("validateSpace");
			// agent.adoptPlan("deliberate");
			validateSpace();
			deliberate();
		}
	}

	@Plan
	public void validateSpace() {
		Object[] response = getWinningAgent(new int[] { next_cell_x, next_cell_y });
		boolean won = (Boolean) response[0];
		int winner_strategy = (Integer) response[1];
		int payoff = (Integer) response[2];

		if (won) {
			current_cell_x = next_cell_x;
			current_cell_y = next_cell_y;
			Space.getInstance().ownCell(current_cell_x, current_cell_y, signature);
			if (type == 0) {
				hawk_wins++;
				if (Space.getInstance().getCell(current_cell_x, current_cell_y).getState() == Cell.NATURAL) {
					hawk_new_vegetation_area++;
				} else {
					hawk_new_anthropic_area++;
				}
			} else {
				dove_wins++;
				if (Space.getInstance().getCell(current_cell_x, current_cell_y).getState() == Cell.NATURAL) {
					dove_new_vegetation_area++;
				} else {
					dove_new_anthropic_area++;
				}
			}

		} else {
			if (type == 0) {
				hawk_losses++;
			} else {
				dove_losses++;
			}

		}
		reconsiderStrategy(won, winner_strategy, payoff);
	}

	public void reconsiderStrategy(boolean winner, int winner_strategy, int payoff) {
		switch (strategy_change_type) {
		case CommonBeliefDB.NONE_CHANGE_TYPE:
			return;
		case CommonBeliefDB.REVENGE_CHANGE_TYPE:
			if (!winner) {
				type = JSONLoader.getInstance().getCurrGame().get_best_response(type, winner_strategy);
			}
			break;
		case CommonBeliefDB.ACC_PAYOFF_CHANGE_TYPE:

			acc_payoff[type] += payoff;
			int curr_payoff = acc_payoff[type];
			for (int i = 0; i < acc_payoff.length; i++) {
				if (curr_payoff < acc_payoff[i]) {
					type = i;
				}
			}
			break;
		}
		
		signature.setAgent_type(type);
		//CommonBeliefDB.updateAgentSignature(index, signature);

	}

	public void calculateHawkDoveConflicts() {

		if (initial_type == 0 && type == 0) {
			hawk_permanence++;
		} else if (initial_type == 1 && type == 1) {
			dove_permanence++;
		} else if (initial_type == 0 && type == 1) {
			hawk_to_dove_conversion++;
		} else {
			dove_to_hawk_conversion++;
		}
		if (type == 0) {
			hawk_dove_conflicts_register[0] = 1;
			hawk_dove_conflicts_register[1] = 0;
		} else {
			hawk_dove_conflicts_register[0] = 0;
			hawk_dove_conflicts_register[1] = 1;
		}
		hawk_dove_conflicts_register[2] = hawk_hawk_conflicts;
		hawk_dove_conflicts_register[3] = hawk_dove_conflicts;
		hawk_dove_conflicts_register[4] = dove_hawk_conflicts;
		hawk_dove_conflicts_register[5] = dove_dove_conflicts;

		hawk_dove_conflicts_register[6] = hawk_wins;
		hawk_dove_conflicts_register[7] = hawk_losses;
		hawk_dove_conflicts_register[8] = dove_wins;
		hawk_dove_conflicts_register[9] = dove_losses;

		hawk_dove_conflicts_register[10] = hawk_permanence;
		hawk_dove_conflicts_register[11] = hawk_to_dove_conversion;
		hawk_dove_conflicts_register[12] = dove_permanence;
		hawk_dove_conflicts_register[13] = dove_to_hawk_conversion;

		hawk_dove_conflicts_register[14] = hawk_new_vegetation_area;
		hawk_dove_conflicts_register[15] = hawk_new_anthropic_area;
		hawk_dove_conflicts_register[16] = dove_new_vegetation_area;
		hawk_dove_conflicts_register[17] = dove_new_anthropic_area;

	}

	public void resetVariables() {
		hawk_dove_conflicts_register = new int[18];
		hawk_hawk_conflicts = 0;
		hawk_dove_conflicts = 0;
		dove_hawk_conflicts = 0;
		dove_dove_conflicts = 0;
		hawk_wins = 0;
		hawk_losses = 0;
		dove_wins = 0;
		dove_losses = 0;
		hawk_permanence = 0;
		hawk_to_dove_conversion = 0;
		dove_to_hawk_conversion = 0;
		dove_permanence = 0;
		hawk_new_vegetation_area = 0;
		hawk_new_anthropic_area = 0;
		dove_new_vegetation_area = 0;
		dove_new_anthropic_area = 0;
	}

	@Plan
	public void finish() {
		StatsCollector.getInstance().registerConflictingTime(total_conflicting_time);
		StatsCollector.getInstance().registerCensus(type);
		StatsCollector.getInstance().registerPosition(Space.getInstance().getCell(current_cell_x, current_cell_y));
		StatsCollector.getInstance().registerPayoffRegisters(acc_payoff[0], acc_payoff[1]);

		/* calculando hawks_doves */
		calculateHawkDoveConflicts();
		StatsCollector.getInstance().register_hawk_dove_conflicts(hawk_dove_conflicts_register);

		boolean finished_last = CommonBeliefDB.registerFinish(index, current_cell_x, current_cell_y);
		if (finished_last) {
			Simulation.getInstance().endStep();
		}
	}

	/* TODO refazer classe de capability */
	public Object[] getWinningAgent(int[] pos) {
		Object[] response = null;
		boolean am_i_the_winner = false;
		Integer winning_strategy = null;
		Integer payoff = null;
		int conflicts = 0;

		while (winning_strategy == null) {
			try {
				switch (CommonBeliefDB.getAgentResolveMethod()) {
				case CommonBeliefDB.RESOLVE_FTA:

					response = new ResolveConflictFTA().resolve(index, pos);
					am_i_the_winner = (Boolean) response[0];
					winning_strategy = (Integer) response[1];
					payoff = (Integer) response[2];
					conflicts = (Integer) response[3];
					hawk_hawk_conflicts += ((Integer) response[4]);
					hawk_dove_conflicts += ((Integer) response[5]);
					dove_hawk_conflicts += ((Integer) response[6]);
					dove_dove_conflicts += ((Integer) response[7]);
					break;
				case CommonBeliefDB.RESOLVE_SNG:
					// response = (Integer) agent.dispatchTopLevelGoal(res_conf_SNG_capa.new
					// ResolveConflict(pos)).get();
					response = new ResolveConflictSNG().resolve(index, pos);
					am_i_the_winner = (Boolean) response[0];
					winning_strategy = (Integer) response[1];
					payoff = (Integer) response[2];
					conflicts = (Integer) response[3];
					if (am_i_the_winner) {
						hawk_hawk_conflicts += ((Integer) response[4]);
						hawk_dove_conflicts += ((Integer) response[5]);
						dove_hawk_conflicts += ((Integer) response[6]);
						dove_dove_conflicts += ((Integer) response[7]);
					}
					if (am_i_the_winner && conflicts == 2) {
						if (hawk_hawk_conflicts > 0) {
							Space.getInstance().setConflictedCell(pos[0], pos[1], Space.ZERO_ZERO_CONFLICT);
						} else if (dove_dove_conflicts > 0) {
							Space.getInstance().setConflictedCell(pos[0], pos[1], Space.ONE_ONE_CONFLICT);
						} else {
							Space.getInstance().setConflictedCell(pos[0], pos[1], Space.ZERO_ONE_CONFLICT);
						}

					} else if (am_i_the_winner && conflicts > 2) {
						Space.getInstance().setConflictedCell(pos[0], pos[1], Space.MANY_MANY_CONFLICT);
					}

					break;
				}
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("getWinningAgent");
			}
		}

		return new Object[] { am_i_the_winner, winning_strategy, payoff };
	}

}
