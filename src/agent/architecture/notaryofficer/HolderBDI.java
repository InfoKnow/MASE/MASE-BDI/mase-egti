package agent.architecture.notaryofficer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import agent.capability.GetNewPositionCapability;
import agent.capability.UpdateSignatureCapability;
import agent.feature.AgentSignature;
import jadex.bdiv3.BDIAgent;
import jadex.bdiv3.annotation.Belief;
import jadex.bdiv3.annotation.Capability;
import jadex.bdiv3.annotation.Plan;
import jadex.bridge.IComponentIdentifier;
import jadex.bridge.fipa.SFipa;
import jadex.bridge.service.types.message.MessageType;
import jadex.commons.SUtil;
import jadex.micro.annotation.Agent;
import jadex.micro.annotation.AgentArgument;
import jadex.micro.annotation.AgentBody;
import jadex.micro.annotation.AgentCreated;
import jadex.micro.annotation.AgentMessageArrived;
import jadex.micro.annotation.Argument;
import jadex.micro.annotation.Arguments;
import main.Simulation;
import main.loader.CommonBeliefDB;
import main.stats.StatsCollector;
import spatial.Space;

@Agent
@Arguments({ @Argument(name = "index", clazz = Integer.class), @Argument(name = "type", clazz = Integer.class) })
public class HolderBDI {
	@Agent
	public BDIAgent agent;

	@AgentArgument
	private int index;
	@AgentArgument
	private int type;
	@Belief
	private AgentSignature signature;
	@Belief
	private int current_cell_x;
	@Belief
	private int current_cell_y;
	@Belief
	private int next_cell_x;
	@Belief
	private int next_cell_y;
	@Belief
	private int base_exploration;
	@Belief
	private int current_exploration;
	@Belief
	private String content;
	@Belief
	private String performative;
	@Belief
	private IComponentIdentifier manager_aid;
	@Belief
	private long total_conflicting_time;
	@Belief
	private long conflicting_time_t0;
	@Belief
	private int agent_vision;
	@Belief
	private List<int[]> failed_positions;
	@Belief
	private int winner_id;

	@Capability
	protected UpdateSignatureCapability upd_sig_capa = new UpdateSignatureCapability();
	@Capability
	protected GetNewPositionCapability new_pos_capa = new GetNewPositionCapability();

	@AgentCreated
	public void created() {
		//System.out.println(index+": started");
		current_cell_x = -1;
		current_cell_y = -1;
		base_exploration = CommonBeliefDB.getStrategy_exploration(type);
		content = null;
		performative = null;
		manager_aid = null;
		agent_vision = CommonBeliefDB.getAgent_vision();
	}

	@AgentBody
	public void body() {
		signature = new AgentSignature(index, agent.getComponentIdentifier(), type);
		upd_sig_capa.setIndex(index);
		upd_sig_capa.setSignature(signature);
		agent.dispatchTopLevelGoal(upd_sig_capa.new Update()).get();
	}

	@AgentMessageArrived
	public void messageArrived(final Map<String, Object> msg, final MessageType mt) {
		
		final String performative = (String) msg.get(SFipa.PERFORMATIVE);
		System.out.println(index+": message "+performative);
		switch (performative) {
		case CommonBeliefDB.PLEASE_START_MESSAGE:
			agent.adoptPlan("start");
			break;
		case CommonBeliefDB.PLEASE_VALIDATE_MY_CELL:
			System.out.println(index+": received validation");
			registerConflictingTimeEnd();
			String str_result = (String) msg.get(SFipa.CONTENT);
			winner_id = Integer.parseInt(str_result);
			if (winner_id == index) {
				System.out.println(index+": won");
				agent.adoptPlan("winCell");
			} else {
				System.out.println(index+": lost");
				agent.adoptPlan("loseCell");
			}
			break;
		}
		agent.adoptPlan("deliberate");
		return;
	}

	@Plan
	public void start() {
		//System.out.println(index+": start");
		current_exploration = base_exploration;
		failed_positions = new ArrayList<int[]>();
	}

	@Plan
	public void deliberate() {
		//System.out.println(index+": deliberate");

		if (current_cell_x < 0 && current_cell_y < 0) {
			agent.adoptPlan("getNewSpace");
		} else if (current_exploration > 0
				&& Space.getInstance().getCell(current_cell_x, current_cell_y).getMaxLocalValue() > 1) {
			agent.adoptPlan("act");
		} else if (current_exploration > 0
				&& Space.getInstance().getCell(current_cell_x, current_cell_y).getMaxLocalValue() < 1) {
			agent.adoptPlan("getNewSpace");
		} else if (current_exploration == 0) {
			agent.adoptPlan("finish");
		}
	}

	@Plan
	public void act() {
		//System.out.println(index+": act");
		while (current_exploration > 0) {
			int curr_cell_potential = Space.getInstance().getCell(current_cell_x, current_cell_y).getMaxLocalValue();
			if (curr_cell_potential < 1) {
				break;
			} else {
				current_exploration = Space.getInstance().transformCell(current_cell_x, current_cell_y,
						current_exploration);
			}
		}
		agent.adoptPlan("deliberate");
	}

	@Plan
	public void getNewSpace() {
		//System.out.println(index+": getnewspace");
		Object[] parameters = new Object[5];
		parameters[0] = current_cell_x;
		parameters[1] = current_cell_y;
		parameters[2] = signature;
		parameters[3] = agent_vision;
		parameters[4] = failed_positions;

		String str_new_cell = (String) agent.dispatchTopLevelGoal(new_pos_capa.new GetNewCell(parameters)).get();
		String positions[] = str_new_cell.split(CommonBeliefDB.SEPARATOR);
		next_cell_x = Integer.parseInt(positions[0]);
		next_cell_y = Integer.parseInt(positions[1]);
		CommonBeliefDB.registerMovement(index, next_cell_x, next_cell_y);
		agent.adoptPlan("askForSpaceValidation");
	}

	@Plan
	public void askForSpaceValidation() {
		//System.out.println(index+": askforspacevalidation");
		content = String.format("%s%s%s", next_cell_x, CommonBeliefDB.SEPARATOR, next_cell_y);
		performative = CommonBeliefDB.PLEASE_VALIDATE_MY_CELL;
		while (manager_aid == null) {
			AgentSignature sig = CommonBeliefDB.getManagerSignature();
			if (sig == null) {
				try {
					Thread.sleep(100);
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				manager_aid = sig.getAgent_aid();
			}
		}
		agent.adoptPlan("sendMessage");
	}

	@Plan
	public void winCell() {
		//System.out.println(index+": win");
		current_cell_x = next_cell_x;
		current_cell_y = next_cell_y;
		Space.getInstance().ownCell(current_cell_x, current_cell_y, signature);
	}

	@Plan
	public void loseCell() {
		//System.out.println(index+": lose");
		failed_positions.add(new int[] { next_cell_x, next_cell_y });
		StatsCollector.getInstance().registerUTurn();
	}

	@Plan
	public void finish() {
		//System.out.println(index+": finish");
		StatsCollector.getInstance().registerConflictingTime(total_conflicting_time);
		boolean finished_last = CommonBeliefDB.registerFinish(index, current_cell_x, current_cell_y);
		if (finished_last) {
			Simulation.getInstance().endStep();
		}
	}

	@Plan
	private void sendMessage() {
		StatsCollector.getInstance().registerExchangedMessages(1);

		Map<String, Object> msg = new HashMap<>();
		msg.put(SFipa.CONTENT, content);
		msg.put(SFipa.PERFORMATIVE, performative);
		String convid = SUtil.createUniqueId("" + System.currentTimeMillis());
		msg.put(SFipa.CONVERSATION_ID, convid);
		msg.put(SFipa.RECEIVERS, new IComponentIdentifier[] { manager_aid });
		msg.put(SFipa.SENDER, agent.getComponentIdentifier());
		StatsCollector.getInstance().registerExchangedMessages(1);
		agent.sendMessage(msg, SFipa.FIPA_MESSAGE_TYPE);

		registerConflictingTimeStart();
	}

	public void registerConflictingTimeStart() {
		conflicting_time_t0 = System.currentTimeMillis();
	}

	public void registerConflictingTimeEnd() {
		total_conflicting_time += System.currentTimeMillis() - conflicting_time_t0;
	}
}
