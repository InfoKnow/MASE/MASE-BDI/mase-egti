package agent.architecture.notaryofficer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import agent.capability.*;
import agent.feature.AgentSignature;
import jadex.bdiv3.BDIAgent;
import jadex.bdiv3.annotation.Belief;
import jadex.bdiv3.annotation.Capability;
import jadex.bdiv3.annotation.Plan;
import jadex.bdiv3.annotation.Trigger;
import jadex.bridge.IComponentIdentifier;
import jadex.bridge.fipa.SFipa;
import jadex.bridge.service.types.message.MessageType;
import jadex.micro.annotation.Agent;
import jadex.micro.annotation.AgentArgument;
import jadex.micro.annotation.AgentBody;
import jadex.micro.annotation.AgentCreated;
import jadex.micro.annotation.AgentMessageArrived;
import jadex.micro.annotation.Argument;
import jadex.micro.annotation.Arguments;
import main.loader.CommonBeliefDB;
import main.stats.StatsCollector;

@Agent
@Arguments({ @Argument(name = "index", clazz = Integer.class) })
public class NotaryBDI {
	@Agent
	protected BDIAgent agent;
	@AgentArgument
	private int index;
	@Belief
	private AgentSignature signature;
	@Capability
	protected UpdateSignatureCapability upd_sig_capa = new UpdateSignatureCapability();
	@Capability
	protected ResolveConflictFTACapability res_conf_FTA_capa = new ResolveConflictFTACapability();
	@Capability
	protected ResolveConflictSNGCapability res_conf_SNG_capa = new ResolveConflictSNGCapability();
	@Belief
	private List<Object> msg_queue;
	@Belief
	private int msg_queue_step_counter;
	@Belief
	private int max_msg_queue_step_counter;
	@Belief
	private boolean queue_ready;
	@Belief
	private int[] queue_ready_agents;
	@Belief
	private int msg_queue_general_counter;

	@AgentCreated
	public void created() {
		// System.out.println("Notary created");
		signature = new AgentSignature(index, agent.getComponentIdentifier(), -1);
		upd_sig_capa.setIndex(index);
		upd_sig_capa.setSignature(signature);
		msg_queue = new ArrayList<>();
		msg_queue_general_counter = 0;
		msg_queue_step_counter = 0;
		queue_ready = false;
	}

	@AgentBody
	public void body() {
		agent.dispatchTopLevelGoal(upd_sig_capa.new Update()).get();
	}

	@AgentMessageArrived
	public void messageArrived(final Map<String, Object> msg, final MessageType mt) {
		final String performative = (String) msg.get(SFipa.PERFORMATIVE);
		// System.out.println("Notary message "+performative);
		switch (performative) {
		case CommonBeliefDB.PLEASE_START_MESSAGE:
			agent.adoptPlan("start");
			break;
		case CommonBeliefDB.PLEASE_VALIDATE_MY_CELL:
			msg_queue_step_counter++;
			if (max_msg_queue_step_counter < msg_queue_step_counter) {
				max_msg_queue_step_counter = msg_queue_step_counter;
			}
			putMsgOnQueue(msg);
			break;
		case CommonBeliefDB.PLEASE_PROCESS_QUEUE:
			queue_ready = true;
			if (CommonBeliefDB.getAgentResolveMethod().equals(CommonBeliefDB.RESOLVE_WFN)) {
				System.out.println("Notary going to resolve " + (String) msg.get(SFipa.CONTENT));
				queue_ready_agents = CommonBeliefDB.destringify((String) msg.get(SFipa.CONTENT));
			}
			deliberate();
			break;
		}
		return;
	}

	@Plan
	public void start() {
		// System.out.println("Notary start ");
		StatsCollector.getInstance().registerRequisitionLineLength(max_msg_queue_step_counter);
		msg_queue_step_counter = 0;
		max_msg_queue_step_counter = 0;
	}

	public Map<String, Object> getMsgFromQueue() {
		return modifyQueue(null, null);
	}

	public Map<String, Object> getMsgFromQueue(AgentSignature a) {
		return modifyQueue(null, a);
	}

	public void putMsgOnQueue(Map<String, Object> new_message) {
		modifyQueue(new_message, null);
	}

	// HACK
	@SuppressWarnings("unchecked")
	private synchronized Map<String, Object> modifyQueue(Map<String, Object> new_message,
			AgentSignature specificSender) {
		if (new_message != null) {
			msg_queue.add(new_message);
			msg_queue_general_counter++;
		} else if (specificSender != null && msg_queue_general_counter > 0 && msg_queue.size() != 0) {
			int search = -1;
			for (int i = 0; i < msg_queue.size(); i++) {
				IComponentIdentifier aux = (IComponentIdentifier) ((Map<String, Object>) msg_queue.get(i))
						.get(SFipa.SENDER);
				if (CommonBeliefDB.getAgentSignature(aux).equals(specificSender)) {
					search = i;
					break;
				}
			}
			msg_queue_step_counter--;
			return (Map<String, Object>) msg_queue.remove(search);
		} else if (msg_queue_general_counter > 0 && msg_queue.size() != 0) {
			msg_queue_step_counter--;
			return (Map<String, Object>) msg_queue.remove(0);
		}
		return null;
	}

	@Plan(trigger = @Trigger(factchangeds = "msg_queue_general_counter"))
	public void deliberate() {
		// System.out.println("Notary deliberate ");
		String resolve_method = CommonBeliefDB.getAgentResolveMethod();
		switch (resolve_method) {
		case CommonBeliefDB.RESOLVE_FTA:
			processMessageQueue();
			break;
		case CommonBeliefDB.RESOLVE_SNG:
			if (queue_ready) {
				queue_ready = false;
				CommonBeliefDB.clearMovement();
				processMessageQueue();
			}
			break;
		case CommonBeliefDB.RESOLVE_WFN:
			if (queue_ready) {
				queue_ready = false;
				processMessageQueue(queue_ready_agents);
			}
			break;
		}
	}

	public void processMessageQueue() {
		// System.out.println("Notary process queue ");
		if (msg_queue_general_counter > 0) {
			Map<String, Object> msg = null;
			ArrayList<Map<String, Object>> replies = new ArrayList<>();

			while ((msg = getMsgFromQueue()) != null) {
				String content = (String) msg.get(SFipa.CONTENT);
				String performative = (String) msg.get(SFipa.PERFORMATIVE);
				String positions[] = content.split(CommonBeliefDB.SEPARATOR);
				int position_x = Integer.parseInt(positions[0]);
				int position_y = Integer.parseInt(positions[1]);

				Integer winning_index = getWinningAgent(new int[] { position_x, position_y });

				StatsCollector.getInstance().registerExchangedMessages(1);
				Map<String, Object> reply = agent.createReply(msg, SFipa.FIPA_MESSAGE_TYPE);
				reply.put(SFipa.CONTENT, "" + winning_index);
				reply.put(SFipa.PERFORMATIVE, performative);
				replies.add(reply);
			}

			for (Map<String, Object> reply : replies) {
				agent.sendMessage(reply, SFipa.FIPA_MESSAGE_TYPE);
			}
		}
	}

	public void processMessageQueue(int[] agent_list) {
		Map<String, Object> msg = null;
		ArrayList<Map<String, Object>> replies = new ArrayList<>();
		for (int i = 0; i < agent_list.length; i++) {
			CommonBeliefDB.deRegisterMovement(agent_list[i]);
			AgentSignature sig_ag = CommonBeliefDB.getAgentSignature(agent_list[i]);
			msg = getMsgFromQueue(sig_ag);
			String content = (String) msg.get(SFipa.CONTENT);
			String performative = (String) msg.get(SFipa.PERFORMATIVE);
			String positions[] = content.split(CommonBeliefDB.SEPARATOR);
			int position_x = Integer.parseInt(positions[0]);
			int position_y = Integer.parseInt(positions[1]);

			Integer winning_index = getWinningAgent(new int[] { position_x, position_y });

			StatsCollector.getInstance().registerExchangedMessages(1);
			Map<String, Object> reply = agent.createReply(msg, SFipa.FIPA_MESSAGE_TYPE);
			reply.put(SFipa.CONTENT, "" + winning_index);
			reply.put(SFipa.PERFORMATIVE, performative);
			replies.add(reply);

			System.out.println("Resolving " + sig_ag);
		}
		for (Map<String, Object> reply : replies) {
			agent.sendMessage(reply, SFipa.FIPA_MESSAGE_TYPE);
		}
	}

	public Integer getWinningAgent(int[] pos) {
		switch (CommonBeliefDB.getAgentResolveMethod()) {
		case CommonBeliefDB.RESOLVE_FTA:
			return (Integer) agent.dispatchTopLevelGoal(res_conf_FTA_capa.new ResolveConflict(pos)).get();
		case CommonBeliefDB.RESOLVE_SNG:
			return (Integer) agent.dispatchTopLevelGoal(res_conf_SNG_capa.new ResolveConflict(pos)).get();
		case CommonBeliefDB.RESOLVE_WFN:
			return (Integer) agent.dispatchTopLevelGoal(res_conf_SNG_capa.new ResolveConflict(pos)).get();
		}
		return null;
	}
}
