package agent.special;

import java.util.*;

import agent.feature.AgentSignature;
import jadex.bdiv3.BDIAgent;
import jadex.bdiv3.annotation.*;
import jadex.bridge.IComponentIdentifier;
import jadex.bridge.fipa.SFipa;
import jadex.commons.SUtil;
import jadex.micro.annotation.*;
import main.loader.CommonBeliefDB;
import main.stats.StatsCollector;

@Agent
@Arguments({ @Argument(name = "performative", clazz = String.class),
		@Argument(name = "agent_list", clazz = String.class), @Argument(name = "content", clazz = String.class) })
public class MercuryAgentBDI {

	
	@AgentArgument
	private String performative;
	@AgentArgument
	private String agent_list;
	@AgentArgument
	private String content;

	@Agent
	private BDIAgent agent;
	@Belief
	private IComponentIdentifier[] aid_geral;

	@AgentCreated
	public void created() {
		}

	@AgentBody
	public void body() {
		//agent.adoptPlan("getAgentsAID");
		//agent.adoptPlan("sendMessage");
		getAgentsAID();
		sendMessage();
	}

	@Plan
	private IComponentIdentifier[] getAgentsAID() {
		String[] agents = agent_list.split("" + CommonBeliefDB.SEPARATOR);

		IComponentIdentifier[] addresses = new IComponentIdentifier[agents.length];
		for (int i = 0; i < agents.length; i++) {

			int index = Integer.parseInt(agents[i]);
			AgentSignature sig = null;

			if(index >= 0) {
				sig = CommonBeliefDB.getAgentSignature(index);
			}else {
				sig = CommonBeliefDB.getManagerSignature();
			}
			while (sig == null) {

				try {
					Thread.sleep(100);
				} catch (Exception e) {
					e.printStackTrace();
				}
				if(index >= 0) {
					sig = CommonBeliefDB.getAgentSignature(index);
				}else {
					sig = CommonBeliefDB.getManagerSignature();
				}
			}
			addresses[i] = sig.getAgent_aid();
		}

		aid_geral = addresses;
		return addresses;
	}

	@Plan
	private void sendMessage() {
		StatsCollector.getInstance().registerExchangedMessages(aid_geral.length);
		
		Map<String, Object> msg = new HashMap<>();
		msg.put(SFipa.CONTENT, content);
		msg.put(SFipa.PERFORMATIVE, performative);
		String convid = SUtil.createUniqueId("" + System.currentTimeMillis());
		msg.put(SFipa.CONVERSATION_ID, convid);
		msg.put(SFipa.RECEIVERS, aid_geral);
		msg.put(SFipa.SENDER, agent.getComponentIdentifier());
		agent.sendMessage(msg, SFipa.FIPA_MESSAGE_TYPE).getException();
		System.out.println("sent "+performative+" to "+agent_list);
	}

}
