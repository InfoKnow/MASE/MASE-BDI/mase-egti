package agent;

import jadex.bdiv3.annotation.Belief;
import jadex.bdiv3.annotation.Plan;
import jadex.bridge.IComponentIdentifier;
import jadex.bridge.IInternalAccess;
import jadex.bridge.component.IMessageFeature;
import jadex.bridge.fipa.SFipa;
import jadex.micro.annotation.*;
import main.Main;
import main.Simulation;
import spatial.Cell;
import spatial.Space;
import util.DirectoryFacilitator;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import game.GenericGame;
import jadex.bridge.service.types.message.MessageType;
import jadex.commons.future.IFuture;
import jadex.commons.future.IResultListener;

@Agent
@Arguments({ @Argument(name = "type", clazz = Integer.class), @Argument(name = "index", clazz = Integer.class),
		@Argument(name = "agent_quantity", clazz = Integer.class),
		@Argument(name = "exploration_per_step", clazz = Integer.class),
		@Argument(name = "agent_vision", clazz = Integer.class),
		@Argument(name = "strategy_change_condition", clazz = String.class),
		@Argument(name = "strategy_change_type", clazz = String.class),
		@Argument(name = "start_paused", clazz = Boolean.class) })
public class AgentBDI {
	public static final String PLEASE_START_MESSAGE = "PLEASE_START_MESSAGE";
	public static final String PLEASE_ACT_MESSAGE = "PLEASE_ACT_MESSAGE";
	public static final String PLEASE_RESOLVE_MESSAGE = "PLEASE_RESOLVE_MESSAGE";
	public static final String IM_CONFLICTING_WITH_YOU_MESSAGE = "IM_CONFLICTING_WITH_YOU_MESSAGE";
	public static final String IM_RESOLVING_WITH_YOU_MESSAGE = "IM_RESOLVING_WITH_YOU_MESSAGE";
	public static final String SELF_DESTRUCT_MESSAGE = "SELF_DESTRUCT_MESSAGE";

	public static final String message_separator = " ";

	@AgentArgument
	private int type;
	@AgentArgument
	private int index;
	@AgentArgument
	private int agent_quantity;
	@AgentArgument
	private String exploration_per_step;
	@AgentArgument
	private int agent_vision;
	@AgentArgument
	private String strategy_change_condition;
	@AgentArgument
	private String strategy_change_type;
	@AgentArgument
	private boolean start_paused;

	@Agent
	private IInternalAccess agent;

	// explora��o
	@Belief
	private int curr_exploration = 0;
	@Belief
	private boolean is_regeneration_agent = false;

	// posi��o
	@Belief
	private int position_x;
	@Belief
	private int position_y;
	@Belief
	private int next_x;
	@Belief
	private int next_y;

	public ArrayList<Point2D> visited_positions;

	// resolu��o de conflito
	@Belief
	private String agentname;
	@Belief
	private int my_id;
	@Belief
	private int my_strategy;
	@Belief
	private double my_payoff;
	@Belief
	private int adversary_id;
	@Belief
	private int adversary_strategy;
	@Belief
	private double adversary_payoff;

	// troca de estrategia
	@Belief
	private double[] acc_payoff;

	@AgentCreated
	public void created() {
		my_id = index;
		agentname = DirectoryFacilitator.getInstance().createName(this.getClass().getName(), my_id);
		registerSelf(agentname, agent.getComponentIdentifier());
		my_strategy = type;
		Cell position = Space.getInstance().getBestCell(my_id, my_strategy);
		position_x = position.getX();
		position_y = position.getY();
		next_x = position_x;
		next_y = position_y;
		acc_payoff = new double[GenericGame.getInstance().strategy_count];
		for (int i = 0; i < acc_payoff.length; i++) {
			acc_payoff[i] = 0;
		}
		print(my_id, " created! Position: (" + position_x + ", " + position_y + ")");
	}

	@AgentMessageArrived
	public void messageArrived(final Map<String, Object> msg, final MessageType mt) {
		final String performative = (String) msg.get(SFipa.PERFORMATIVE);
		switch (performative) {
		case PLEASE_START_MESSAGE:
			start();
			break;
		case PLEASE_ACT_MESSAGE:
			act();
			break;
		case PLEASE_RESOLVE_MESSAGE:
			resolve();
			break;
		case IM_CONFLICTING_WITH_YOU_MESSAGE:
			treat_conflict(msg);
			break;
		case IM_RESOLVING_WITH_YOU_MESSAGE:
			receive_reply(msg);
			break;
		case SELF_DESTRUCT_MESSAGE:
			Main.getInstance().getInformationScreen().appendToInfoArea("Agent " + index + " is self-destructing");
			agent.killComponent();
			break;
		default:
		}
		return;
	}

	@Plan
	public void start() {
		curr_exploration = GenericGame.getInstance().get_exploration_per_step(type);
		if(curr_exploration > 0) {
			is_regeneration_agent = false;
		}else {
			is_regeneration_agent = true;
		}
		visited_positions = new ArrayList<>();
		act();
	}

	@Plan
	public void act() {
		// TODO activate regeneration agents
		if(is_regeneration_agent) {
			// this is a regeneration agent
			curr_exploration = Space.getInstance().recoverCell(position_x, position_y, curr_exploration);
		}else {
			//this is an exploration agent
			curr_exploration = Space.getInstance().exploreCell(position_x, position_y, curr_exploration);
		}
		boolean is_waiting = curr_exploration != 0;
		boolean is_done = !is_waiting;
		if (is_waiting) {
			move();
		} else {
			end();
		}
		Simulation.getInstance().registerState(index, is_waiting, is_done);

	}

	@Plan
	public void move() {
		Cell[] neighbours = Space.getInstance().getNeighbourhood(position_x, position_y, agent_vision);
		double[] probabilities = new double[neighbours.length];
		double sum = 0;

		for (int i = 0; i < probabilities.length; i++) {
			if (neighbours[i] != null && neighbours[i].getCurrExploration() > 1
					&& neighbours[i].getState() != Cell.INVALID && neighbours[i].getOwner() == -1
					&& !visited_positions.contains(new Point2D.Double(neighbours[i].getX(), neighbours[i].getY()))) {
				sum = sum + neighbours[i].getProximalValue();
				probabilities[i] = sum;
			} else {
				probabilities[i] = 0;
			}
		}

		if (sum != 0) {
			for (int i = 0; i < probabilities.length; i++) {
				probabilities[i] = probabilities[i] / sum;
			}
			double random = Math.random();
			for (int i = 0; i < probabilities.length; i++) {
				if (random < probabilities[i]) {
					next_x = neighbours[i].getX();
					next_y = neighbours[i].getY();
					break;
				}
			}
			Space.getInstance().occupyCell(next_x, next_y, index);
		} else {
			Cell substitute_next = Space.getInstance().getBestCell(index, my_strategy);
			next_x = substitute_next.getX();
			next_y = substitute_next.getY();
			// aqui n�o precisa de occupycell pq o m�todo getBestCell j� garante
			// exclusividade pro agente
		}
		visited_positions.add(new Point2D.Double(next_x, next_y));
	}

	@Plan
	public void resolve() {
		// checking if there's more agents with me in this cell
		ArrayList<Integer> occupants = Space.getInstance().getOccupants(next_x, next_y);
		if (occupants.size() == 1) {
			win();
		} else {
			Collections.sort(occupants);
			if (my_id == occupants.get(0)) {
				adversary_id = occupants.get(1);
				conflict();
			}
		}
	}

	public void conflict() {

		double my_bet = Math.random();
		String information = String.format("%s%s%s%s%s", my_id, message_separator, my_strategy, message_separator,
				my_bet);
		Map<String, Object> msg = new HashMap<>();
		msg.put(SFipa.CONTENT, information);
		msg.put(SFipa.PERFORMATIVE, IM_CONFLICTING_WITH_YOU_MESSAGE);
		msg.put(SFipa.RECEIVERS, new IComponentIdentifier[] { DirectoryFacilitator.getInstance()
				.getAgentAID(DirectoryFacilitator.getInstance().createName(AgentBDI.class.getName(), adversary_id)) });
		msg.put(SFipa.SENDER, agent.getComponentIdentifier());
		try {
			IFuture<Void> fut = agent.getComponentFeature(IMessageFeature.class).sendMessage(msg,
					SFipa.FIPA_MESSAGE_TYPE);
			fut.addResultListener(new IResultListener<Void>() {

				public void exceptionOccurred(Exception arg0) {
				}

				public void resultAvailable(Void arg0) {
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void treat_conflict(Map<String, Object> msg) {
		String information = (String) msg.get(SFipa.CONTENT);
		String[] tokens_information = information.split(message_separator);
		double my_bet = Math.random();
		adversary_id = Integer.parseInt(tokens_information[0]);
		adversary_strategy = Integer.parseInt(tokens_information[1]);
		double adversary_bet = Double.parseDouble(tokens_information[2]);
		my_payoff = GenericGame.getInstance().get_strategy_payoff(my_strategy, adversary_strategy);
		adversary_payoff = GenericGame.getInstance().get_strategy_payoff(my_strategy, adversary_strategy);
		Boolean i_won = null;
		Boolean adversary_won = null;

		if (my_payoff > adversary_payoff) {
			i_won = true;
			adversary_won = false;
		} else if (my_payoff < adversary_payoff) {
			i_won = false;
			adversary_won = true;
		} else if (my_payoff == adversary_payoff && my_bet > adversary_bet) {
			i_won = true;
			adversary_won = false;
		} else {
			i_won = false;
			adversary_won = true;
		}

		String reply = String.format("%s%s%s%s%s%s%s", my_strategy, message_separator, my_payoff, message_separator,
				adversary_payoff, message_separator, adversary_won);

		if (i_won) {
			Space.getInstance().leaveCell(next_x, next_y, adversary_id);
		} else {
			retreat();
		}

		Map<String, Object> reply_msg = new HashMap<>();
		reply_msg.put(SFipa.CONTENT, reply);
		reply_msg.put(SFipa.PERFORMATIVE, IM_RESOLVING_WITH_YOU_MESSAGE);
		reply_msg.put(SFipa.RECEIVERS, new IComponentIdentifier[] { (IComponentIdentifier) msg.get(SFipa.SENDER) });
		reply_msg.put(SFipa.SENDER, agent.getComponentIdentifier());
		try {
			IFuture<Void> fut = agent.getComponentFeature(IMessageFeature.class).sendMessage(reply_msg,
					SFipa.FIPA_MESSAGE_TYPE);
			fut.addResultListener(new IResultListener<Void>() {

				public void exceptionOccurred(Exception arg0) {
				}

				public void resultAvailable(Void arg0) {
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}

		reconsider_strategy();
		adversary_id = -1;
		adversary_strategy = -1;

		if (i_won) {
			resolve();
		}

	}

	public void receive_reply(Map<String, Object> msg) {
		String information = (String) msg.get(SFipa.CONTENT);
		String[] tokens_information = information.split(message_separator);
		adversary_strategy = Integer.parseInt(tokens_information[0]);
		adversary_payoff = Double.parseDouble(tokens_information[1]);
		my_payoff = Double.parseDouble(tokens_information[2]);
		Boolean i_won = Boolean.parseBoolean(tokens_information[3]);

		reconsider_strategy();

		if (i_won) {
			resolve();
		} else {
			retreat();
		}
	}

	public void reconsider_strategy() {
		acc_payoff[my_strategy] += my_payoff;
		if (strategy_change_type.equals("best_acc_payoff")) {
			int best_strategy_index = -1;
			double best_payoff_so_far = -1;
			for (int i = 0; i < acc_payoff.length; i++) {
				if (best_payoff_so_far < acc_payoff[i]) {
					best_strategy_index = i;
					best_payoff_so_far = acc_payoff[i];
				}
			}
			if (my_strategy != best_strategy_index) {
				my_strategy = best_strategy_index;
			}
		} else {
			int best_response = GenericGame.getInstance().get_best_response(adversary_strategy);
			if (best_response != my_strategy) {
				my_strategy = best_response;
			}
		}
	}

	public void win() {
		Space.getInstance().leaveCell(position_x, position_y, my_id);
		Space.getInstance().disownCell(position_x, position_y, my_id);
		position_x = next_x;
		position_y = next_y;
		Space.getInstance().ownCell(position_x, position_y, my_id, my_strategy);
		Simulation.getInstance().registerResolution();
	}

	public void retreat() {
		Space.getInstance().leaveCell(next_x, next_y, my_id);
		Simulation.getInstance().registerResolution();
	}

	@Plan
	public void end() {
		// generate statistics
		Simulation.getInstance().reportStrategy(my_strategy);
		Simulation.getInstance().reportExploration(curr_exploration, my_strategy);
	}

	public static void print(int my_id, String msg) {
		Main.getInstance().getInformationScreen().appendToInfoArea("[Agent" + my_id + "]: " + msg);
	}

	protected void registerSelf(String name, IComponentIdentifier identifier) {

		DirectoryFacilitator.getInstance().registerAgent(name, identifier);
		return;
	}
}
