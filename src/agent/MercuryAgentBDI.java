package agent;

import java.util.*;

import jadex.bridge.IComponentIdentifier;
import jadex.bridge.IInternalAccess;
import jadex.bridge.component.IMessageFeature;
import jadex.bridge.fipa.SFipa;
import jadex.micro.annotation.*;
import main.Platform;
import util.DirectoryFacilitator;
import jadex.commons.future.*;

@Agent
@Arguments({ @Argument(name = "performative", clazz = String.class),
		@Argument(name = "agent_list", clazz = String.class) })
public class MercuryAgentBDI {
	public static final char SEPARATOR = ' ';
	@AgentArgument
	private String performative;
	@AgentArgument
	private String agent_list;

	@Agent
	private IInternalAccess agent;

	@AgentCreated
	public void created() {
	}

	@AgentBody
	public void body() {

		IComponentIdentifier[] aid_geral = getAgentsAID();
		Platform.getInstance().getCms().getComponentIdentifiers()
				.addResultListener(new IResultListener<IComponentIdentifier[]>() {

					@Override
					public void exceptionOccurred(Exception arg0) {
					}

					@Override
					public void resultAvailable(IComponentIdentifier[] arg0) {
						Map<String, Object> msg = new HashMap<>();
						msg.put(SFipa.CONTENT, null);
						msg.put(SFipa.PERFORMATIVE, performative);
						msg.put(SFipa.RECEIVERS, aid_geral);
						msg.put(SFipa.SENDER, agent.getComponentIdentifier());
						try {
							IFuture<Void> fut = agent.getComponentFeature(IMessageFeature.class).sendMessage(msg,
									SFipa.FIPA_MESSAGE_TYPE);
							fut.addResultListener(new IResultListener<Void>() {

								@Override
								public void exceptionOccurred(Exception arg0) {
								}

								@Override
								public void resultAvailable(Void arg0) {
									agent.killComponent();
								}
							});
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});

	}

	private IComponentIdentifier[] getAgentsAID() {
		String[] agents = agent_list.split("" + SEPARATOR);

		IComponentIdentifier[] addresses = new IComponentIdentifier[agents.length];
		for (int i = 0; i < agents.length; i++) {
			int index = Integer.parseInt(agents[i]);
			String agentname = DirectoryFacilitator.getInstance().createName(AgentBDI.class.getName(), index);
			addresses[i] = DirectoryFacilitator.getInstance().getAgentAID(agentname);
		}

		return addresses;
	}

}
