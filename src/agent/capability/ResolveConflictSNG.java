package agent.capability;

import java.util.ArrayList;

import agent.feature.AgentSignature;
import game.Game;
import main.loader.CommonBeliefDB;
import spatial.Space;

// HACK
public class ResolveConflictSNG {
	// am_i_the_winner, winning_strategy, payoff
	public Object[] resolve(int my_id, int[] pivot_cell) {
		int pivot_cell_x = pivot_cell[0];
		int pivot_cell_y = pivot_cell[1];
		int my_type = CommonBeliefDB.getAgentSignature(my_id).getAgent_type();
		Game choosen_game = CommonBeliefDB.getChoosenGame();
		
		int concurrent_agent_quantity = 1;
		int h_vs_h = 0;		
		int h_vs_d = 0;
		int d_vs_h = 0;
		int d_vs_d = 0;

		if (Space.getInstance().getCell(pivot_cell_x, pivot_cell_y).getOwner() != null) {
			boolean am_i_the_winner = my_id == Space.getInstance().getCell(pivot_cell_x, pivot_cell_y).getOwner()
					.getAgent_id();
			Integer winning_strategy = my_type;
			Integer payoff = 0;
			if (!am_i_the_winner) {
				winning_strategy = Space.getInstance().getCell(pivot_cell_x, pivot_cell_y).getOwner().getAgent_type();
				payoff = (int) choosen_game.get_strategy_payoff(my_type, winning_strategy);
			}

			return new Object[] { am_i_the_winner, winning_strategy, payoff, concurrent_agent_quantity, h_vs_h, h_vs_d, d_vs_h, d_vs_d};
		} else {

			ArrayList<AgentSignature> agents = (ArrayList<AgentSignature>)Space.getInstance().getCell(pivot_cell_x, pivot_cell_y).getWantedList().clone();
			ArrayList<AgentSignature> agents_indexes = Space.getInstance().getCell(pivot_cell_x, pivot_cell_y).getWantedList();
			ArrayList<Double> bets = Space.getInstance().getCell(pivot_cell_x, pivot_cell_y).getBets();
			concurrent_agent_quantity = agents.size();
			
			if (agents.size() > 0) {
				AgentSignature winner = agents.remove(0);
				Integer winning_strategy = winner.getAgent_type();
				Integer payoff = 0;

//			Collections.sort(agents);

				while (agents.size() > 0) {
					AgentSignature opponent = agents.remove(0);
					
					if(winner.getAgent_type() == 0 && opponent.getAgent_type() == 0) {
						h_vs_h++;
					}else if(winner.getAgent_type() == 0 && opponent.getAgent_type() == 1) {
						h_vs_d++;
					}else if(winner.getAgent_type() == 1 && opponent.getAgent_type() == 0) {
						d_vs_h++;
					}else {
						d_vs_d++;
					}
					
					boolean referential_conflict_winner = winner.getAgent_id() == my_id;
					boolean referential_conflict_opponent = opponent.getAgent_id() == my_id;
					int payoff0 = (int) Math
							.round(choosen_game.get_strategy_payoff(winner.getAgent_type(), opponent.getAgent_type()));
					int payoff1 = (int) Math
							.round(choosen_game.get_strategy_payoff(opponent.getAgent_type(), winner.getAgent_type()));
					if (payoff0 <payoff1 ) {
						winner = opponent;
					} else if(payoff0 == payoff1){
						int winner_index = agents_indexes.indexOf(winner);
						double winners_bet = bets.get(winner_index);
						int opponent_index =  agents_indexes.indexOf(opponent);
						double opponents_bet = bets.get(opponent_index);
						if (opponents_bet > winners_bet) {
							winner = opponent;
						}
					}
					if (referential_conflict_winner) {
						payoff = payoff0;
						winning_strategy = winner.getAgent_type();
					}else if (referential_conflict_opponent) {
						payoff = payoff1;
						winning_strategy = winner.getAgent_type();
					}

				}
				boolean am_i_the_winner = my_id == winner.getAgent_id();
				return new Object[] { am_i_the_winner, winning_strategy, payoff, concurrent_agent_quantity, h_vs_h, h_vs_d, d_vs_h, d_vs_d };
			} else {
				return new Object[] { true, my_type, 0, concurrent_agent_quantity, h_vs_h, h_vs_d, d_vs_h, d_vs_d};

			}
		}
	}
}
