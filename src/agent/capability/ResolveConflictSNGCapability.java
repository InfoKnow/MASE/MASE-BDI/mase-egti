package agent.capability;

import java.util.ArrayList;
import java.util.Collections;

import agent.feature.AgentSignature;
import game.Game;
import jadex.bdiv3.annotation.Capability;
import jadex.bdiv3.annotation.Goal;
import jadex.bdiv3.annotation.GoalParameter;
import jadex.bdiv3.annotation.GoalResult;
import jadex.bdiv3.annotation.Plan;
import jadex.bdiv3.annotation.Trigger;
import main.loader.CommonBeliefDB;
import spatial.Space;

@Capability
public class ResolveConflictSNGCapability {
	@Goal
	public class ResolveConflict {
		@GoalParameter
		protected int[] pivot_cell;
		@GoalResult
		protected Integer winning_index;

		public ResolveConflict(int[] pivot_cell) {
			this.pivot_cell = pivot_cell;
		}
	}

	@Plan(trigger = @Trigger(goals = ResolveConflict.class))
	public Integer resolve(int[] pivot_cell) {
		int pivot_cell_x = pivot_cell[0];
		int pivot_cell_y = pivot_cell[1];

		if (Space.getInstance().getCell(pivot_cell_x, pivot_cell_y).getOwner() != null) {
			return Space.getInstance().getCell(pivot_cell_x, pivot_cell_y).getOwner().getAgent_id();
		}

		ArrayList<AgentSignature> agents = Space.getInstance().getCell(pivot_cell_x, pivot_cell_y).getWantedList();
		ArrayList<AgentSignature> agents_indexes = Space.getInstance().getCell(pivot_cell_x, pivot_cell_y).getWantedList();
		ArrayList<Double> bets = Space.getInstance().getCell(pivot_cell_x, pivot_cell_y).getBets();
		Game choosen_game = CommonBeliefDB.getChoosenGame();
		if (agents.size() > 0) {

			Collections.sort(agents);
			AgentSignature winner = agents.get(0);

			for (int i = 1; i < agents.size(); i++) {
				AgentSignature opponent = agents.get(1);
				double payoff = choosen_game.get_strategy_payoff(winner.getAgent_type(), opponent.getAgent_type());
				if (payoff < 1) {
					winner = opponent;
				} else {
					int winner_index = agents_indexes.indexOf(winner);
					double winners_bet = bets.get(winner_index);
					int opponent_index =  agents_indexes.indexOf(opponent);
					double opponents_bet = bets.get(opponent_index);
					if (opponents_bet > winners_bet) {
						winner = opponent;
					}
				}
			}
			return winner.getAgent_id();
		}
		return -1;
	}
}
