package agent.capability;

import java.util.List;

import agent.feature.AgentSignature;
import jadex.bdiv3.annotation.*;
import main.loader.CommonBeliefDB;
import spatial.Cell;
import spatial.Space;

@Capability
public class GetNewPositionCapability {

	@Goal
	public class GetNewCell {
		@GoalParameter
		Object[] parameters;
		@GoalResult
		protected String str_new_cell;

		public GetNewCell(Object[] parameters) {
			this.parameters = parameters;
		}
	}

	@Plan(trigger = @Trigger(goals = GetNewCell.class))
	public String getNextCell(Object[] parameters) {
		int pivot_cell_x = (Integer) parameters[0];
		int pivot_cell_y = (Integer) parameters[1];
		AgentSignature signature = (AgentSignature) parameters[2];
		int agent_vision = (Integer) parameters[3];
		@SuppressWarnings("unchecked")
		List<int[]> ignored_positions = (List<int[]>) parameters[4];

		Cell next_cell = null;
		Cell[] neighbours = Space.getInstance().getNeighbourhood(pivot_cell_x, pivot_cell_y, agent_vision);
		double[] probabilities = new double[neighbours.length];
		double sum = 0;

		for (int i = 0; i < neighbours.length; i++) {
			Cell neighbour = neighbours[i];
			if (neighbour != null && neighbour.getMaxLocalValue() > 1 && neighbour.getGeneralProximalValue() >= 0
					&& neighbour.getState() != Cell.INVALID && neighbour.getOwner() == null
					&& !shouldNeighbourBeIgnored(neighbour, ignored_positions)) {
				sum = sum + neighbour.getGeneralProximalValue();
				probabilities[i] = sum;
			} else {
				probabilities[i] = 0;
			}
		}

		if (sum != 0) {
			for (int i = 0; i < probabilities.length; i++) {
				probabilities[i] = probabilities[i] / sum;
			}
			double random = Math.random();
			for (int i = 0; i < probabilities.length; i++) {
				if (probabilities[i] != 0 && random < probabilities[i]) {
					next_cell = neighbours[i];
					break;
				}
			}

		} else {
			next_cell = Space.getInstance().getNextCell();
		}
		int next_cell_x = next_cell.getIndex()[0];
		int next_cell_y = next_cell.getIndex()[1];
		Space.getInstance().wantCell(next_cell_x, next_cell_y, signature);

		String result = String.format("%s%s%s", next_cell_x, CommonBeliefDB.SEPARATOR, next_cell_y);
		return result;
	}

	private boolean shouldNeighbourBeIgnored(Cell neighbour, List<int[]> ignored_positions) {
		if (ignored_positions == null || ignored_positions.isEmpty()) {
			return false;
		} else {
			int[] pos_neighbour = neighbour.getIndex();
			for (int[] aux : ignored_positions) {
				if (aux[0] == pos_neighbour[0] && aux[1] == pos_neighbour[1]) {
					return true;
				}
			}
		}

		return false;
	}
}
