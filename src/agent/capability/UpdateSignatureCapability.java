package agent.capability;

import agent.feature.AgentSignature;
import jadex.bdiv3.annotation.Belief;
import jadex.bdiv3.annotation.Capability;
import jadex.bdiv3.annotation.Goal;
import jadex.bdiv3.annotation.Plan;
import jadex.bdiv3.annotation.Trigger;
import main.loader.CommonBeliefDB;

@Capability
public class UpdateSignatureCapability {

	@Belief
	public int index;
	@Belief
	public AgentSignature signature;

	public UpdateSignatureCapability() {
	}
	
	public void setIndex(int index) {
		this.index = index;
	}
	
	public void setSignature(AgentSignature signature) {
		this.signature = signature;
	}
	
	@Goal
	public class Update {
		public Update() {
			
		}
	}

	@Plan(trigger = @Trigger(goals = Update.class))
	public void updateSignature() {
		CommonBeliefDB.updateAgentSignature(index, signature);
	}
}
