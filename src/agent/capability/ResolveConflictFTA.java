package agent.capability;

import java.util.ArrayList;

import agent.feature.AgentSignature;
import main.loader.CommonBeliefDB;
import spatial.Space;

public class ResolveConflictFTA {
	public Object[] resolve(int my_id, int[] pivot_cell) {
		int h_vs_h = 0;
		int h_vs_d = 0;
		int d_vs_h = 0;
		int d_vs_d = 0;

		int pivot_cell_x = pivot_cell[0];
		int pivot_cell_y = pivot_cell[1];
		int my_type = CommonBeliefDB.getAgentSignature(my_id).getAgent_type();
		
		ArrayList<AgentSignature> agents = (ArrayList<AgentSignature>)Space.getInstance().getCell(pivot_cell_x, pivot_cell_y).getWantedList().clone();
		int concurrent_agent_quantity = agents != null? agents.size(): 1;
		
		
		if (Space.getInstance().getCell(pivot_cell_x, pivot_cell_y).getOwner() != null) {
			boolean am_i_the_winner = my_id == Space.getInstance().getCell(pivot_cell_x, pivot_cell_y).getOwner()
					.getAgent_id();
			Integer winning_strategy = my_type;
			Integer payoff = 0;
			return new Object[] { am_i_the_winner, winning_strategy, payoff, concurrent_agent_quantity, h_vs_h, h_vs_d,
					d_vs_h, d_vs_d };
		} else if (Space.getInstance().getCell(pivot_cell_x, pivot_cell_y).getWantedList() != null
				&& concurrent_agent_quantity > 1) {
			int winner = Space.getInstance().getCell(pivot_cell_x, pivot_cell_y).getWantedList().get(0).getAgent_id();
			int winner_type = Space.getInstance().getCell(pivot_cell_x, pivot_cell_y).getWantedList().get(0).getAgent_type();
			
			for(int i = 1; i < agents.size(); i++) {
				int opponent_type = agents.get(i).getAgent_type();
				if(winner_type == 0 && opponent_type == 0) {
					h_vs_h++;
				}else if(winner_type == 0 && opponent_type == 1) {
					h_vs_d++;
				}else if(winner_type == 1 && opponent_type == 0) {
					d_vs_h++;
				}else {
					d_vs_d++;
				}
			}
			boolean am_i_the_winner = my_id == winner;
		
			return new Object[] { am_i_the_winner, winner_type, 0, concurrent_agent_quantity, h_vs_h, h_vs_d, d_vs_h, d_vs_d };
		}

		return new Object[] { true, my_type, 0, concurrent_agent_quantity, h_vs_h, h_vs_d, d_vs_h, d_vs_d };
	}

}
