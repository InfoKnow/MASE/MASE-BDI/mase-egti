package agent.capability;


import jadex.bdiv3.annotation.*;
import spatial.Space;

@Capability
public class ResolveConflictFTACapability {
	@Goal
	public class ResolveConflict {
		@GoalParameter
		protected int[] pivot_cell;
		@GoalResult
		protected Integer winning_index;

		public ResolveConflict(int[] pivot_cell) {
			this.pivot_cell = pivot_cell;
		}
	}

	@Plan(trigger = @Trigger(goals = ResolveConflict.class))
	public Integer resolve(int[] pivot_cell) {
		int pivot_cell_x = pivot_cell[0];
		int pivot_cell_y = pivot_cell[1];
		if (Space.getInstance().getCell(pivot_cell_x, pivot_cell_y).getOwner() != null) {
			return Space.getInstance().getCell(pivot_cell_x, pivot_cell_y).getOwner().getAgent_id();
		} else {
			if (Space.getInstance().getCell(pivot_cell_x, pivot_cell_y).getWantedList() != null
					&& Space.getInstance().getCell(pivot_cell_x, pivot_cell_y).getWantedList().size() > 0) {
				return Space.getInstance().getCell(pivot_cell_x, pivot_cell_y).getWantedList().get(0).getAgent_id();
			}
			return -1;
		}

	}
}
