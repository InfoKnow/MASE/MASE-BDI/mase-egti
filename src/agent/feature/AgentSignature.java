package agent.feature;

import jadex.bridge.IComponentIdentifier;

public class AgentSignature implements Comparable<AgentSignature> {
	private int agent_id;
	private IComponentIdentifier agent_aid;
	private int agent_type;

	public AgentSignature(int agent_id, IComponentIdentifier agent_aid, int agent_type) {
		this.agent_id = agent_id;
		this.agent_aid = agent_aid;
		this.agent_type = agent_type;
	}

	public int getAgent_id() {
		return agent_id;
	}

	public void setAgent_id(int agent_id) {
		this.agent_id = agent_id;
	}

	public IComponentIdentifier getAgent_aid() {
		return agent_aid;
	}

	public void setAgent_aid(IComponentIdentifier agent_aid) {
		this.agent_aid = agent_aid;
	}

	public int getAgent_type() {
		return agent_type;
	}

	public void setAgent_type(int agent_type) {
		this.agent_type = agent_type;
	}

	public boolean equals(Object obj) {
		return obj instanceof AgentSignature && this.agent_id == ((AgentSignature) obj).getAgent_id();
	}

	@Override
	public int compareTo(AgentSignature o) {
		return agent_id - o.getAgent_id();
	}
}
