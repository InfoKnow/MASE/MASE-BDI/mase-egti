package game;

import java.awt.Color;

import util.PrettyPrinter;

public class Game {
	private String game;
	private int strategy_count;
	private String[] strategy_label;
	private double[][] strategy_payoff;
	private Color[] strategy_color;
	private int[] strategy_exploration;
	private String[] enable_condition;
	private String[] disable_condition;

	public static final String UNSUSTAINABLE_SCENARIO_CONDITION = "unsustainable_scenario";

	private boolean unsustainable_scenario_condition = false;

	public static final String NONE_CONDITION = "none";

	public Game(String game, int strategy_count) {
		this.game = game;
		this.strategy_count = strategy_count;
		strategy_label = new String[strategy_count];
		strategy_payoff = new double[strategy_count][strategy_count];
		strategy_color = new Color[strategy_count];
		strategy_exploration = new int[strategy_count];
		enable_condition = new String[strategy_count];
		disable_condition = new String[strategy_count];
	}

	public String getGame() {
		return game;
	}

	public void setStrategy(int index, String label, double[] payoff, Color color, int exploration, String enable_cond,
			String disable_cond) {
		strategy_label[index] = label;
		strategy_payoff[index] = payoff;
		strategy_color[index] = color;
		strategy_exploration[index] = exploration;
		enable_condition[index] = enable_cond;
		disable_condition[index] = disable_cond;
	}

	public int getStrategy_count() {
		return strategy_count;
	}

	public boolean isStrategyEnabled(int index) {
		String condition = enable_condition[index];
		switch (condition) {
		case NONE_CONDITION:
			return true;
		case UNSUSTAINABLE_SCENARIO_CONDITION:
			return unsustainable_scenario_condition;
		}
		return true;
	}

	public String[] getLabels() {
		return strategy_label;
	}

	public String getLabel(int strategy) {
		return strategy_label[strategy];
	}

	public double get_strategy_payoff(int my_type, int opponent_type) {
		return strategy_payoff[my_type][opponent_type];
	}

	public int get_best_response(int player_type, int opponent_type) {
		/*
		 * int candidate = opponent_type; for (int i = 0; i <
		 * strategy_payoff[opponent_type].length; i++) { if (!isStrategyEnabled(i)) {
		 * continue; } else if (strategy_payoff[opponent_type][i] < 0) { candidate = i;
		 * if (strategy_payoff[i][opponent_type] > 0) { return candidate; } } }
		 */

		double payoff = strategy_payoff[player_type][opponent_type];
		double opponent_payoff = strategy_payoff[opponent_type][player_type];
		int best_response = player_type;
		for (int i = 0; i < strategy_payoff.length; i++) {
			if(i == player_type) {continue;}
			double candidate_payoff = strategy_payoff[i][opponent_type];
			double symmetric_candidate_payoff = strategy_payoff[opponent_type][i];
			if (payoff <= opponent_payoff && candidate_payoff >= symmetric_candidate_payoff) {
				best_response = i;
			}
		}
		//PrettyPrinter.print(String.format("The best response for %i is %i", opponent_type, best_response));
		return best_response;
	}
	
	public double get_revenge_payoff(int player_type, int opponent_type) {
		double direct_payoff = get_strategy_payoff(player_type, opponent_type);
		double reverse_payoff = get_strategy_payoff(opponent_type, player_type);
		int best_response = get_best_response(player_type, opponent_type);
		
		if(direct_payoff > reverse_payoff) {// 0 1
			return 0;
		}else if(direct_payoff == reverse_payoff && best_response == player_type) {// 0 0
			return 1;
		}else if(direct_payoff == reverse_payoff && best_response != player_type) {// 1 1
			return 0.5;
		}else if(direct_payoff < reverse_payoff) {// 1 0
			return -1;
		}
		return 0;
	}

	public String getAllExplorationPerStep() {

		String aux = "";
		for (int j = 0; j < getStrategy_count(); j++) {
			aux = aux + getStrategy_Exploration(j);
			if (j != getStrategy_count() - 1) {
				aux = aux + ",";
			}
		}

		return aux;
	}

	public void setUnsustainableScenario(boolean value) {
		unsustainable_scenario_condition = value;
	}

	public int[] getStrategiesExploration() {
		return strategy_exploration;
	}

	public int getStrategy_Exploration(int index) {
		return strategy_exploration[index];
	}

	public Color[] getStrategiesColors() {
		return strategy_color;
	}

	public String[] getEnableConditions() {
		return enable_condition;
	}

	public String[] getDisableConditions() {
		return disable_condition;
	}
}
